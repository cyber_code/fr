export enum WorkflowItemComponentType {
  DataSource,
  Transformation,
  Aggregation,
  ComparisonRule,
  Report,
  Start
}
export enum ComponentSavedStatus {
  UnSavedChanges = "UnSavedChanges" as any,
  Saving = "Saving" as any,
  Saved = "Saved" as any
}
