  import { WorkflowItemCustomData } from "./workflow-item-custom-data";
  import { WorkflowItemComponentType } from "./workflow-item-component-type.enum";

export class WorkflowItem {
   id: string;
   text: string;
   customData: WorkflowItemCustomData;
   workflowItemComponentType: WorkflowItemComponentType;
   startWorkflowItemCustomData: WorkflowItemCustomData;
   previousWorkflowItemCustomData: WorkflowItemCustomData;
   constructor(id: string, text: string, customData: WorkflowItemCustomData) {
      this.id = id;
     this.text = text;
      this.customData = customData;
     this.workflowItemComponentType = customData.componentType;
     this.customData.id = id;
   }
  }
