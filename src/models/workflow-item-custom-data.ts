import { WorkflowItemComponentType } from "./workflow-item-component-type.enum";

  export class WorkflowItemCustomData {
    id: string;
    pipelineId: string;
    componentType: WorkflowItemComponentType; //DataSource, Transformation,Aggregation,ComparisonRule..
    jsonMetadata: string;
}
