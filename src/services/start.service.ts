import { Injectable } from "@angular/core";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { ExecutePipeline, GetPipelineItemByPipelineId } from "src/app/body/commands-queries/start-command-queries";
import { CommandMethod } from "src/app/shared/command-method";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class StartService {
  private dataUrlExecution: string;
  private dataUrlDesign: string;

  constructor(private configurationService: ConfigurationService, private httpExecutor: HttpExecutorService) {
    this.dataUrlExecution = `${configurationService.serverSettings.apiUrl}Execution/`;
    this.dataUrlDesign = `${configurationService.serverSettings.apiUrl}Design/`;
  }

  execute(id) {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrlExecution,
      new ExecutePipeline(id),
      CommandMethod.POST
    );
  }

  getPipelineItemByPipelineId(pipelineId): Observable<any> {
    return this.httpExecutor.executeQuery(this.dataUrlDesign, new GetPipelineItemByPipelineId(pipelineId));
  }
  
  // prepareReport(id) {
  //   return this.httpExecutor.executeQuery(this.dataUrlDesign, new CreateReaportById(id));
  // }

}
