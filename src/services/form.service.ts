import { Injectable } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Injectable({
  providedIn: "root"
})
export class FormService {
  constructor() {}
  public validateAllFormFields(formToValidate: FormGroup) {
    Object.keys(formToValidate.controls).forEach(field => {
      const control = formToValidate.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
