import * as joint from "../../vendor/rappid";
import * as _ from "lodash";
import { StencilService } from "./stencil-service";
import { ToolbarService } from "./toolbar-service";
import { InspectorService } from "./inspector-service";
import { HaloService } from "./halo-service";
import { KeyboardService } from "./keyboard-service";
import * as appShapes from "../shapes/app-shapes";
import { RapidEventsService } from "./rapid-events.service";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { DialogService, DialogRef } from "@progress/kendo-angular-dialog";
import { ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs";

class KitchenSinkService {
  el: Element;

  graph: joint.dia.Graph;
  paper: joint.dia.Paper;
  paperScroller: joint.ui.PaperScroller;

  commandManager: joint.dia.CommandManager;
  snaplines: joint.ui.Snaplines;
  clipboard: joint.ui.Clipboard;
  selection: joint.ui.Selection;
  navigator: joint.ui.Navigator;

  stencilService: StencilService;
  toolbarService: ToolbarService;
  inspectorService: InspectorService;
  haloService: HaloService;
  keyboardService: KeyboardService;
  rapidEventService: RapidEventsService;
  isEditMode: boolean;
  dialogService;
  dialogView;
  selectedElement: joint.dia.Cell;
  constructor(
    el: Element,
    stencilService: StencilService,
    toolbarService: ToolbarService,
    inspectorService: InspectorService,
    haloService: HaloService,
    keyboardService: KeyboardService,
    rapidEventService: RapidEventsService,
    isEditMode: boolean,
    dialogService?: DialogService,
    dialogView?: ViewContainerRef
  ) {
    this.el = el;
    this.isEditMode = isEditMode;
    // apply current joint js theme
    new joint.mvc.View({ el: this.el });

    this.stencilService = stencilService;
    this.toolbarService = toolbarService;
    this.inspectorService = inspectorService;
    this.haloService = haloService;
    this.keyboardService = keyboardService;
    this.rapidEventService = rapidEventService;
    this.dialogService = dialogService;
    this.dialogView = dialogView;
  }
  getGraphJson() {
    return this.graph.toJSON();
  }

  loadGraphJson(graphJson: any) {
    this.graph.fromJSON(graphJson);
  }

  startRappid() {
    joint.setTheme("modern");

    this.initializePaper();
    this.initializeStencil();
    this.initializeSelection();
    this.initializeHaloAndInspector();
    this.initializeNavigator();
    this.initializeToolbar();
    this.initializeKeyboardShortcuts();
    this.initializeTooltips();
  }

  initializePaper() {
    const graph = (this.graph = new joint.dia.Graph(
      {},
      {
        cellNamespace: appShapes
      }
    ));

    graph.on("add", (cell: joint.dia.Cell, collection: any, opt: any) => {
         if (opt.stencil) {
        this.inspectorService.create(cell);
      }
    });

    this.commandManager = new joint.dia.CommandManager({ graph: graph });

    const paper = (this.paper = new joint.dia.Paper({
      width: 1000,
      height: 1000,
      gridSize: 10,
      drawGrid: true,
      model: graph,
      cellViewNamespace: appShapes,
      defaultLink: <joint.dia.Link>new appShapes.app.Link()
    }));

    paper.on("blank:mousewheel", _.partial(this.onMousewheel, null), this);
    paper.on("cell:mousewheel", this.onMousewheel.bind(this));

    this.snaplines = new joint.ui.Snaplines({ paper: paper });

    const paperScroller = (this.paperScroller = new joint.ui.PaperScroller({
      paper,
      autoResizePaper: true,
      cursor: "grab"
    }));

    this.renderPlugin(".paper-container", paperScroller);
    paperScroller.render().center();
  }
  public saleContentToFitPaper(paperWidth: number, paperHeight: number) {
    this.paper.setDimensions(paperWidth, paperHeight);
    this.paper.scaleContentToFit();
  }

  initializeStencil() {
    this.stencilService.create(this.paperScroller, this.snaplines);
    this.renderPlugin(".stencil-container", this.stencilService.stencil);
    this.stencilService.setShapes();
  }

  initializeSelection() {
    this.clipboard = new joint.ui.Clipboard();
    this.selection = new joint.ui.Selection({ paper: this.paper });
    const keyboard = this.keyboardService.keyboard;

    // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
    // Otherwise, initiate paper pan.
    this.paper.on(
      "blank:pointerdown",
      (evt: JQuery.Event, x: number, y: number) => {
        if (
          this.isEditMode &&
          this.selectedElement &&
          this.rapidEventService.hasUnSavedChanges
        ) {
          this.openConfirmUnSavedDialog().subscribe((res: any) => {
            let selectedValue = res.value || false;
            if (selectedValue) {
              this.rapidEventService.hasUnSavedChanges = false;
              this.blankPointerdownEvent(evt, x, y);
            } else {
              this.selection.startSelecting(evt);
              this.selectActualSelectedCellInGraph();
            }
          });
        } else {
          this.blankPointerdownEvent(evt, x, y);
        }
      }
    );

    this.paper.on(
      "blank:pointerup",
      (evt: JQuery.Event, x: number, y: number) => { }
    );

    this.paper.on(
      "element:pointerdown",
      (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
        // Select an element if CTRL/Meta key is pressed while the element is clicked.
        if (keyboard.isActive("ctrl meta", evt)) {
          this.selection.collection.add(elementView.model);
        }
      }
    );

    this.selection.on(
      "selection-box:pointerdown",
      (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
        // Unselect an element if the CTRL/Meta key is pressed while a selected element is clicked.
        if (keyboard.isActive("ctrl meta", evt)) {
          this.selection.collection.remove(elementView.model);
        }
      },
      this
    );
    const rapidEvent = this.rapidEventService;
    this.selection.changeHandle("remove", {
      events: {
        pointerdown: function (evt) {
          evt.stopPropagation();
          const cells = this.collection.models;
          rapidEvent.itemsForDelete.next(cells);
        }
      }
    } as any);
  }
  public selectActualSelectedCellInGraph() {
    var cell = this.graph.getCells().find(x => x.id == this.selectedElement.id);
    let actualCellView = this.paper.findViewByModel(cell);
    this.selectCellInGraph(actualCellView);
  }

  blankPointerdownEvent(evt: JQuery.Event, x: number, y: number) {
    if (this.keyboardService.keyboard.isActive("shift", evt)) {
      this.selection.startSelecting(evt);
    } else {
      this.selection.cancelSelection();
      this.paperScroller.startPanning(evt);
    }
    this.rapidEventService.itemSelected.next(null);
    this.selectedElement = null;
    let removeButton = this.el.querySelector(".joint-selection .handle.remove");
    if (removeButton) {
      if (this.isEditMode) {
        removeButton.classList.remove("noDisplay");
      } else {
        removeButton.classList.add("noDisplay");
      }
    }
  }
  selectCellInGraph(cellView: joint.dia.CellView) {
    const cell = cellView.model;
    if (!this.selection.collection.contains(cell)) {
      if (cell.isElement()) {
        new joint.ui.FreeTransform({
          cellView,
          allowRotation: false,
          preserveAspectRatio: !!cell.get("preserveAspectRatio"),
          allowOrthogonalResize: cell.get("allowOrthogonalResize") !== false
        }).render();
        if (this.isEditMode) {
          this.haloService.create(cellView);
          const rapidEvent = this.rapidEventService;
          this.haloService.halo.changeHandle("remove", {
            events: {
              pointerdown: function (evt) {
                evt.stopPropagation();
                const cell = this.options.cellView.model;
                rapidEvent.itemsForDelete.next([cell]);
              }
            }
          });
        }
        this.selection.collection.reset([]);
        this.selection.collection.add(cell, { silent: true });
      }
      this.paper.removeTools();
      this.inspectorService.create(cell);
    }
  }

  initializeHaloAndInspector() {
    this.paper.on("element:pointerup", (cellView: joint.dia.CellView) => {
      if (
        this.isEditMode &&
        this.selectedElement &&
        this.rapidEventService.hasUnSavedChanges
      ) {
        this.openConfirmUnSavedDialog().subscribe((res: any) => {
          let selectedValue = res.value || false;
          if (selectedValue) {
            this.rapidEventService.hasUnSavedChanges = false;
            this.selectCellInGraph(cellView);
            this.selectedElement = cellView.model;
            this.rapidEventService.itemSelected.next(cellView.model);
          } else {
            this.selectActualSelectedCellInGraph();
          }
        });
      } else {
        this.selectCellInGraph(cellView);
        this.selectedElement = cellView.model;
        this.rapidEventService.itemSelected.next(cellView.model);
      }
    });
    this.paper.on("link:pointerup", this.linkPointerUpEvent.bind(this));
  }
  public linkPointerUpEvent(linkView: joint.dia.LinkView) {
    if (
      this.isEditMode &&
      this.selectedElement &&
      this.rapidEventService.hasUnSavedChanges
    ) {
      this.openConfirmUnSavedDialog().subscribe((res: any) => {
        let selectedValue = res.value || false;
        if (selectedValue) {
          this.rapidEventService.hasUnSavedChanges = false;
          this.rapidEventService.itemSelected.next(null);
          this.selectedElement = null;
          this.linkPointerUp(linkView);
        } else {
          this.selectActualSelectedCellInGraph();
        }
      });
    } else {
      this.rapidEventService.itemSelected.next(null);
      this.selectedElement = null;
      this.linkPointerUp(linkView);
    }
  }
  public linkPointerUp(linkView: joint.dia.LinkView) {
    const link = linkView.model;
    const ns = joint.linkTools;
    const toolsView = new joint.dia.ToolsView({
      name: "link-pointerdown",
      tools: [
        new ns.Vertices(),
        new ns.SourceAnchor(),
        new ns.TargetAnchor(),
        // new ns.TargetArrowhead(),
        new ns.Segments(),
        new ns.Boundary({ padding: 15 })
        // new ns.Remove({ offset: -20, distance: 40 })
        //  new ns.Remove({})
      ]
    });
    this.selection.collection.reset([]);
    this.selection.collection.add(link, { silent: true });
    const paper = this.paper;
    joint.ui.Halo.clear(paper);
    joint.ui.FreeTransform.clear(paper);
    paper.removeTools();
    if (this.isEditMode) {
      linkView.addTools(toolsView);
    }
    this.inspectorService.create(link);
  }

  initializeNavigator() {
    const navigator = (this.navigator = new joint.ui.Navigator({
      width: 240,
      height: 115,
      paperScroller: this.paperScroller,
      zoom: false,
      paperOptions: {
        elementView: appShapes.NavigatorElementView,
        linkView: appShapes.NavigatorLinkView,
        cellViewNamespace: {
          /* no other views are accessible in the navigator */
        }
      }
    }));

    this.renderPlugin(".navigator-container", navigator);
  }

  initializeToolbar() {
    this.toolbarService.create(
      this.commandManager,
      this.paperScroller,
      !this.isEditMode
    );

    this.toolbarService.toolbar.on({
      "svg:pointerclick": this.openAsSVG.bind(this),
      "png:pointerclick": this.openAsPNG.bind(this),
      "fullscreen:pointerclick": joint.util.toggleFullScreen.bind(
        joint.util,
        document.body
      ),
      "to-front:pointerclick": this.selection.collection.invoke.bind(
        this.selection.collection,
        "toFront"
      ),
      "to-back:pointerclick": this.selection.collection.invoke.bind(
        this.selection.collection,
        "toBack"
      ),
      "layout:pointerclick": this.layoutDirectedGraph.bind(this),
      "snapline:change": this.changeSnapLines.bind(this),
      "clear:pointerclick": this.graph.clear.bind(this.graph),
      "print:pointerclick": this.paper.print.bind(this.paper),
      "grid-size:change": this.paper.setGridSize.bind(this.paper)
    });

    this.renderPlugin(".toolbar-container", this.toolbarService.toolbar);
  }

  changeSnapLines(checked: boolean) {
    if (checked) {
      this.snaplines.startListening();
      this.stencilService.stencil.options.snaplines = this.snaplines;
    } else {
      this.snaplines.stopListening();
      this.stencilService.stencil.options.snaplines = null;
    }
  }

  initializeKeyboardShortcuts() {
    if (this.isEditMode) {
      this.keyboardService.create(
        this.graph,
        this.clipboard,
        this.selection,
        this.paperScroller,
        this.commandManager,
        this.rapidEventService
      );
    }
  }

  initializeTooltips(): joint.ui.Tooltip {
    return new joint.ui.Tooltip({
      rootTarget: document.body,
      target: "[data-tooltip]",
      direction: joint.ui.Tooltip.TooltipArrowPosition.Auto,
      padding: 10
    });
  }

  openAsSVG() {
    this.paper.toSVG(
      (svg: string) => {
        new joint.ui.Lightbox({
          image: "data:image/svg+xml," + encodeURIComponent(svg),
          downloadable: true,
          fileName: "Rappid"
        }).open();
      },
      { preserveDimensions: true, convertImagesToDataUris: true }
    );
  }

  openAsPNG() {
    this.paper.toPNG(
      (dataURL: string) => {
        new joint.ui.Lightbox({
          image: dataURL,
          downloadable: true,
          fileName: "Rappid"
        }).open();
      },
      { padding: 10 }
    );
  }

  onMousewheel(
    cellView: joint.dia.CellView,
    evt: JQuery.Event,
    ox: number,
    oy: number,
    delta: number
  ) {
    if (this.keyboardService.keyboard.isActive("alt", evt)) {
      evt.preventDefault();
      this.paperScroller.zoom(delta * 0.2, {
        min: 0.2,
        max: 5,
        grid: 0.2,
        ox,
        oy
      });
    }
  }

  renderPlugin(selector: string, plugin: any): void {
    this.el.querySelector(selector).appendChild(plugin.el);
    plugin.render();
  }
  setEditMode(editMode: boolean) {
    this.isEditMode = editMode;
    this.initializeToolbar();
    this.initializeKeyboardShortcuts();
  }
  public unSelectItemsFromGraph() {
    this.selection.cancelSelection();
    this.selection.collection.reset([]);
    const paper = this.paper;
    joint.ui.Halo.clear(paper);
    joint.ui.FreeTransform.clear(paper);
    paper.removeTools();
    this.rapidEventService.itemSelected.next(null);
    this.selectedElement = null;
  }
  getCustomData(workflowItemModel: any) {
    workflowItemModel.attributes.customData = Object.assign(
      new WorkflowItemCustomData(),
      workflowItemModel.attributes.customData
    );
    return workflowItemModel.attributes.customData as WorkflowItemCustomData;
  }

  layoutGraph(centerContent, hideIcons?) {
    if (window.location.href.includes("test-generation") || hideIcons) {
      this.layoutDirectedGraph(centerContent);
    } else {
      const graph = this.getGraphJson();
      graph.cells.map(el => {
        if (el.type === "standard.Image") {
          let image = this.graph.getCell(el.id);
          if (image) {
            image.remove();
          }
        }
      });
      this.layoutDirectedGraph(centerContent);
    }
  }
  layoutDirectedGraph(centerContent) {
    joint.layout.DirectedGraph.layout(this.graph, {
      setVertices: true,
      rankDir: "TB",
      marginX: 100,
      marginY: 100
    });
    if (centerContent) {
      this.paperScroller.centerContent();
    }
  }
  openConfirmUnSavedDialog(): Observable<any> {
    const dialog: DialogRef = this.dialogService.open({
      title: "Please confirm",
      appendTo: this.dialogView,
      content:
        "Any unsaved changes on current selected component will be lost? Are you sure you want to continue?",
      actions: [
        { text: "No", primary: true, value: false },
        { text: "Yes", value: true }
      ],
      width: 450,
      height: 200,
      minWidth: 250
    });
    return dialog.result;
  }
}

export default KitchenSinkService;
