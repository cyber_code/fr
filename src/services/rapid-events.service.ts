import { Subject, Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class RapidEventsService {
  itemSelected: Subject<any> = new Subject<any>();
  itemSelected$ = this.itemSelected.asObservable();
  itemsForDelete: Subject<Array<joint.dia.Cell>> = new Subject<
    Array<joint.dia.Cell>
  >();
  itemsForDelete$ = this.itemsForDelete.asObservable();
  public hasUnSavedChanges: boolean;
}
