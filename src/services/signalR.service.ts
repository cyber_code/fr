import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { SessionService } from 'src/app/core/services/session.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
 
@Injectable({
  providedIn: 'root'
})
export class SignalRService {
    private hubConnection: signalR.HubConnection;
    public arrangementFields: any = [];
    public propAndAttributes: any = [];
    private signalRUrl;

    constructor(
        private sessionService: SessionService,
        private configuration: ConfigurationService
    ) {
       // this.signalRUrl = `${configuration.serverSettings.signalRUrl}`;
    }
    
    //public startConnection = () => {
    //     this.hubConnection = new signalR.HubConnectionBuilder()
    //                             .withUrl(this.signalRUrl, {
    //                                 transport: signalR.HttpTransportType.LongPolling,
    //                                 accessTokenFactory: () => this.sessionService.getToken() 
    //                             })
    //                             .build();

    //     this.hubConnection
    //         .start()
    //         .then(() => console.log('Connection started'))
    //         .catch(err => console.log('Error while starting connection: ' + err));
    // }

    // test() {
    //     this.hubConnection.on("BroadcastMessage", (type: string, payload: string) => {
    //         // this.messageService.add({ severity: type, summary: payload, detail: 'Via SignalR' });
    //       });
    
   // }
    
}