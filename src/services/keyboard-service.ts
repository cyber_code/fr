import * as joint from "../../vendor/rappid";
import * as _ from "lodash";
import { RapidEventsService } from "./rapid-events.service";

export class KeyboardService {
  keyboard: joint.ui.Keyboard;

  constructor() {
    this.keyboard = new joint.ui.Keyboard();
  }

  create(
    graph: joint.dia.Graph,
    clipboard: joint.ui.Clipboard,
    selection: joint.ui.Selection,
    paperScroller: joint.ui.PaperScroller,
    commandManager: joint.dia.CommandManager,
    rapidEventsService: RapidEventsService
  ) {
    this.keyboard.on({
      "delete backspace": (evt: JQuery.Event) => {
        evt.preventDefault();
        //graph.removeCells(selection.collection.toArray());
        if (
          selection.collection.toArray().length > 0 &&
          !selection.collection.toArray().every(x => x.isLink())
        ) {
          rapidEventsService.itemsForDelete.next(
            selection.collection.toArray()
          );
        }
      },

      "ctrl+z": () => {
        commandManager.undo();
        selection.cancelSelection();
      },

      "ctrl+y": () => {
        commandManager.redo();
        selection.cancelSelection();
      },

      "ctrl+a": () => {
        selection.collection.reset(graph.getElements());
      },

      "ctrl+plus": (evt: JQuery.Event) => {
        evt.preventDefault();
        paperScroller.zoom(0.2, { max: 5, grid: 0.2 });
      },

      "ctrl+minus": (evt: JQuery.Event) => {
        evt.preventDefault();
        paperScroller.zoom(-0.2, { min: 0.2, grid: 0.2 });
      },

      "keydown:shift": (evt: JQuery.Event) => {
        paperScroller.setCursor("crosshair");
      },

      "keyup:shift": () => {
        paperScroller.setCursor("grab");
      }
    });
  }
}
