import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainScreenComponent } from "./components/main-screen/main-screen.component";
import { AuthGuard } from "../shared/auth.guard";

const routes: Routes = [
  {
    path: "main-screen",
    component: MainScreenComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodyRoutingModule {}
