export enum MenuAction {
  NewFolder,
  NewPipeline,
  StartEdit,
  StopEdit,
  Rename,
  Delete
}
