import { NodeType } from "./node-type.enum";
import { MenuAction } from "./menu-action.enum";

export class MenuItem {
  type: NodeType;
  menuAction: MenuAction;
  text: string;
  isActive: boolean;

  constructor(type: NodeType, menuAction: MenuAction, isActive: boolean) {
    this.type = type;
    this.menuAction = menuAction;
    this.text = MenuAction[menuAction].replace(/([A-Z])/g, " $1").trim();
    this.isActive = isActive;
  }
}
