import {
  DataFileColumnItem,
  TreeNodeElement
} from "./properties-container.model";
import { FileInfo } from "@progress/kendo-angular-upload";
export enum TypesOfItem {
  Column = 1,
  Operator = 2,
  InputText = 3
}
export class EditTransformationItem {
  id: string;
  name: string;
  dataSourceId: string;
  transformations: Array<TransformationElement>;
  transformationFiltersData: Array<FilterItem>;
}
export class TransformationItem {
  id: string;
  name: string;
  dataSourceId: string;
  transformations: Array<TransformationElement>;
  transformationFiltersData: Array<TreeNodeElement>;
}
export class TransformationElement {
  key: string;
  fileId: string;
  transformationFunction: string;
  fileName: string;
  separator: string;
  sourceKey: string;
  targetKey: string;
}
export class EditTransformationElement {
  tempUiId: string;
  key: DataFileColumnItem;
  listKeyField: Array<DataFileColumnItem>;
  fileId: string;
  uploadedFilesInfo: Array<FileInfo>;
  transformationFunction: string;
  separator: string;
  fileName: string;
  sourceKey: string;
  targetKey: string;
}
export class FilterItem {
  filterNodeElement: TreeNodeElement;
  _tempId: string;
}
export class FilterExpressionElement {
  isOperator: boolean;
  value: string;
  expression: string;
}
