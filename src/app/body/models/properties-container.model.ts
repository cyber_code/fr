import { MathOperatorType } from "./properties-container.enum";
import { FileInfo } from "@progress/kendo-angular-upload";
import { PipelineItemColumnDataType } from "./properties-container.enum";

export interface KeyValueItem {
  key: string;
  value: string;
}
export class DataFileItem {
  id: string;
  path: string;
  pipelineItemColumns: Array<DataFileColumnItem>;
  uploadedFilesInfo: Array<FileInfo>;
  separator: string;
  haveHeaders: boolean;
}
export class DataFileColumnItem {
  id: string;
  name: string;
  dataType: null;
}
export class PipelineItemColumn {
  id: string;
  name: string;
  dataType: PipelineItemColumnDataType;
}
export class OperatorItem {
  key: string;
  name: string;
  value: string;
  precedence: number;
  mathOperatorType: MathOperatorType;
}
export class TreeNodeElement {
  isOperator: boolean;
  value: string;
  left: TreeNodeElement;
  right: TreeNodeElement;
  expression: string;
}
