import { ReportTemplateType, ReportType } from "./report-type.enum";
export class ReportsItem {
  id: string;
  name: string;
  reportType: ReportType;
  reportTemplateType: ReportTemplateType;
}
export class EditReportsItem extends ReportsItem {}
