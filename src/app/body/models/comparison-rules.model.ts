import {
  DataFileColumnItem,
  PipelineItemColumn,
  TreeNodeElement
} from "./properties-container.model";
import { FileInfo } from "@progress/kendo-angular-upload";
import { PipelineItemColumnDataType } from "./properties-container.enum";

export class ColumnItem {
  sourceId: string;
  columnId: string;
  __columnName: string;
  columnName: string;
  dataType: PipelineItemColumnDataType;
}

export class ComparisonItem {
  id: string;
  name: string;
  comparisonRules: Array<ComparisonRule>;
}

export class ComparisonRule {
  ShowDifference: boolean;
  toleranceValue: number;
  ComparisonRuleElement: TreeNodeElement;
  __tempId: string;
  itemsDataType: PipelineItemColumnDataType;
}
export class ColumnItemComparison {
  isOperator: boolean;
  value: string;
  expression: string;
  dataType: PipelineItemColumnDataType;
}

export class EditComparisonItem extends ComparisonItem {
  id: string;
}

export class EditComparisonElement {
  tempUiId: string;
  key: DataFileColumnItem;
  listKeyField: Array<DataFileColumnItem>;
  fileId: string;
  uploadedFilesInfo: Array<FileInfo>;
  comparisonFunction: string;
}

//////////////////////////////////////////////////
export class PipelineComponentItem {
  id: string;
  name: string;
  pipelineItemColumns: PipelineItemColumn[];
  isTarget: boolean;
}
