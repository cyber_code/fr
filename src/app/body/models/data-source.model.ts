import { DataFileColumnItem, KeyValueItem } from "./properties-container.model";

export enum DataSourceType {
  Legacy = 0,
  Target = 1
}
export class DataSourceItem {
  id: string;
  name: string;
  dataSourceColumns: Array<DataFileColumnItem>;
  identifiers?: Array<string>;
  dataSourceFiles: Array<FileItem>;
  isTarget?: boolean;
}
export class EditDataSourceItem extends DataSourceItem {
  dataSourceIdentifiers: Array<DataFileColumnItem>;
  dataFileSeparator: string;
  dataFileHaveHeaders: boolean;
  dataSourceType: KeyValueItem;
}
export class SimpleFileItem {
  fileId: string;
  separator: string;
  haveHeaders: boolean;
}
export class FileItem extends SimpleFileItem {
  path: string;
  fileName: string;
}
