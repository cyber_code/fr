export enum ReportType {
  Excel = 0,
  Pdf = 1,
  Word = 2
}
export enum ReportTemplateType {
  Full = 0,
  MissingActualResult = 1,
  MissingExpectedResult = 2,
  Differences = 3,
  Summary = 4
}
