import {
  DataFileColumnItem,
  PipelineItemColumn
} from "./properties-container.model";

export class AggregationFunctionItem {
  function: string;
  field: string;
}
export class AggregationItem {
  id: string;
  name: string;
  // key: string[];
  aggregations: AggregationElement[];
  groupByFields: string[];
}

export class EditAggregationItem {
  id: string;
  name: string;
  // key: string[];
  aggregations: AggregationElement[];
  groupByFields: string[];
}

export class EditAggregationElement {
  tempUiId: string;
  id: string;
  columnName: DataFileColumnItem;
  aggregationFunction: string;
  listcolumnName: DataFileColumnItem[];
  alias: string;
}

export class AggregationElement {
  id: string;
  aggregationFunction: string;
  columnName: string;
  alias: string;
}

export class PipelineItem {
  pipelineItemColumns: Array<PipelineItemColumn>;
}
