export enum PipelineItemColumnDataType {
  Numeric,
  Date,
  Text
}
export enum MathOperatorType {
  Arithmetic = 0,
  Comparison = 1,
  Logical = 2,
  Parenthesis = 3
}
