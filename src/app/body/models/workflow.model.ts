import { MenuAction } from "./menu-action.enum";

export class Workflow {
  id: string;
  pipelineId: string;
  title: string;
  description: string;
  designerJson: string;
  folderId: string;
}
export class ToolbarActionModel {
  popupTitle: string;
  action: MenuAction;
  title: string;
  description: string;
}
