import { CoreModule } from "./../core/core.module";
import { ConfirmDeleteActionComponent } from "./../core/components/confirm-delete-action/confirm-delete-action.component";
import { PipelinesService } from "../reports/services/pipelines.service";
import { WorkflowsService } from "./services/workflows.service";
import { ReportsService } from "./services/reports.service";
import { TransformationService } from "./services/transformation.service";
import { DeleteWorkflowItemComponent } from "./../core/components/delete-workflow-item/delete-workflow-item.component";
import { FieldErrorDisplayComponent } from "src/app/core/components/field-error-display/field-error-display.component";
import { DataSourceService } from "./services/data-source.service";
import { AggregationService } from "./services/aggregation.service";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TreeViewModule } from "@progress/kendo-angular-treeview";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { UploadModule } from "@progress/kendo-angular-upload";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { GridModule } from "@progress/kendo-angular-grid";
import { DialogsModule } from "@progress/kendo-angular-dialog";
import { BodyRoutingModule } from "./body-routing.module";
import { MenuModule } from "@progress/kendo-angular-menu";
import { ChartsModule } from "ng2-charts";
import { NavbarComponent } from "../core/components/navbar/navbar.component";
import { DataSourceComponent } from "./components/properties/data-source/data-source.component";
import { TransformationComponent } from "./components/properties/transformation/transformation.component";
import { AggregationComponent } from "./components/properties/aggregation/aggregation.component";
import { ComparisonRulesComponent } from "./components/properties/comparison-rules/comparison-rules.component";
import { ReportsComponent } from "./components/properties/reports/reports.component";
import { RapidEventsService } from "src/services/rapid-events.service";
import { PropetiesContainerComponent } from "./components/properties/propeties-container/propeties-container.component";
import { StartComponent } from "./components/properties/start/start.component";
import { TransformationCuFormComponent } from "./components/properties/transformation-cu-form/transformation-cu-form.component";
import { WorkflowItemService } from "./services/workflow-item.service";
import { WorkflowToolbarComponent } from "./components/workflow-toolbar/workflow-toolbar.component";
import { UploadDataFileComponent } from "./components/properties/upload-data-file/upload-data-file.component";
import { UploadMappingFileComponent } from "./components/properties/upload-mapping-file/upload-mapping-file.component";
import { ComparisionService } from "./services/comparision.service";
import { ComparisonRuleItemComponent } from "./components/properties/comparison-rule-item/comparison-rule-item.component";
import { AggregationFormComponent } from "./components/properties/aggregation-form/aggregation-form.component";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { DatePipe } from "@angular/common";
import { TransformationFilterItemComponent } from "./components/properties/transformation-filter-item/transformation-filter-item.component";

@NgModule({
  declarations: [
    WorkflowToolbarComponent,
    PropetiesContainerComponent,
    DataSourceComponent,
    TransformationComponent,
    AggregationComponent,
    ComparisonRulesComponent,
    ComparisonRuleItemComponent,
    ReportsComponent,
    StartComponent,
    TransformationCuFormComponent,
    FieldErrorDisplayComponent,
    DeleteWorkflowItemComponent,
    ConfirmDeleteActionComponent,
    UploadDataFileComponent,
    UploadMappingFileComponent,
    AggregationFormComponent,
    TransformationFilterItemComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BodyRoutingModule,
    TreeViewModule,
    BrowserAnimationsModule,
    DropDownsModule,
    UploadModule,
    ButtonsModule,
    GridModule,
    DialogsModule,
    DateInputsModule,
    MenuModule,
    InputsModule,
    ChartsModule,
    CoreModule
  ],
  providers: [
    RapidEventsService,
    DataSourceService,
    AggregationService,
    TransformationService,
    WorkflowItemService,
    ComparisionService,
    ReportsService,
    PipelinesService,
    WorkflowsService,
    DatePipe
  ],
  exports: [
    PropetiesContainerComponent,
    DeleteWorkflowItemComponent,
    ConfirmDeleteActionComponent,
    WorkflowToolbarComponent
  ]
})
export class BodyModule {}
