import { Injectable } from "@angular/core";
import { WorkflowItem } from "src/models/workflow-item";
import { Subject, Observable } from "rxjs";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";

@Injectable()
@AutoUnsubscribe()
export class WorkflowItemService {
  itemUpdated: Subject<WorkflowItem> = new Subject<WorkflowItem>();
  itemUpdated$ = this.itemUpdated.asObservable();
  workflowItemSelected: WorkflowItem;
  workflowEditable: Subject<boolean> = new Subject<boolean>();
  workflowEditable$ = this.workflowEditable.asObservable();
  editable: boolean;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.itemUpdated$.subscribe(itemSelected => {
      this.workflowItemSelected = itemSelected;
    });
    this.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(result => {
      this.editable = result;
    });
  }
}
