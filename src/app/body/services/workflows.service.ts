import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { CommandMethod } from "src/app/shared/command-method";
import { Workflow } from "../models/workflow.model";
import {
  savePipelineItem,
  deletePipelineItem
} from "../commands-queries/workflows-command-queries";

@Injectable()
export class WorkflowsService {
  private dataUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Design/`;
  }
  savePipelineItem(id: string, pipelineId: string, designerJson: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.dataUrl,
      new savePipelineItem(id, pipelineId, designerJson),
      CommandMethod.POST
    );
  }
  deletePipelineItem(id: string, pipelineId: string, designerJson: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.dataUrl,
      new deletePipelineItem(id, pipelineId, designerJson),
      CommandMethod.POST
    );
  }
}
