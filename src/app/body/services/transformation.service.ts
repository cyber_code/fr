import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { Observable } from "rxjs";
import {
  GetTransformationFunctions,
  DoEtlOnMappingFile,
  GetTransformationOperators
} from "../commands-queries/transformation-command-queries";
import { Workflow } from "../models/workflow.model";
import { CommandMethod } from "src/app/shared/command-method";
import { OperatorItem } from "../models/properties-container.model";

@Injectable()
export class TransformationService {
  private dataUrl: string;
  private mappingFileUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Design/`;
    this.mappingFileUrl = `${configurationService.serverSettings.apiUrl}MappingFile/`;
  }
  getTransformationFunctions(): Observable<string[]> {
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetTransformationFunctions()
    );
  }

  doEtlOnMappingFile(mappingFileId: string, separator: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.mappingFileUrl,
      new DoEtlOnMappingFile(mappingFileId, separator),
      CommandMethod.POST
    );
  }
  getTransformationOperators(): Observable<OperatorItem[]>
  {
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetTransformationOperators()
    );
  }
}
