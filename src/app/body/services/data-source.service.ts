import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { Workflow } from "../models/workflow.model";
import {
  DoEtlOnDataFile,
  DoEtlOnDataListOfFiles
} from "../commands-queries/data-source-command-queries";
import { CommandMethod } from "src/app/shared/command-method";
import { SimpleFileItem } from "../models/data-source.model";

@Injectable()
export class DataSourceService {
  private dataUrl: string;
  private dataFileUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Design/`;
    this.dataFileUrl = `${configurationService.serverSettings.apiUrl}DataFile/`;
  }

  doEtlOnDataFile(dataFileId: string, separator: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.dataFileUrl,
      new DoEtlOnDataFile(dataFileId, separator),
      CommandMethod.POST
    );
  }

  doEtlOnDataListOfFiles(dataFileId: string, dataFiles: Array<SimpleFileItem>) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.dataFileUrl,
      new DoEtlOnDataListOfFiles(dataFileId, dataFiles),
      CommandMethod.POST
    );
  }
}
