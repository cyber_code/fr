import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { GetAggregationFunctions } from "../commands-queries/aggregation-command-queries";
import { Observable } from "rxjs";

@Injectable()
export class AggregationService {
  private dataUrl: string;
  constructor(
    configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Design/`;
  }
  GetAggregationFunctions(): Observable<string[]> {
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetAggregationFunctions()
    );
  }
}
