import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { Workflow } from "../models/workflow.model";
import {
  GetComparisonRuleOperators,
  GetComparisonRuleLinkedItems
} from "../commands-queries/comparison-command-queries";
import { CommandMethod } from "src/app/shared/command-method";
import { KeyValueItem } from "../models/properties-container.model";
import { PipelineComponentItem } from "../models/comparison-rules.model";

@Injectable()
export class ComparisionService {
  private dataUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Design/`;
  }

  getComparisonRuleOperators(): Observable<KeyValueItem[]> {
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetComparisonRuleOperators()
    );
  }

  getComparisonRuleLinkedItems(
    ids: string[]
  ): Observable<PipelineComponentItem[]> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new GetComparisonRuleLinkedItems(ids),
      CommandMethod.POST
    );
  }
}
