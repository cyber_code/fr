import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { Workflow } from "../models/workflow.model";
import { CommandMethod } from "src/app/shared/command-method";
import { SaveReportPipelineItem } from "../commands-queries/report-command-queries";
import { ReportTemplateType } from "../models/report-type.enum";

@Injectable()
export class ReportsService {
  private url: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.url = `${configurationService.serverSettings.apiUrl}Design/`;
  }

  saveReportPipelineItem(id: string, pipelineId: string, designerJson: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.url,
      new SaveReportPipelineItem(id, pipelineId, designerJson),
      CommandMethod.POST
    );
  }
  getDownloadReportUrl(reportId: string, pipelineId: string) {
    return (
      this.url +
      "DownloadReportById?pipelineId=" +
      pipelineId +
      "&reportId=" +
      reportId
    );
  }
  public getReportTitle(reportTemplateType: ReportTemplateType) {
    switch (reportTemplateType) {
      case ReportTemplateType.Full:
        return "FR Report";

      case ReportTemplateType.MissingActualResult:
        return "Missing Records on Legacy";

      case ReportTemplateType.MissingExpectedResult:
        return "Missing Records on Target";

      case ReportTemplateType.Differences:
        return "Differences";

      case ReportTemplateType.Summary:
        return "Summary report";

      default:
        return "";
    }
  }
}
