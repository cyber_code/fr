import { NodeType } from "./../../models/node-type.enum";
import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { TreeViewItem } from "src/app/shared/tree-view-item";
import { DesignService } from "src/app/core/services/design.service";
import { Guid } from "src/app/shared/guid";

@Component({
  selector: "app-tree-view",
  templateUrl: "./tree-view.component.html",
  styleUrls: ["./tree-view.component.css"]
})
export class TreeViewComponent implements OnInit {
  public searchTerm: string = "";
  public data: TreeViewItem[] = [];
  public selectedKeys: String[] = [];
  public expandedKeys: String[] = [];
  public editModeState = false;
  public selectedItem: TreeViewItem;

  @Output() public treeViewItemSelected = new EventEmitter<TreeViewItem>();
  constructor(private designService: DesignService) {}

  ngOnInit() {
    this.getAllFolders();
  }
  getAllFolders() {
    this.designService.getFolders().subscribe(result => {
      this.data = result.map(
        item =>
          new TreeViewItem(
            item.id,
            item.title,
            item.description,
            NodeType.Folder,
            []
          )
      );
    });
  }
  public fetchChildren = (dataItem: TreeViewItem) =>
    this.designService.fetchPipelines(
      dataItem.id,
      this.searchTerm,
      dataItem.text.toLowerCase().includes(this.searchTerm.toLowerCase())
    );

  public hasChildren(node: TreeViewItem): boolean {
    return node.type !== NodeType.Workflow;
  }

  public iconClass(data: any): any {
    if (data.type == NodeType.Folder) {
      return "k-icon k-i-folder";
    } else {
      return "k-icon k-i-share";
    }
  }
  public disable(disable: boolean) {
    this.editModeState = disable;
  }
  public handleSelection($event: any): void {
    if (this.editModeState === true) {
      return;
    }
    this.selectedItem = $event.dataItem;
    this.treeViewItemSelected.emit(this.selectedItem);
  }
  public addTreeViewElement(treeViewItem: TreeViewItem): void {
    this.expandedKeys = [];
    if (treeViewItem.type === NodeType.Folder) {
      this.getAllFolders();
    } else if (this.selectedItem) {
      this.data
        .find(x => x.id == this.selectedItem.id)
        .children.push(treeViewItem);
      let id = this.selectedItem.id;
      setTimeout(() => {
        this.expandedKeys = [id];
      });
    }
    this.selectedItem = treeViewItem;
    this.selectedKeys = [this.selectedItem.id];
    this.treeViewItemSelected.emit(this.selectedItem);
  }
  public renameTreeViewElement(treeViewItem: TreeViewItem): void {
    if (treeViewItem.type == NodeType.Folder) {
      let item = this.data.find(x => x.id == this.selectedItem.id);
      item.text = treeViewItem.text;
      item.description = treeViewItem.description;
    } else {
      let expKeys = this.expandedKeys;
      this.expandedKeys = [];
      setTimeout(() => {
        this.expandedKeys = expKeys;
      });
    }
    this.selectedItem.text = treeViewItem.text;
    this.selectedItem.description = treeViewItem.description;
  }
  public deleteTreeViewElement(id: string): void {
    this.data = this.data.filter(item => item.id !== id);
  }

  searchTreeView() {
    if (this.searchTerm.length > 0) {
      this.designService
        .searchTreeViewItem(this.searchTerm)
        .subscribe(result => {
          let itemFolderFiltered: TreeViewItem[] = [];
          let folders = result.filter(x => x.pipelineId == Guid.empty);
          folders.forEach(function(item) {
            itemFolderFiltered.push(
              new TreeViewItem(
                item.folderId,
                item.title,
                item.description,
                NodeType.Folder,
                null
              )
            );
          });
          this.data = itemFolderFiltered;
        });
    } else {
      this.getAllFolders();
    }
  }
  public collapseAll() {
    this.expandedKeys = [];
  }
}
