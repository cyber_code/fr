import { ComponentSavedStatus } from "./../../../../models/workflow-item-component-type.enum";
import { DesignService } from "src/app/core/services/design.service";
import { TreeViewComponent } from "./../tree-view/tree-view.component";
import { MessageService } from "src/app/core/services/message.service";
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";
import { isNullOrUndefined } from "util";
import { StencilService } from "src/services/stencil-service";
import { ToolbarService } from "src/services/toolbar-service";
import { InspectorService } from "src/services/inspector-service";
import { HaloService } from "src/services/halo-service";
import { KeyboardService } from "src/services/keyboard-service";
import { RapidEventsService } from "src/services/rapid-events.service";
import { WorkflowItem } from "src/models/workflow-item";
import { WorkflowItemComponentType } from "src/models/workflow-item-component-type.enum";
import KitchenSinkService from "src/services/kitchensink-service";
import { WorkflowItemService } from "../../services/workflow-item.service";
import { MessageType } from "src/app/core/models/message.model";
import { TreeViewItem } from "src/app/shared/tree-view-item";
import { WorkflowToolbarComponent } from "../workflow-toolbar/workflow-toolbar.component";
import { MenuAction } from "../../models/menu-action.enum";
import { WorkflowsService } from "../../services/workflows.service";
import { ReportsService } from "../../services/reports.service";
import { DialogService } from "@progress/kendo-angular-dialog";
import { dia } from "vendor/rappid";
import { HighLighGraphHelper } from "src/app/shared/highlight-graph-helper";

//import { takeWhileAlive} from 'take-while-alive';
@Component({
  selector: "app-main-screen",
  templateUrl: "./main-screen.component.html",
  styleUrls: ["./main-screen.component.css"]
})
@AutoUnsubscribe()
export class MainScreenComponent implements OnInit {
  public rappid: KitchenSinkService;
  title = "Rappid App";
  public selectedWorkflowItem: WorkflowItem;
  public selectedWorkflowItemsForDelete: Array<joint.dia.Cell>;
  public openedDeleteWorkFlowItemWindow: boolean = false;
  public componentType: WorkflowItemComponentType;
  public graphCells: joint.dia.Cell[];
  public isWorkflowChanged: Boolean = false;
  private selectedTreeViewItem: TreeViewItem;
  public editMode = false;
  public componentSavedStatus: any = ComponentSavedStatus.Saved;
  public resultOfSelectedItem;
  public componentElements: dia.Cell[] = [];
  @ViewChild(WorkflowToolbarComponent)
  private workflowToolbarComponent: WorkflowToolbarComponent;
  @ViewChild(TreeViewComponent)
  private treeViewComponent: TreeViewComponent;
  @ViewChild("container", { read: ViewContainerRef })
  public containerRef: ViewContainerRef;
  public sourceC: any[] = [];
  public sourceT: any[] = [];
  constructor(
    private element: ElementRef,
    private rapidEventsService: RapidEventsService,
    private workflowItemService: WorkflowItemService,
    private workflowsService: WorkflowsService,
    private messageService: MessageService,
    private designService: DesignService,
    private reportsService: ReportsService,
    private dialogService: DialogService
  ) {

    this.selectedWorkflowItem = { customData: {} } as WorkflowItem;
    this.workflowItemService.workflowEditable.next(this.editMode);
    rapidEventsService.itemSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(result => {
        /// if it is the same element we don't have any reason to update the properties
        if (
          this.componentType &&
          this.selectedWorkflowItem &&
          result &&
          this.selectedWorkflowItem.id === result.id
        ) {
          return;
        }
        this.componentType = null;
        setTimeout(() => {
          this.updateWorkflowItemPropertiesPanel(result);
        }, 0);
      });
    rapidEventsService.itemsForDelete$
      .pipe(takeWhileAlive(this))
      .subscribe(result => {
        this.selectedWorkflowItemsForDelete = result;
        this.openedDeleteWorkFlowItemWindow = result != null;
      });
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false,
      this.dialogService,
      this.containerRef
    );
    this.rappid.startRappid();
    this.rappid.graph.on("change:target change:source", params => {
      this.onChangeLink(params);
    });
    this.rappid.paper.on("cell:pointerup", (elementView, evt) => {
      let cell = elementView.model;
      if (
        cell.isLink() &&
        cell.attributes.target.componentType ==
        WorkflowItemComponentType.Report &&
        cell.attributes.source.componentType !=
        WorkflowItemComponentType.ComparisonRule
      ) {
        cell.remove();
        this.messageService.sendMessage({
          type: MessageType.Error,
          text: `The report component should be allowed to be linked only with comparison component!`
        });
      } else if (
        cell.isLink() &&
        cell.attributes.target &&
        !cell.attributes.target.id
      ) {
        cell.remove();
        this.messageService.sendMessage({
          type: MessageType.Error,
          text: `The link should have target component!`
        });
      }
      else {
        if (
          cell.isLink() &&
          cell.attributes.source.componentType !=
          WorkflowItemComponentType.Start &&
          cell.attributes.target.componentType !=
          WorkflowItemComponentType.ComparisonRule
        ) {

          let selectedLink = cell;
          let up = cell.attributes.source.id;
          let down = cell.attributes.target.id;
          let graphCells = this.rappid.graph.getCells();
          let upComponent = graphCells.find(x => !x.isLink() && x.id == up);
          let downComponent = graphCells.find(x => !x.isLink() && x.id == down);

          if (
            selectedLink &&
            selectedLink.attributes.source.componentType ==
            WorkflowItemComponentType.DataSource &&
            selectedLink.attributes.target.componentType ==
            WorkflowItemComponentType.DataSource
          ) {
            selectedLink.remove();
            this.messageService.sendMessage({
              type: MessageType.Error,
              text: `Cannot link DataSource with DataSource!`
            });
          }
          else {
            if (selectedLink &&
              selectedLink.attributes.source.componentType ==
              WorkflowItemComponentType.Transformation &&
              cell.attributes.target.componentType ==
              WorkflowItemComponentType.Transformation) {
              selectedLink.remove();
              this.messageService.sendMessage({
                type: MessageType.Error,
                text: `Cannot link Transformation with Transformation!`
              });
            }
            if (selectedLink &&
              selectedLink.attributes.source.componentType ==
              WorkflowItemComponentType.Aggregation &&
              cell.attributes.target.componentType ==
              WorkflowItemComponentType.Aggregation) {
              selectedLink.remove();
              this.messageService.sendMessage({
                type: MessageType.Error,
                text: `Cannot link Aggregation with Aggregation!`
              });
            }
            else {
              if (upComponent.attributes.customData.jsonMetadata == "") {
                selectedLink.remove();
                this.messageService.sendMessage({
                  type: MessageType.Error,
                  text: `You should define the source component first`
                });
              } else {
                let dsTypesUp = this.findDatasourceType(upComponent, graphCells);
                let dsTypesDown = this.findDatasourceType(downComponent, graphCells);
                if (dsTypesUp &&
                  dsTypesDown &&
                  dsTypesUp.length > 0 &&
                  dsTypesDown.length > 0) {
                  let firstItemUp = dsTypesUp[0];
                  let firstItemDown = dsTypesDown[0];
                  if (firstItemUp != firstItemDown) {
                    selectedLink.remove();
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Different DataSource's file types`
                    });
                  }
                }
                if (selectedLink &&
                  selectedLink.attributes.source.componentType == WorkflowItemComponentType.DataSource &&
                  selectedLink.attributes.target.componentType == WorkflowItemComponentType.Aggregation
                ) {
                  let sourceId = this.findSourceIdOfLinks(this.rappid.graph.getCells().filter(x => x.isLink() && x.attributes.source.componentType == 0 && x.attributes.target.componentType == 2));
                  let sourceActualLink = selectedLink.attributes.source.id;
                  sourceId.pop()
                  let exists = sourceId.find(x => x == sourceActualLink)
                  if (!exists) {
                    sourceId.push(sourceActualLink)
                  }
                  else {
                    selectedLink.remove();
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Cannot link DataSource with two Aggregations`
                    });
                  }
                  
                  let targetId = this.findTargetIdOfLinks(this.rappid.graph.getCells().filter(x => x.isLink() && x.attributes.source.componentType == 0 && x.attributes.target.componentType == 2));
                  let targetActualLink = selectedLink.attributes.target.id;
                  targetId.pop();
                  let existsTarget = targetId.find(x => x == targetActualLink)
                  if (!existsTarget) {
                    targetId.push(targetActualLink)
                  }
                  else {
                    selectedLink.remove();
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Cannot link two DataSources with one Aggregation`
                    });
                  }
                }
                if (selectedLink &&
                  selectedLink.attributes.source.componentType == WorkflowItemComponentType.DataSource &&
                  selectedLink.attributes.target.componentType == WorkflowItemComponentType.Transformation
                ) {
                  let sourceId = this.findSourceIdOfLinks(this.rappid.graph.getCells().filter(x => x.isLink() && x.attributes.source.componentType == 0 && x.attributes.target.componentType == 1))
                  let c = selectedLink.attributes.source.id
                  sourceId.pop()
                  let exists = sourceId.find(x => x === c);
                  if (!exists) {
                    sourceId.push(c)
                    if (dsTypesUp &&
                      dsTypesDown &&
                      dsTypesUp.length > 0 &&
                      dsTypesDown.length > 0) {
                      let firstItemUp = dsTypesUp[0];
                      let firstItemDown = dsTypesDown[0];
                      if (firstItemUp != firstItemDown) {
                        selectedLink.remove();
                        sourceId.pop()
                      }
                    }
                  }
                  else {
                    selectedLink.remove()
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Cannot link DataSource with two Transformations`
                    });
                  }

                  let targetId = this.findTargetIdOfLinks(this.rappid.graph.getCells().filter(x => x.isLink() && x.attributes.source.componentType == 0 && x.attributes.target.componentType == 1));
                  let targetActualLink = selectedLink.attributes.target.id;
                  targetId.pop();
                  let existsTarget = targetId.find(x => x == targetActualLink)
                  if (!existsTarget) {
                    targetId.push(targetActualLink)
                  }
                  else {
                    selectedLink.remove();
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Cannot link two DataSources with one Transformation`
                    });
                  }
                }
                if (selectedLink &&
                  selectedLink.attributes.source.componentType == WorkflowItemComponentType.Transformation &&
                  selectedLink.attributes.target.componentType == WorkflowItemComponentType.Aggregation
                ) {
                  let sourceIdT = this.findSourceIdOfLinks(this.rappid.graph.getCells().filter(x => x.isLink() && x.attributes.source.componentType == 1 && x.attributes.target.componentType == 2))
                  let c = selectedLink.attributes.source.id
                  sourceIdT.pop()
                  let exists = sourceIdT.find(x => x === c);
                  if (!exists) {
                    sourceIdT.push(c)
                    if (dsTypesUp &&
                      dsTypesDown &&
                      dsTypesUp.length > 0 &&
                      dsTypesDown.length > 0) {
                      let firstItemUp = dsTypesUp[0];
                      let firstItemDown = dsTypesDown[0];
                      if (firstItemUp != firstItemDown) {
                        selectedLink.remove();
                        sourceIdT.pop()
                      }
                    }
                  }
                  else {
                    selectedLink.remove()
                    this.messageService.sendMessage({
                      type: MessageType.Error,
                      text: `Cannot link Transformation with two Aggregations`
                    });
                  }
                }
              }
            }

          }
        }
      }
    });
  }
  private updateWorkflowItemPropertiesPanel(result: any) {
    if (isNullOrUndefined(result)) {
      this.componentType = null;
      return;
    }
    const id = result.id;
    const label = result.attributes.attrs.label || result.attributes.attrs.text;
    const displayText = label.text;
    const customData = this.rappid.getCustomData(result);
    this.selectedWorkflowItem = new WorkflowItem(id, displayText, customData);
    this.componentType = this.selectedWorkflowItem.customData.componentType;
    const startComponent = this.rappid.graph
      .getCells()
      .filter(x => !x.isLink() && x.attributes.customData.componentType == 5);
    if (startComponent.length > 1) {
      this.rappid.graph.removeCells(result);
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `You already have a pipeline.`
      });
    }

    if (startComponent.length < 1) {
      this.rappid.graph.removeCells(result);
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `You should have a Start in your pipeline`
      });
    }
    this.selectedWorkflowItem.customData.pipelineId = this.treeViewComponent.selectedItem.id;
    this.workflowItemService.itemUpdated.next(this.selectedWorkflowItem);
  }
  public changeWorkflowItemTitle(title: string) {
    let currentCell = this.rappid.graph.getCell(this.selectedWorkflowItem.id);
    currentCell.attr("label/text", title);
  }

  public saveWorkflow(workflowItemForSave: WorkflowItem) {
    HighLighGraphHelper.highlightGraph(
      this.rappid.graph,
      HighLighGraphHelper.defaultColor
    );
    let existTitle = this.rappid.graph
      .getCell(this.selectedWorkflowItem.id)
      .attr("label/text");
    let existJsonMetadata = this.selectedWorkflowItem.customData.jsonMetadata;
    this.changeWorkflowItemTitle(
      workflowItemForSave.text ? workflowItemForSave.text : existTitle
    );
    var isLinked = this.rappid.graph
      .getCells()
      .find(
        x => x.isLink() && x.attributes.target.id == workflowItemForSave.id
      );

    this.rappid.graph.getCells().forEach(element => {
      if (
        !element.isLink() &&
        element.attributes.customData.componentType != 5 &&
        element.id != this.selectedWorkflowItem.customData.id &&
        element.attributes.customData.jsonMetadata == ""
      ) {
        element.remove();
      }
    });

    this.selectedWorkflowItem.customData.jsonMetadata =
      workflowItemForSave.customData.jsonMetadata;
    if (isLinked) {
      this.workflowsService
        .savePipelineItem(
          this.selectedWorkflowItem.id,
          this.selectedWorkflowItem.customData.pipelineId,
          JSON.stringify(this.rappid.getGraphJson())
        )
        .subscribe(res => {
          if (res) {
            this.messageService.sendMessage({
              type: MessageType.Info,
              text: `Workflow item is saved!`
            });
            this.rapidEventsService.hasUnSavedChanges = false;
            this.componentSavedStatus = new String(
              ComponentSavedStatus.Saved.toString()
            ) as string;
          } else {
            this.changeWorkflowItemTitle(existTitle);
            this.selectedWorkflowItem.customData.jsonMetadata = existJsonMetadata;
            this.componentSavedStatus = new String(
              ComponentSavedStatus.UnSavedChanges.toString()
            ) as string;
          }
        });
    } else {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `Cannot save unlinked components.`
      });
      this.componentSavedStatus = new String(
        ComponentSavedStatus.UnSavedChanges.toString()
      ) as string;
    }
    HighLighGraphHelper.highLightFullDesignedGraph(this.rappid.graph);
  }
  public confirmDeleteWorkFlowItems(result: Array<any>) {
    HighLighGraphHelper.highlightGraph(
      this.rappid.graph,
      HighLighGraphHelper.defaultColor
    );
    this.openedDeleteWorkFlowItemWindow = false;
    let existGraph: string;

    let graphCells = this.rappid.graph.getCells();
    var notSelectedChild = result.some(function (element) {
      let links = graphCells.filter(
        x => x.isLink() && x.attributes.source.id == element.id
      );
      let existNotSelectedChild =
        links &&
        links.some(
          e =>
            e.attributes.target &&
            !result.find(x => x.id == e.attributes.target.id)
        );
      return existNotSelectedChild == true;
    });
    if (notSelectedChild) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `Cannot delete components that have other components depending on them/ linked.`
      });
    } else {
      result.forEach(element => {
        if (
          element.attributes &&
          element.attributes.customData &&
          element.attributes.customData.componentType !=
          WorkflowItemComponentType.Start
        ) {
          existGraph = JSON.stringify(this.rappid.getGraphJson());
          this.rappid.graph.removeCells(element);
          let itemCustomData = this.rappid.getCustomData(element);
          let deletedLinkT = graphCells.find(x => x.isLink() && x.attributes.source.componentType == 0 && x.attributes.target.id == itemCustomData.id)
          let deletedLinkA = graphCells.find(x => x.isLink() && x.attributes.source.componentType == 1 && x.attributes.target.id == itemCustomData.id)
          if (deletedLinkT) {
            let index = this.sourceC.indexOf(deletedLinkT.attributes.source.id)
            this.sourceC.splice(index, 1)
          }
          if (deletedLinkA) {
            let index = this.sourceT.indexOf(deletedLinkA.attributes.source.id)
            this.sourceT.splice(index, 1)
          }
          this.workflowsService
            .deletePipelineItem(
              itemCustomData.id,
              itemCustomData.pipelineId,
              JSON.stringify(this.rappid.getGraphJson())
            )
            .subscribe(res => {
              if (res) {
                this.messageService.sendMessage({
                  type: MessageType.Info,
                  text: `Workflow item is deleted!`
                });
                this.rapidEventsService.itemSelected.next(null);
                this.rapidEventsService.itemsForDelete.next(null);
              } else {
                this.rappid.loadGraphJson(JSON.parse(existGraph));
              }
              HighLighGraphHelper.highLightFullDesignedGraph(this.rappid.graph);
            });
        }
      });
    }
  }
  public cancelDeleteWorkFlowItem() {
    this.openedDeleteWorkFlowItemWindow = false;
  }

  private loadWorkflow(treeViewItem: TreeViewItem) {
    if (true) {
      this.isWorkflowChanged = false;
      this.designService.getPipelineById(treeViewItem.id).subscribe(result => {
        let jsonParsed;
        jsonParsed =
          (result && result.designerJson && JSON.parse(result.designerJson)) ||
          [];
        if (
          jsonParsed.length === 0 ||
          (jsonParsed.cells && jsonParsed.cells.length === 0)
        ) {
          this.clearGraph();
        } else {
          this.rappid.loadGraphJson(jsonParsed);
          this.rappid.layoutGraph(false);
          HighLighGraphHelper.highLightFullDesignedGraph(this.rappid.graph);
        }
      });
    }
  }

  private clearGraph() {
    this.rappid.loadGraphJson({ cells: [] });
  }
  disableTreeView(state: boolean) {
    this.treeViewComponent.disable(state);
  }
  private setEditMode(editMode: boolean) {
    this.editMode = editMode;
    this.rappid.setEditMode(this.editMode);
    this.disableTreeView(this.editMode);
    this.rappid.paper.setInteractivity(this.editMode);

    if (this.editMode) {
      this.rappid.keyboardService.keyboard.enable();
    } else {
      this.rappid.keyboardService.keyboard.disable();
    }
    this.workflowItemService.workflowEditable.next(this.editMode);
  }
  treeViewItemSelected(treeViewItem: TreeViewItem) {
    this.componentType = undefined;
    if (
      !isNullOrUndefined(this.selectedTreeViewItem) &&
      this.selectedTreeViewItem.id === treeViewItem.id
    ) {
      return;
    }
    this.selectedTreeViewItem = treeViewItem;
    this.workflowToolbarComponent.updateToolbar(treeViewItem);
    this.loadWorkflow(treeViewItem);
  }
  treeViewItemCreated(item: TreeViewItem) {
    this.treeViewComponent.addTreeViewElement(item);
  }
  treeViewItemRenamed(item: TreeViewItem) {
    this.treeViewComponent.renameTreeViewElement(item);
  }
  treeViewItemDeleted(id: string) {
    this.treeViewComponent.deleteTreeViewElement(id);
    this.rappid.unSelectItemsFromGraph();
    this.clearGraph();
  }
  startEditClicked() {
    this.rappid.unSelectItemsFromGraph();
    this.setEditMode(true);
    this.workflowToolbarComponent.resetMenuForEdit(MenuAction.StartEdit);
  }
  stopEditClicked() {
    if (
      this.rappid.isEditMode &&
      this.rappid.selectedElement &&
      this.rappid.rapidEventService.hasUnSavedChanges
    ) {
      this.rappid.openConfirmUnSavedDialog().subscribe((res: any) => {
        let selectedValue = res.value || false;
        if (selectedValue) {
          this.rappid.rapidEventService.hasUnSavedChanges = false;
          this.stopEditMode();
        } else {
          this.rappid.selectActualSelectedCellInGraph();
        }
      });
    } else {
      this.stopEditMode();
    }
  }
  private stopEditMode() {
    this.rappid.unSelectItemsFromGraph();
    this.loadWorkflow(this.selectedTreeViewItem);
    this.setEditMode(false);
    this.workflowToolbarComponent.resetMenuForEdit(MenuAction.StopEdit);
  }
  private onChangeLink(params) {
    if (params.attributes.source && params.attributes.source.id) {
      const sourceId = params.attributes.source.id;
      const sourceCell = this.rappid.graph.getCell(sourceId);
      params.attributes.source.componentType =
        sourceCell.attributes.customData.componentType;
    }
    if (params.attributes.target && params.attributes.target.id) {
      const targetId = params.attributes.target.id;
      const targetCell = this.rappid.graph.getCell(targetId);
      params.attributes.target.componentType =
        targetCell.attributes.customData.componentType;
    }
  }

  findDatasourceType(component, cells) {
    let dsTypes = [];
    if (component.attributes.customData.componentType == 0) {
      let metaData = JSON.parse(component.attributes.customData.jsonMetadata);
      dsTypes.push(metaData.isTarget ? "T" : "L");
      return dsTypes;
    } else {
      let links = cells.filter(
        x => x.isLink() && x.attributes.target.id == component.id
      );
      links.forEach(link => {
        let upperElementId = link.attributes.source.id;
        let upperElement = cells.find(item => item.id == upperElementId);
        let conectedDatasources = this.findDatasourceType(upperElement, cells);
        dsTypes.push(...conectedDatasources);
        return dsTypes;
      });
    }
    return dsTypes;
  }

  findSourceIdOfLinks(cells) {
    let sourceId = [];
    let links = cells.filter(x => x.isLink())
    links.forEach(link => {
      let c = link.attributes.source.id
      sourceId.push(c)
      return sourceId
    });
    return sourceId
  }

  findTargetIdOfLinks(cells) {
    let targetId = [];
    let links = cells.filter(x => x.isLink())
    links.forEach(link => {
      let c = link.attributes.target.id
      targetId.push(c)
      return targetId
    });
    return targetId
  }
}
