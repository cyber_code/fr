import {
  ColumnItemComparison,
  ComparisonRule
} from "./../../../models/comparison-rules.model";
import { DataSourceType } from "./../../../models/data-source.model";
import { Component, Output, EventEmitter, Input } from "@angular/core";
import { ColumnItem } from "src/app/body/models/comparison-rules.model";
import {
  KeyValueItem,
  TreeNodeElement
} from "src/app/body/models/properties-container.model";
import { PipelineItemColumnDataType } from "src/app/body/models/properties-container.enum";

@Component({
  selector: "app-comparison-rule-item",
  templateUrl: "./comparison-rule-item.component.html",
  styleUrls: ["./comparison-rule-item.component.css"]
})
export class ComparisonRuleItemComponent {
  @Input() _legacyColumns: ColumnItem[] = [];
  @Input() _targetColumns: ColumnItem[] = [];
  @Input() _mathOperators: KeyValueItem[] = [];
  @Input() _compareOperators: KeyValueItem[] = [];
  @Output() ruleAdded: EventEmitter<ComparisonRule> = new EventEmitter();

  /**
   * Types of item:
   *  1=Column (Expression)
   *  2=Operator
   */

  // Legacy
  legacyExpression: string;
  legacySelectedItems: ColumnItemComparison[] = [];
  legacyLastItem: number = 2;

  // Target
  targetExpression: string;
  targetSelectedItems: ColumnItemComparison[] = [];
  targetLastItem = 2;

  // General
  tree: TreeNodeElement = new TreeNodeElement();
  // compareOperations = ['=', '<', '>', '<>', '<=', '>=']
  // mathOperations = ['+', '-', '*', '/']
  public dataSourceType = DataSourceType;
  public get isValidExpression(): boolean {
    // Check if is valid expression before adding to compare rules
    return (
      this.tree != undefined &&
      this.tree.value != undefined &&
      this.legacySelectedItems.length != 0 &&
      this.targetSelectedItems.length != 0 &&
      this.legacyLastItem != 2 &&
      this.targetLastItem != 2
    );
  }
  constructor() {
    this.initTree();
  }

  initTree() {
    this.tree.isOperator = true;
    // this.tree.value = undefined;
    this.clearElements(1);
    this.clearElements(2);
  }

  compareOperatorChanged(e: KeyValueItem) {
    this.tree.value = e.key;
    this.tree.expression = e.value;
  }

  private prepareExpression(source: number) {
    let s: string;
    switch (source) {
      case 1:
        this.legacySelectedItems.forEach(i => {
          s = `${s || ""} ${i.expression || ""}`;
        });
        this.legacyExpression = s;
        break;
      case 2:
        this.targetSelectedItems.forEach(i => {
          s = `${s || ""} ${i.expression || ""}`;
        });
        this.targetExpression = s;
      default:
        break;
    }
  }

  insertElement(clicked: any, type: number, source: number) {
    let c: ColumnItemComparison = new ColumnItemComparison();
    let value = JSON.parse(JSON.stringify(clicked));
    c.isOperator = type == 2;
    if (c.isOperator) {
      c.expression = value.value;
      c.value = JSON.stringify(+value.key);
    } else if (!c.isOperator) {
      c.expression = value.__columnName;
     // delete value.__columnName;
      //delete value.columnName;
      c.value = JSON.stringify(value);
      c.dataType = value.dataType;
    }

    switch (source) {
      case 1:
        this.legacySelectedItems.push(c);
        this.legacyLastItem = c.isOperator ? 2 : 1;
        break;
      case 2:
        this.targetSelectedItems.push(c);
        this.targetLastItem = c.isOperator ? 2 : 1;
      default:
        break;
    }
    this.prepareExpression(source);
  }
  dataSourceColumnIsEnabled(
    dataSourceColumn: ColumnItem,
    dataSourceType: DataSourceType
  ) {
    let lastItemLegacy: ColumnItemComparison =
      this.legacySelectedItems.length > 0
        ? this.legacySelectedItems[this.legacySelectedItems.length - 1]
        : null;
    let lastItemTarget: ColumnItemComparison =
      this.targetSelectedItems.length > 0
        ? this.targetSelectedItems[this.targetSelectedItems.length - 1]
        : null;
    if (dataSourceType == DataSourceType.Legacy) {
      if (
        lastItemLegacy == null &&
        (lastItemTarget == null ||
          dataSourceColumn.dataType == lastItemTarget.dataType ||
          (lastItemTarget.isOperator &&
            dataSourceColumn.dataType == PipelineItemColumnDataType.Numeric))
      ) {
        return true;
      }
      if (
        lastItemLegacy &&
        lastItemLegacy.isOperator &&
        dataSourceColumn.dataType == PipelineItemColumnDataType.Numeric &&
        (lastItemTarget == null ||
          lastItemTarget.isOperator ||
          lastItemTarget.dataType == PipelineItemColumnDataType.Numeric)
      ) {
        return true;
      }
    } else {
      if (
        lastItemTarget == null &&
        (lastItemLegacy == null ||
          dataSourceColumn.dataType == lastItemLegacy.dataType ||
          (lastItemLegacy.isOperator &&
            dataSourceColumn.dataType == PipelineItemColumnDataType.Numeric))
      ) {
        return true;
      }
      if (
        lastItemTarget &&
        lastItemTarget.isOperator &&
        dataSourceColumn.dataType == PipelineItemColumnDataType.Numeric &&
        (lastItemLegacy == null ||
          lastItemLegacy.isOperator ||
          lastItemLegacy.dataType == PipelineItemColumnDataType.Numeric)
      ) {
        return true;
      }
    }
    return false;
  }
  mathOperatorAreEnabled(dataSourceType: DataSourceType) {
    let lastItem: ColumnItemComparison;
    if (dataSourceType == DataSourceType.Legacy) {
      lastItem =
        this.legacySelectedItems.length > 0
          ? this.legacySelectedItems[this.legacySelectedItems.length - 1]
          : null;
    } else {
      lastItem =
        this.targetSelectedItems.length > 0
          ? this.targetSelectedItems[this.targetSelectedItems.length - 1]
          : null;
    }
    if (
      lastItem != null &&
      lastItem.dataType == PipelineItemColumnDataType.Numeric
    ) {
      return true;
    }
    return false;
  }

  setCompareOperation(value: string) {
    this.tree.value = value;
  }

  clearElements(source: number) {
    switch (source) {
      case 1:
        this.legacySelectedItems = [];
        this.tree.left = undefined;
        this.legacyLastItem = 2;
        break;
      case 2:
        this.targetSelectedItems = [];
        this.tree.right = undefined;
        this.targetLastItem = 2;
      default:
        break;
    }
    this.prepareExpression(source);
  }

  addRule() {
    let tree: TreeNodeElement = JSON.parse(JSON.stringify(this.tree));
    delete tree.expression;
    tree.left = this.buildTree(
      this.legacySelectedItems.map(x => {
        let comparisonElement = {
          isOperator: x.isOperator,
          value: x.value,
          left: null,
          right: null
        } as TreeNodeElement;
        return comparisonElement;
      }),
      tree.left
    );
    tree.right = this.buildTree(
      this.targetSelectedItems.map(x => {
        let comparisonElement = {
          isOperator: x.isOperator,
          value: x.value,
          left: null,
          right: null
        } as TreeNodeElement;
        return comparisonElement;
      }),
      tree.right
    );

    /**
     * Expression needed for apperance reasons only. Needs to be deleted as property before
     * saving rules on parent component
     */
    tree.expression = `${this.legacyExpression || ""} ${this.tree.expression ||
      ""} ${this.targetExpression || ""}`;
    let itemsDataType = this.legacySelectedItems[0]
      ? this.legacySelectedItems[0].dataType
      : PipelineItemColumnDataType.Text;
    let comparisonRule = {
      ComparisonRuleElement: tree,
      itemsDataType: itemsDataType
    } as ComparisonRule;
    this.ruleAdded.emit(comparisonRule);
    this.initTree();
  }
  addAutomaticRule() {
    this.clearElements(1);
    this.clearElements(2);
    this._legacyColumns.forEach(legacyColumn => {
      let targetColumn = this._targetColumns.find(
        x => x.columnName == legacyColumn.columnName
      );
      if (targetColumn) {
        this.insertElement(legacyColumn, 1, 1);
        this.compareOperatorChanged(
          this._compareOperators.find(x => x.value == "=")
        );
        this.insertElement(targetColumn, 1, 2);
        this.addRule();
      }
    });
  }
  buildTree(elements: TreeNodeElement[], tree): TreeNodeElement {
    let result = tree ? JSON.parse(JSON.stringify(tree)) : undefined;
    elements.forEach(item => {
      result = this.generateBinaryTree(item, result);
    });
    return result;
  }

  generateBinaryTree(
    node: TreeNodeElement,
    tree: TreeNodeElement
  ): TreeNodeElement {
    if (node.isOperator == true) {
      if (tree == undefined) {
        tree = node;
      } else {
        let temp: TreeNodeElement = JSON.parse(JSON.stringify(tree));
        tree = node;
        tree.left = temp;
      }
    } else {
      if (tree == undefined) {
        tree = node;
      } else {
        tree.right = node;
      }
    }
    return tree;
  }
}
