import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { StartService } from "src/services/start.service";
import { Subscription, Observable } from "rxjs";
import * as signalR from "@aspnet/signalr";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { ReportsService } from "src/app/body/services/reports.service";

interface ExecutionModel {
  id: string;
  currentStatus: string;
  timeElapsed: string;
  description: string;
  rowsImported: string;
}

@Component({
  selector: "app-start",
  templateUrl: "./start.component.html",
  styleUrls: ["./start.component.css"]
})
export class StartComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  // isStrted: boolean = false;
  hasError = false;
  pipeLine: any;
  progressbars: any[] = [];
  pipelineId: string = "";
  @Input() getWorkFlowItem: any;
  itemsToShow: any[] = [];
  private hubConnection: signalR.HubConnection;
  private apiUrl: string;
  isFinished: boolean = false;
  isStarted: boolean = false;
  executionModels: ExecutionModel[] = [];
  inProgressSize: number = 0;
  currentIndex: number = 0;
  startedTime: Date = new Date();
  endTime: Date = new Date();
  timedDiff: string = "00:00:00";
  isExecutionOngoing = false;
  reportsBeingDownloaded = 0;
  startedItems = [];
  downloads: Observable<any>[] = [];
  downloadLink = "";
  public reports: any[];
  public report: any;
  constructor(
    private startService: StartService,
    private configurationService: ConfigurationService,
    private reportService: ReportsService
  ) {
    this.apiUrl = `${configurationService.serverSettings.apiUrl}`;
  }

  ngOnInit() {
    // Get one random element and get it's pipelineId. PipelineId is the same for all items
    let randomElement = this.getWorkFlowItem.find(
      x =>
        x.attributes &&
        x.attributes.customData &&
        x.attributes.customData.pipelineId
    );
    this.pipelineId = randomElement.attributes.customData.pipelineId;
  }

  getTimeSpan(start: Date, end: Date) {
    var date1 = start.getTime();
    var date2 = end.getTime();
    // var msec = Math.round((date2 - date1) / 1000);
    var msec = Math.round(((date2 - date1) / 1000) % 60);
    var mins = Math.floor(msec / 60000);
    var hrs = Math.floor(mins / 60);
    var days = Math.floor(hrs / 24);
    var yrs = Math.floor(days / 365);

    return (
      (hrs <= 9 ? "0" + hrs : hrs) +
      ":" +
      (mins <= 9 ? "0" + mins : mins) +
      ":" +
      (msec <= 9 ? "0" + msec : msec)
    );
  }

  startItem(i: any) {
    console.log("Started:", i);
    let item = this.getWorkFlowItem.find(x => x.id == i.id);
    if (item) {
      if (item.attributes.customData.componentType == 4) {
        let customData = JSON.parse(item.attributes.customData.jsonMetadata);
        item.name = customData.name;
        item.executionData = i;
        item.status = -1;
        item.type = "report";
        item.reportType = customData.reportType;
        this.startedItems.push(item);
      } else {
        let customData = JSON.parse(item.attributes.customData.jsonMetadata);
        item.name = customData.name;
        item.executionData = i;
        item.status = -1;
        item.type = "component";
        this.startedItems.push(item);
      }
    }
  }

  itemExecuted(i) {
    console.log("Executed:", i);
    let item = this.startedItems.find(x => x.id == i.id);
    if (item) {
      // setTimeout(() => {
      item.status = i.status;
      item.rowsExecuted = i.numberOfRows || 0;
      item.elapsed = this.getTimeSpan(
        new Date(i.startTime),
        new Date(i.endTime)
      );
      // }, 2000)
    }
  }

  async startExecution() {
    this.startedTime = new Date();
    this.startedItems = [];
    this.isStarted = true;
    this.hasError = false;
    this.isExecutionOngoing = true;

    this.hubConnection = await new signalR.HubConnectionBuilder()
      .withUrl(this.apiUrl + "notificationhub", {
        transport: signalR.HttpTransportType.LongPolling
        // accessTokenFactory: () => this.sessionService.getToken()
      })
      .build();

    await this.hubConnection
      .start()
      .then(c => {
        this.hubConnection.on("PipelineItemStarted", data =>
          this.startItem(data)
        );
        this.hubConnection.on("PipelineItemExecuted", data =>
          this.itemExecuted(data)
        );
        this.startService.execute(this.pipelineId).subscribe(res => {
          console.log("Endresult:", res);
          this.isFinished = true;
          this.isExecutionOngoing = false;
          this.hubConnection.stop();
          this.endTime = new Date();
          this.timedDiff = this.getTimeSpan(this.startedTime, this.endTime);
          // this.startedItems = this.startedItems.filter(x => x.type == 'component')
          if (res.status == true) {
            this.startedItems = this.startedItems.filter(
              x => x.type == "component"
            );
            this.reports = res.reports;
            this.reports.forEach(element => {
              let item = {
                id: element.reportId,
                name: "Report",
                reportType: element.reportType,
                reportTemplateType: element.reportTemplateType,
                status: 0,
                type: "report",
                downloadLink: `${this.apiUrl}Design/DownloadReportById/?PipelineId=${this.pipelineId}&ReportId=${element.reportId}`
              };
              this.startedItems.push(item);
            });
          } else {
            this.startedItems;
            this.hasError = true;
          }
        });
      })
      .catch(err => console.log("Error while starting connection: " + err));

    //////////////////////////////////////////////////////
    // var isvalidJSONObject = true;
    // this.executionModels = [];
    // try {
    //   for (let attr = 0; attr <= this.getWorkFlowItem.length - 1; attr++) {
    //     try {
    //       var customData = this.getWorkFlowItem[attr].attributes.customData;
    //       // if (customData.componentType == 2) {

    //       if (this.pipelineId == "") {
    //         this.pipelineId = customData.pipelineId;
    //       }
    //       console.log(this.pipelineId)

    //       isvalidJSONObject = true;
    //       break;
    //       // }
    //     } catch {}
    //   }
    // } catch {
    //   isvalidJSONObject = false;
    // }

    // if (isvalidJSONObject) {
    //   // console.log(this.pipelineId);

    //   this.subscriptions.add(
    //     this.startService
    //       .getPielineById(this.pipelineId)
    //       .subscribe(response => {
    //         this.pipeLine = response;

    //         if (this.pipeLine.result.designerJson.length == 0) {
    //           return;
    //         }
    //         var jsonObject = JSON.parse(this.pipeLine.result.designerJson);
    //         //console.log(jsonObject);

    //         this.progressbars = [];
    //         for (let i = 0; i <= jsonObject.cells.length - 1; i++) {
    //           var item = jsonObject.cells[i];
    //           if (item.type == "standard.Rectangle") {
    //             var jsonItem;
    //             var isJsonvalid = true;

    //             try {
    //               jsonItem = JSON.parse(item.customData.jsonMetadata);
    //               isJsonvalid = true;

    //               //console.log(jsonItem);
    //             } catch {
    //               isJsonvalid = false;
    //             }

    //             if (isJsonvalid) {
    //               var addItem = Object.assign({
    //                 id: "jsonItem_" + jsonItem.id,
    //                 currentStatus: "In progress",
    //                 timeElapsed: "10min",
    //                 description: jsonItem.name,
    //                 rowsImported: "4k"
    //               });
    //               this.executionModels.push(addItem);
    //               // this.executionModels[index].currentStatus = 'Finished';

    //               //this.progressbars.push(jsonItem.name);
    //             }
    //           }
    //         }
    //       })
    //   );

    // }

    // this.showHideProgress();

    // setInterval(() => {
    //   if(  this.isStrted == false){
    //     return;
    //   }
    //   for (let i = 0; i <= this.executionModels.length - 1; i++) {
    //     if (this.executionModels[i].currentStatus != "Finished") {
    //       this.executionModels[i].currentStatus = "Finished";

    //       // var id = this.executionModels[i].id;//.split('_')[1];
    //       // var elem = document.getElementById('progressDivjsonItem_' + id);
    //       // var width = 10;
    //       // var idd = setInterval(function () {
    //       //   if (width >= 100) {
    //       //     clearInterval(idd);
    //       //   } else {
    //       //     width++;
    //       //   //  elem.style.width = width + '%';
    //       //  //   elem.innerHTML = width * 1 + '%';
    //       //   }
    //       // }, 10);

    //       // debugger;
    //       // var id = this.executionModels[i].id;//.split('_')[1];
    //       // setInterval(() => {
    //       //   this.inProgressSize = 0;
    //       //   debugger;
    //       //   if (this.inProgressSize <= 33) {
    //       //     this.inProgressSize++;
    //       //     var progressDiv = document.getElementById('progressDivjsonItem_' + id);
    //       //     //progressDiv.style.display = '';
    //       //     progressDiv.style.height = this.inProgressSize + 'px';
    //       //   }
    //       //   else {
    //       //     clearInterval(this.inProgressSize);
    //       //   }
    //       // }, 10);

    //       //  if (i == 1) {
    //       //     this.executionModels[i].currentStatus = 'Failed';
    //       //  }

    //       // debugger;
    //       // var xpath = "//*[text()='" + this.executionModels[i].description + "']";
    //       // //var doc = document.getElementsByClassName('app-body');
    //       // var matchingElement =  document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    //       // var getParent = matchingElement.parentElement;//.parentElement;
    //       // var found = getParent.outerHTML.split('stroke');

    //       // // document.getElementById(getParent.getAttribute('id')).insertAdjacentHTML("afterend",
    //       // // html);

    //       // getParent.parentElement.querySelectorAll('[stroke="#c8d6e5"]')[0].style.stroke = '#62D57F';

    //       //debugger;
    //       var id = this.executionModels[i].id.split("_")[1];
    //       var obj = this.executionModels[i].id.split("_");

    //       // var connectionLine = document.getElementById((document.querySelectorAll("g[model-id='" + id + "']")[0].id.split('_')[0] + '_') +
    //       //   (Number(document.querySelectorAll("g[model-id='" + id + "']")[0].id.split('_')[1]) + 2)).lastElementChild;

    //       // var itemConnLine = <HTMLElement>connectionLine;
    //       // itemConnLine.style.stroke =
    //       //   this.executionModels[i].currentStatus == 'Finished' ? '#ff4500' : (this.executionModels[i].currentStatus == 'Failed' ? '#FF0000' : '#C8D6E5');

    //       var modelElement = <HTMLElement>(
    //         document.querySelectorAll("g[model-id='" + id + "']")[0]
    //           .childNodes[0]
    //       );
    //       modelElement.style.stroke =
    //         this.executionModels[i].currentStatus == "Finished"
    //           ? "#ff4500"
    //           : this.executionModels[i].currentStatus == "Failed"
    //           ? "#FF0000"
    //           : "#C8D6E5";
    //       this.endTime = new Date();

    //       this.timedDiff = this.getTimeSpan();//(val).toString();//Math.abs(this.startedTime - this.endTime);
    //       break;
    //     }

    //     if (this.isFinished == false) {
    //       this.isFinished = i == this.executionModels.length - 2;
    //       this.timedDiff = this.getTimeSpan();
    //     }
    //   }
    // }, 2000);

    /////////////
  }

  stopExecution() {
    this.subscriptions.unsubscribe();
    if (
      this.hubConnection &&
      this.hubConnection.state == signalR.HubConnectionState.Connected
    ) {
      this.hubConnection.stop();
    }
    this.startedItems = [];
    this.isStarted = false;
    this.isExecutionOngoing = false;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if (
      this.hubConnection &&
      this.hubConnection.state == signalR.HubConnectionState.Connected
    ) {
      this.hubConnection.stop();
    }
  }
}
