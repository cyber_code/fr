import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EditAggregationElement } from "src/app/body/models/aggregation.model";
import { FormService } from "src/services/form.service";

@Component({
  selector: "app-aggregation-form",
  templateUrl: "./aggregation-form.component.html",
  styleUrls: ["./aggregation-form.component.css"]
})
export class AggregationFormComponent implements OnInit {
  public listFunctions: string[];
  public submittedForm: boolean = false;

  @Input() public isEditForm = false;
  @Input() filteredNumericField: string;
  @Input() public editAggregationElement: EditAggregationElement;
  @Input() set aggregationFunctionNames(aggregationFunctions: string[]) {
    if (!this.listFunctions || this.listFunctions.length == 0) {
      this.listFunctions = aggregationFunctions;
    }
  }
  @Input() public readOnly;
  @Input() public componentId;
  @Output() addAggregationElement = new EventEmitter<EditAggregationElement>();
  @Output() saveEditAggregationElement = new EventEmitter<any>();
  @Output() cancelEditAggregationElement = new EventEmitter<
    EditAggregationElement
  >();

  public aggregationElementForm: FormGroup = this.formBuilder.group({
    aggregationFunction: [null, Validators.required],
    columnName: [null, Validators.required],
    alias: [null, Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    public formService: FormService
  ) {}

  ngOnInit() {
    if (this.readOnly) {
      this.aggregationElementForm.disable();
    }
  }

  public addConfiguredAggregation() {
    this.submittedForm = true;

    if (this.aggregationElementForm.valid) {
      this.editAggregationElement.alias = this.editAggregationElement.alias.replace(
        /\s/g,
        "_"
      );
      this.addAggregationElement.emit(
        Object.assign({}, this.editAggregationElement)
      );

      this.resetAggregationElementForm();
    } else {
      this.formService.validateAllFormFields(this.aggregationElementForm);
    }
  }
  private resetAggregationElementForm() {
    this.submittedForm = false;
    this.aggregationElementForm.reset();
  }

  public editConfiguredAggregation() {
    this.submittedForm = true;
    if (this.aggregationElementForm.valid) {
      this.editAggregationElement.alias = this.editAggregationElement.alias.replace(
        /\s/g,
        "_"
      );
      this.saveEditAggregationElement.emit(
        Object.assign({}, this.editAggregationElement)
      );
      this.resetAggregationElementForm();
    } else {
      this.formService.validateAllFormFields(this.aggregationElementForm);
    }
  }
  public cancelConfiguredAggregation() {
    this.resetAggregationElementForm();
    this.cancelEditAggregationElement.emit();
  }
}
