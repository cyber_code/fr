import { EditTransformationElement } from "./../../../models/transformation.model";
import { FormService } from "src/services/form.service";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DataFileItem } from "src/app/body/models/properties-container.model";
import { AutoUnsubscribe } from "take-while-alive";
import { WorkflowItemService } from "src/app/body/services/workflow-item.service";
import { WorkflowItem } from "src/models/workflow-item";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { UploadMappingFileComponent } from "../upload-mapping-file/upload-mapping-file.component";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";

@Component({
  selector: "app-transformation-cu-form",
  templateUrl: "./transformation-cu-form.component.html",
  styleUrls: ["./transformation-cu-form.component.css"]
})
@AutoUnsubscribe()
export class TransformationCuFormComponent implements OnInit {
  @Input() public isEditForm = false;
  @Input() public editTransformationElement: EditTransformationElement;
  @Input() public componentId;
  @Input() public readOnly;
  @Input()
  set transformationFunctionNames(tranformationFunctions: Array<string>) {
    if (!this.listFunctions || this.listFunctions.length == 0) {
      this.listFunctions = tranformationFunctions;
    }
  }
  @Input() graphCellss;
  @Output() addTransformationElement = new EventEmitter<
    EditTransformationElement
  >();
  @Output() saveEditTransformationElement = new EventEmitter<any>();
  @Output() cancelEditTransformationElement = new EventEmitter<
    EditTransformationElement
  >();
  public listFunctions: Array<string>;
  public transformationElementForm: FormGroup = this.formBuilder.group({
    functions: [Validators.required],
    filesUploaded: [null, Validators.required],
    key: [null, Validators.required],
    mappingFileSeparator: [null]
  });
  public submittedForm: boolean = false;
  public workflowItem: WorkflowItem;
  public workflowItemData: WorkflowItemCustomData;
  @ViewChild("uploadDataFileComp")
  uploadDataFileComp: UploadMappingFileComponent;
  constructor(
    private formBuilder: FormBuilder,
    public formService: FormService,
    private workflowItemService: WorkflowItemService,
    private messageService: MessageService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.workflowItemData = this.workflowItem.customData;
  }
  ngOnInit() {
    if (this.readOnly) {
      this.transformationElementForm.disable();
    }
  }
  public addMappingFileItem(mappingFileItem: DataFileItem) {
    this.editTransformationElement.fileId = mappingFileItem.id;
    this.editTransformationElement.uploadedFilesInfo =
      mappingFileItem.uploadedFilesInfo;
    this.editTransformationElement.fileName =
      mappingFileItem.uploadedFilesInfo[0].name;
    this.editTransformationElement.sourceKey =
      mappingFileItem.pipelineItemColumns[0].name;
    this.editTransformationElement.targetKey =
      mappingFileItem.pipelineItemColumns[1].name;
  }
  public addConfiguredTransformation() {
    this.submittedForm = true;
    var isLinked = this.graphCellss.find(
      x => x.isLink() && x.attributes.source.id == this.workflowItem.id
    );
    if (isLinked) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `Check and redo the work on the next linked components`
      });
    }
    if (this.transformationElementForm.valid) {
      this.addTransformationElement.emit(
        Object.assign({}, this.editTransformationElement)
      );
      this.resetTransformationElementForm();
    } else {
      this.formService.validateAllFormFields(this.transformationElementForm);
    }
  }
  public editConfiguredTransformation() {
    this.submittedForm = true;
    if (this.transformationElementForm.valid) {
      this.saveEditTransformationElement.emit(
        Object.assign({}, this.editTransformationElement)
      );
      this.resetTransformationElementForm();
    } else {
      this.formService.validateAllFormFields(this.transformationElementForm);
    }
  }
  public cancelConfiguredTransformation() {
    this.resetTransformationElementForm();
    this.cancelEditTransformationElement.emit();
  }
  public resetTransformationElementForm() {
    this.submittedForm = false;
    this.transformationElementForm.reset();
    this.uploadDataFileComp.resetUploadField();
    this.editTransformationElement.separator = ",";
  }
}
