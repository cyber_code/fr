import { FilterItem } from "src/app/body/models/transformation.model";
import { RapidEventsService } from "./../../../../../services/rapid-events.service";
import { TransformationService } from "./../../../services/transformation.service";
import {
  EditTransformationItem,
  EditTransformationElement,
  TransformationItem,
  TransformationElement
} from "./../../../models/transformation.model";
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ViewChild
} from "@angular/core";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { WorkflowItemService } from "src/app/body/services/workflow-item.service";
import { WorkflowItem } from "src/models/workflow-item";
import { takeWhileAlive, AutoUnsubscribe } from "take-while-alive";
import { DataSourceItem } from "src/app/body/models/data-source.model";
import { cloneDeep } from "lodash";
import { Guid } from "src/app/shared/guid";
import { FileInfo } from "@progress/kendo-angular-upload";
import { TransformationCuFormComponent } from "../transformation-cu-form/transformation-cu-form.component";
import {
  DataFileColumnItem,
  TreeNodeElement
} from "src/app/body/models/properties-container.model";
@Component({
  selector: "app-transformation",
  templateUrl: "./transformation.component.html",
  styleUrls: ["./transformation.component.css"]
})
@AutoUnsubscribe()
export class TransformationComponent implements OnInit {
  @Input()
  set graphCells(graphCells: joint.dia.Cell[]) {
    this.graphCellss = graphCells;
    var currentLink = graphCells.find(
      x => x.isLink() && x.attributes.target.id == this.workflowItem.id
    );
    var dataSourceElement = graphCells.find(
      x => currentLink && x.id == currentLink.attributes.source.id
    );
    if (
      dataSourceElement &&
      dataSourceElement.attributes &&
      dataSourceElement.attributes.customData &&
      dataSourceElement.attributes.customData.jsonMetadata
    ) {
      if (!this.getDataSourceInformation) {
        let dataSourceItem = JSON.parse(
          dataSourceElement.attributes.customData.jsonMetadata
        ) as DataSourceItem;
        this.prevConnectedDataSourceItem = dataSourceItem;
        this.editTransformationItem.dataSourceId = dataSourceItem.id;
        this.dataSourceItemColumns = dataSourceItem.dataSourceColumns
          ? dataSourceItem.dataSourceColumns
          : [];

        this.getDataSourceInformation = true;
      }
    }
  }
  @Output() saveTransformation = new EventEmitter<WorkflowItem>();
  public prevConnectedDataSourceItem: DataSourceItem = new DataSourceItem();
  public pipelineId: string;
  public transformationFunctionNames: Array<string>;
  public editTransformationElement: EditTransformationElement = new EditTransformationElement();
  public listTransformation: Array<EditTransformationElement> = [];
  public editTransformationItem: EditTransformationItem = new EditTransformationItem();
  public openedDetailsPopup: boolean = false;
  public openedEditTransformationPopup: boolean = false;
  public workflowItem: WorkflowItem;
  public workflowItemData: WorkflowItemCustomData;
  public readOnly: boolean;
  private getDataSourceInformation: boolean = false;
  public dataSourceItemColumns: Array<DataFileColumnItem>;
  public openedTransformationFilterDialogBox: boolean = false;
  graphCellss: joint.dia.Cell[];
  @ViewChild("transformationCuFormComp")
  transformationCuFormComp: TransformationCuFormComponent;
  constructor(
    private workflowItemService: WorkflowItemService,
    private transformationService: TransformationService,
    private rapidEventsService: RapidEventsService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.workflowItemData = this.workflowItem.customData;
    this.pipelineId = this.workflowItemData.pipelineId;
    this.editTransformationItem.id = this.workflowItemData.id;
    this.readOnly = !this.workflowItemService.editable;
    this.getDataSourceInformation = false;
    this.rapidEventsService.hasUnSavedChanges = false;
  }
  ngOnInit() {
    this.editTransformationElement.listKeyField = this.dataSourceItemColumns;
    this.editTransformationElement.separator = ",";
    this.transformationService
      .getTransformationFunctions()
      .pipe(takeWhileAlive(this))
      .subscribe(functionItem => {
        this.transformationFunctionNames = functionItem;
      });
    if (this.workflowItemData.jsonMetadata) {
      let jsonMetadata = JSON.parse(this.workflowItemData.jsonMetadata);
      let formatedListTransformation = jsonMetadata
        ? jsonMetadata.transformations
        : [];
      let listKeys = this.editTransformationElement.listKeyField;
      this.listTransformation = formatedListTransformation.map(function(item) {
        item.tempUiId = Guid.newGuid();
        item.key = listKeys ? listKeys.find(x => x.name == item.key) : item.key;
        item.listKeyField = listKeys;
        item.uploadedFilesInfo = [{ name: item.fileName } as FileInfo];
        return item;
      });
      this.editTransformationItem.name = jsonMetadata.name;
      this.editTransformationItem.transformationFiltersData = jsonMetadata.transformationFiltersData
        ? jsonMetadata.transformationFiltersData.map(x => {
            return {
              _tempId: Guid.newGuid(),
              filterNodeElement: x
            } as FilterItem;
          })
        : [];
    }
    this.editTransformationItem.transformationFiltersData = this
      .editTransformationItem.transformationFiltersData
      ? this.editTransformationItem.transformationFiltersData
      : [];
  }
  public onInputFieldChange(event: any) {
    this.rapidEventsService.hasUnSavedChanges = true;
  }
  public addNewEditTransformationElement(
    editTransformationElement: EditTransformationElement
  ) {
    this.editTransformationElement.tempUiId = Guid.newGuid();
    this.listTransformation.push(editTransformationElement);
    this.onInputFieldChange(true);
  }
  public editTranformationElementHandler({ dataItem }) {
    this.openedEditTransformationPopup = true;
    this.editTransformationElement = Object.assign({}, dataItem);
  }
  public removeTranformationElementHandler({ dataItem }) {
    let index = this.listTransformation.findIndex(
      x => x.tempUiId == dataItem.tempUiId
    );
    this.listTransformation.splice(index, 1);
    this.onInputFieldChange(true);
  }
  public saveEditTransformationElement(
    transformationItem: EditTransformationElement
  ) {
    let updatedIndex = this.listTransformation.findIndex(
      x => x.tempUiId == transformationItem.tempUiId
    );
    this.listTransformation[updatedIndex] = transformationItem;
    this.openedEditTransformationPopup = false;
    this.onInputFieldChange(true);
  }
  public openDetailsPopup() {
    this.openedDetailsPopup = true;
  }
  public closeDetailsPopup() {
    this.openedDetailsPopup = false;
  }
  public cancelEditConfiguredTransformation() {
    this.openedEditTransformationPopup = false;
    this.transformationCuFormComp.resetTransformationElementForm();
  }
  public closeEditTransformationPopup() {
    this.openedEditTransformationPopup = false;
    this.transformationCuFormComp.resetTransformationElementForm();
  }
  public openTransformationFilterDialogBox() {
    this.openedTransformationFilterDialogBox = true;
  }
  public closeTransformationFilterDialogBox() {
    this.openedTransformationFilterDialogBox = false;
  }

  filterAdded(e: TreeNodeElement) {
    let filter: FilterItem = new FilterItem();
    filter.filterNodeElement = JSON.parse(JSON.stringify(e));
    filter._tempId = Guid.newGuid();
    if (
      !this.editTransformationItem.transformationFiltersData.find(
        x => x.filterNodeElement.expression == e.expression
      )
    ) {
      this.editTransformationItem.transformationFiltersData.push(filter);
      this.onInputFieldChange(true);
    }
  }
  removeFilterItem(id: string) {
    this.editTransformationItem.transformationFiltersData = this.editTransformationItem.transformationFiltersData.filter(
      x => x._tempId != id
    );
    this.onInputFieldChange(true);
  }
  public saveFilters() {
    this.closeTransformationFilterDialogBox();
  }
  public saveConfiguredTransformation() {
    this.editTransformationItem.name = this.editTransformationItem.name
      ? this.editTransformationItem.name.replace(/\s/g, "_")
      : this.editTransformationItem.name;
    let workflowItemForSave: WorkflowItem = cloneDeep(this.workflowItem);
    let workflowMetadata = {
      id: this.editTransformationItem.id,
      name: this.editTransformationItem.name,
      transformations: this.listTransformation.map(function(item) {
        return {
          key: item.key.name,
          fileId: item.fileId,
          transformationFunction: item.transformationFunction,
          fileName: item.uploadedFilesInfo[0].name,
          separator: item.separator,
          sourceKey: item.sourceKey,
          targetKey: item.targetKey
        } as TransformationElement;
      }),
      transformationFiltersData: this.editTransformationItem.transformationFiltersData.map(
        x => {
          return x.filterNodeElement;
        }
      )
    } as TransformationItem;
    workflowItemForSave.text =
      "Transformation \n" + this.editTransformationItem.name;
    workflowItemForSave.customData.jsonMetadata = JSON.stringify(
      workflowMetadata
    );
    this.saveTransformation.emit(workflowItemForSave);
  }
}
