import { ReportTemplateType } from "src/app/body/models/report-type.enum";
import { ReportType } from "../../../models/report-type.enum";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { WorkflowItem } from "src/models/workflow-item";
import { WorkflowItemService } from "src/app/body/services/workflow-item.service";

import { cloneDeep } from "lodash";
import { RapidEventsService } from "src/services/rapid-events.service";
import { MessageType } from "src/app/core/models/message.model";
import { MessageService } from "src/app/core/services/message.service";
import {
  EditReportsItem,
  ReportsItem
} from "src/app/body/models/reports-item-model";

@Component({
  selector: "app-reports",
  templateUrl: "./reports.component.html",
  styleUrls: ["./reports.component.css"]
})
export class ReportsComponent implements OnInit {
  public reportType = ReportType;
  public reportTemplateType = ReportTemplateType;
  public workflowItemData: WorkflowItemCustomData;
  public workflowItem: WorkflowItem;

  public editingReportsItem: EditReportsItem = {
    id: "",
    name: "",
    reportType: null,
    reportTemplateType: null
  };
  public readOnly: boolean;
  @Output() saveReports = new EventEmitter<WorkflowItem>();
  constructor(
    private workflowItemService: WorkflowItemService,
    private rapidEventsService: RapidEventsService,
    private messageService: MessageService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.workflowItemData = this.workflowItem.customData;
    if (this.workflowItemData.jsonMetadata) {
      let itemJsonMetadata = JSON.parse(this.workflowItemData.jsonMetadata);
      this.editingReportsItem.reportType = itemJsonMetadata.reportType;
      this.editingReportsItem.reportTemplateType =
        itemJsonMetadata.reportTemplateType;
    }
    this.editingReportsItem.id = this.workflowItemData.id;
    this.readOnly = !this.workflowItemService.editable;
    this.rapidEventsService.hasUnSavedChanges = false;
  }

  ngOnInit() {}
  public onInputFieldChange(event: any) {
    this.rapidEventsService.hasUnSavedChanges = true;
  }
  public saveReportsComp() {
    let workflowItemForSave: WorkflowItem = cloneDeep(this.workflowItem);
    let workflowMetadata = {
      id: this.editingReportsItem.id,
      reportType: this.editingReportsItem.reportType,
      reportTemplateType: this.editingReportsItem.reportTemplateType
    } as ReportsItem;
    workflowItemForSave.text = "Report\n" + this.getReportType();
    workflowItemForSave.customData.jsonMetadata = JSON.stringify(
      workflowMetadata
    );
    if (this.editingReportsItem.reportType == null) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `Select Format Type!`
      });
    } else {
      this.saveReports.emit(workflowItemForSave);
    }
  }

  getReportType(): string {
    let reportType = "";
    switch (this.editingReportsItem.reportType) {
      case 0:
        reportType = "Excel";
        break;
      case 1:
        reportType = "Pdf";
        break;
      case 2:
        reportType = "Word";
        break;
      default:
        break;
    }
    return reportType;
  }
}
