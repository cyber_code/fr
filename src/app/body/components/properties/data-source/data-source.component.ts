import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { WorkflowItemService } from "./../../../services/workflow-item.service";
import {
  DataFileColumnItem,
  DataFileItem,
  KeyValueItem
} from "src/app/body/models/properties-container.model";
import {
  EditDataSourceItem,
  DataSourceType,
  DataSourceItem,
  FileItem,
  SimpleFileItem
} from "./../../../models/data-source.model";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { isNullOrUndefined } from "util";
import { WorkflowItem } from "src/models/workflow-item";
import { cloneDeep } from "lodash";
import { DataSourceService } from "src/app/body/services/data-source.service";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { ComponentSavedStatus } from "src/models/workflow-item-component-type.enum";
import KitchenSinkService from "src/services/kitchensink-service";
import { RapidEventsService } from "src/services/rapid-events.service";

@Component({
  selector: "app-data-source",
  templateUrl: "./data-source.component.html",
  styleUrls: ["./data-source.component.css"]
})
export class DataSourceComponent implements OnInit {
  @Output() saveDataSource = new EventEmitter<WorkflowItem>();
  @Input() public set compSavedStatus(compSavedStatus: any) {
    this.savedStatus = compSavedStatus.toString() as ComponentSavedStatus;
  }
  graphCellsData: any;
  @Input()
  set graphCells(graphCells: joint.dia.Cell[]) {
    this.graphCellsData = graphCells;
  }
  public rappid: KitchenSinkService;
  public dataSourceFileTypes: Array<KeyValueItem> = [
    { key: DataSourceType.Legacy.toString(), value: "Legacy" },
    { key: DataSourceType.Target.toString(), value: "Target" }
  ];
  public cmbSelectedIdentifier: DataFileColumnItem;
  public listDataSourceIdentifier: Array<DataFileColumnItem> = [];
  public editingDataSourceItem: EditDataSourceItem = {
    id: "",
    name: "",
    dataSourceColumns: [],
    dataSourceIdentifiers: [],
    dataFileSeparator: ",",
    dataFileHaveHeaders: true,
    dataSourceType: this.dataSourceFileTypes[0],
    dataSourceFiles: []
  };
  public pipelineId = "";
  public workflowItemData: WorkflowItemCustomData;
  public workflowItem: WorkflowItem;
  public readOnly: boolean;
  public savedStatus: ComponentSavedStatus;
  public componentSavedStatus = ComponentSavedStatus;
  constructor(
    private workflowItemService: WorkflowItemService,
    private messageService: MessageService,
    private dataSourceService: DataSourceService,
    private rapidEventsService: RapidEventsService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.workflowItemData = this.workflowItem.customData;
    if (this.workflowItemData.jsonMetadata) {
      let itemJsonMetadata = JSON.parse(this.workflowItemData.jsonMetadata);
      this.editingDataSourceItem = itemJsonMetadata as EditDataSourceItem;
      this.editingDataSourceItem.dataSourceType = itemJsonMetadata.isTarget
        ? this.dataSourceFileTypes.find(
            x => x.key == DataSourceType.Target.toString()
          )
        : this.dataSourceFileTypes.find(
            x => x.key == DataSourceType.Legacy.toString()
          );
      this.editingDataSourceItem.dataSourceIdentifiers = Object.assign(
        [],
        this.editingDataSourceItem.dataSourceColumns.filter(x =>
          itemJsonMetadata.identifiers.includes(x.id)
        )
      );
      this.listDataSourceIdentifier = Object.assign(
        [],
        this.editingDataSourceItem.dataSourceColumns.filter(x =>
          isNullOrUndefined(
            this.editingDataSourceItem.dataSourceIdentifiers.find(
              v => v.id === x.id
            )
          )
        )
      );
    }
    this.editingDataSourceItem.id = this.workflowItemData.id;
    this.pipelineId = this.workflowItemData.pipelineId;
    this.readOnly = !this.workflowItemService.editable;
    this.editingDataSourceItem.dataFileSeparator = ",";
    this.editingDataSourceItem.dataFileHaveHeaders = true;
    this.rapidEventsService.hasUnSavedChanges = false;
  }
  ngOnInit() {}
  public onInputFieldChange(event: any) {
    this.rapidEventsService.hasUnSavedChanges = true;
  }
  public addDataFileItem(dataFileItem: DataFileItem) {
    var isLinked = this.graphCellsData.find(
      x => x.isLink() && x.attributes.source.id == this.workflowItem.id
    );
    if (isLinked) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `Check and redo the work on the next linked components`
      });
    }
    this.editingDataSourceItem.dataSourceFiles.push({
      fileId: dataFileItem.id,
      path: dataFileItem.path,
      fileName: dataFileItem.uploadedFilesInfo[0]
        ? dataFileItem.uploadedFilesInfo[0].name
        : "",
      separator: dataFileItem.separator,
      haveHeaders: dataFileItem.haveHeaders
    } as FileItem);
    if (this.editingDataSourceItem.dataSourceFiles.length == 1) {
      this.editingDataSourceItem.dataSourceIdentifiers = [];
      this.editingDataSourceItem.dataSourceColumns =
        dataFileItem.pipelineItemColumns;
      this.listDataSourceIdentifier = Object.assign(
        [],
        this.editingDataSourceItem.dataSourceColumns
      );
    }
    this.onInputFieldChange(true);
  }
  public removeFileUploadedHandler({ dataItem }) {
    //TODO:have to add API call to delete file (when api is ready)
    dataItem as FileItem;
    let index = this.editingDataSourceItem.dataSourceFiles.findIndex(
      item => item.fileId == dataItem.fileId
    );
    if (index >= 0) {
      this.editingDataSourceItem.dataSourceFiles.splice(index, 1);
      this.onInputFieldChange(true);
    }
    if (this.editingDataSourceItem.dataSourceFiles.length == 0) {
      this.listDataSourceIdentifier = [];
      this.cmbSelectedIdentifier = null;
      this.editingDataSourceItem.dataSourceIdentifiers = [];
    }
  }
  public addDataSourceIdentifier() {
    if (this.cmbSelectedIdentifier) {
      this.editingDataSourceItem.dataSourceIdentifiers.push(
        this.cmbSelectedIdentifier
      );
      let arrayIndex = this.listDataSourceIdentifier.findIndex(
        item => item.id == this.cmbSelectedIdentifier.id
      );
      if (arrayIndex >= 0) {
        this.listDataSourceIdentifier.splice(arrayIndex, 1);
      }
      this.cmbSelectedIdentifier = null;
      this.onInputFieldChange(true);
    }
  }
  public removeDataSourceIdentifier({ dataItem }) {
    dataItem as string;
    if (this.editingDataSourceItem.dataSourceIdentifiers.length < 1) {
      return;
    }
    let index = this.editingDataSourceItem.dataSourceIdentifiers.findIndex(
      item => item.id == dataItem.id
    );
    if (index >= 0) {
      this.editingDataSourceItem.dataSourceIdentifiers.splice(index, 1);
    }
    this.listDataSourceIdentifier.push(dataItem);
    this.onInputFieldChange(true);
  }
  public saveConfiguredDataSource() {
    let actualSavedStatus = this.savedStatus;
    this.savedStatus = ComponentSavedStatus.Saving;
    let dataFiles = this.editingDataSourceItem.dataSourceFiles.map(function(
      item
    ) {
      item = Object.assign({}, item);
      delete item.fileName;
      delete item.path;
      return item;
    }) as Array<SimpleFileItem>;

    this.dataSourceService
      .doEtlOnDataListOfFiles(this.editingDataSourceItem.id, dataFiles)
      .subscribe(res => {
        if (res) {
          this.saveConfiguredDataSourceMetadata();
        } else {
          this.messageService.sendMessage({
            type: MessageType.Error,
            text: `Error in ETL on data files!`
          });
          this.savedStatus = actualSavedStatus;
        }
      });
  }

  private saveConfiguredDataSourceMetadata() {
    this.editingDataSourceItem.name = this.editingDataSourceItem.name
      ? this.editingDataSourceItem.name.replace(/\s/g, "_")
      : this.editingDataSourceItem.name;
    let workflowItemForSave: WorkflowItem = cloneDeep(this.workflowItem);
    let workflowMetadata = {
      id: this.editingDataSourceItem.id,
      name: this.editingDataSourceItem.name,
      dataSourceColumns: this.editingDataSourceItem.dataSourceColumns,
      identifiers: this.editingDataSourceItem.dataSourceIdentifiers.map(x => {
        return x.id;
      }),
      dataSourceFiles: this.editingDataSourceItem.dataSourceFiles,
      isTarget:
        this.editingDataSourceItem.dataSourceType.key ==
        DataSourceType.Target.toString()
    } as DataSourceItem;
    workflowItemForSave.text =
      "Data Source(" +
      this.editingDataSourceItem.dataSourceType.value +
      ") \n" +
      this.editingDataSourceItem.name;
    workflowItemForSave.customData.jsonMetadata = JSON.stringify(
      workflowMetadata
    );
    this.saveDataSource.emit(workflowItemForSave);
  }
}
