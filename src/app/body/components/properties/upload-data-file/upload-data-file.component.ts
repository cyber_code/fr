import { FormGroup } from "@angular/forms";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FileRestrictions,
  SuccessEvent,
  UploadEvent,
  FileInfo
} from "@progress/kendo-angular-upload";
import { Guid } from "src/app/shared/guid";
import { DataFileItem } from "src/app/body/models/properties-container.model";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";

@Component({
  selector: "app-upload-data-file",
  templateUrl: "./upload-data-file.component.html",
  styleUrls: ["./upload-data-file.component.css"]
})
export class UploadDataFileComponent implements OnInit {
  @Input() public dataFileSeparator;
  @Input() public dataFileHaveHeaders;
  @Input() public readOnly;
  @Output() dataFileItem = new EventEmitter<DataFileItem>();
  public uploadSaveUrl: string;
  public fileTypeRestriction: FileRestrictions = {
    allowedExtensions: ["csv"]
  };
  constructor(
    private configurationService: ConfigurationService,
    private messageService: MessageService
  ) {
    this.uploadSaveUrl = `${configurationService.serverSettings.apiUrl}DataFile/LoadDataFile`;
  }
  ngOnInit() {}
  public uploadUploadEventHandler(e: UploadEvent) {
    e.data = {
      id: Guid.newGuid(),
      separator: this.dataFileSeparator,
      haveHeaders: this.dataFileHaveHeaders,
      file: e.files[0]
    };
  }
  public successUploadEventHandler(e: SuccessEvent) {
    var arr = e.response.body.result.pipelineItemColumns.map(item => {
      return Object.assign({}, item);
    });
    var dataFile = {
      id: e.response.body.result.id,
      path: e.response.body.result.path,
      pipelineItemColumns: arr,
      uploadedFilesInfo: e.files,
      separator: this.dataFileSeparator,
      haveHeaders: this.dataFileHaveHeaders
    } as DataFileItem;

    this.dataFileItem.emit(dataFile);
    //TODO: this changed to DoEtlOnDataListOfFiles in save function
    /* this.dataSourceService
      .doEtlOnDataFile(dataFile.id, this.dataFileSeparator)
      .subscribe(res => {
        if (res) {
          this.dataFileItem.emit(dataFile);
        } else {
          this.messageService.sendMessage({
            type: MessageType.Error,
            text: `Error in ETL on data file!`
          });
        }
      });*/
  }
  public errorUploadEventHandler(e: ErrorEvent) {
    this.messageService.sendMessage({
      text: "Uploaded failed",
      type: MessageType.Error
    });
  }
}
