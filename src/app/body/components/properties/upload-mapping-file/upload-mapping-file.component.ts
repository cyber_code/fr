import { TransformationService } from "./../../../services/transformation.service";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FileRestrictions,
  SuccessEvent,
  UploadEvent,
  FileInfo
} from "@progress/kendo-angular-upload";
import { Guid } from "src/app/shared/guid";
import { DataFileItem } from "src/app/body/models/properties-container.model";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";

@Component({
  selector: "app-upload-mapping-file",
  templateUrl: "./upload-mapping-file.component.html",
  styleUrls: ["./upload-mapping-file.component.css"]
})
export class UploadMappingFileComponent implements OnInit {
  @Input() public initialUploadedFiles: Array<FileInfo>;
  @Input() public parentForm: FormGroup;
  @Input() public uploadFormControlName: string;
  @Input() public readOnly: boolean;
  @Input() public mappingFileSeparator: string;
  @Output() mappingFileItem = new EventEmitter<DataFileItem>();
  public uploadedFiles: Array<FileInfo>;
  public uploadSaveUrl: string;
  public fileTypeRestriction: FileRestrictions = {
    allowedExtensions: ["csv"]
  };
  constructor(
    private configurationService: ConfigurationService,
    private messageService: MessageService,
    private transformationService: TransformationService
  ) {
    this.uploadSaveUrl = `${configurationService.serverSettings.apiUrl}MappingFile/LoadMappingFile`;
  }
  ngOnInit() {
    this.uploadedFiles = this.initialUploadedFiles;
  }
  public uploadUploadEventHandler(e: UploadEvent) {
    e.data = {
      id: Guid.newGuid(),
      file: e.files[0],
      separator: this.mappingFileSeparator
    };
  }
  public successUploadEventHandler(e: SuccessEvent) {
    var arr = e.response.body.result.pipelineItemColumns.map(item => {
      return Object.assign({}, item);
    });
    var mappingFile = {
      id: e.response.body.result.id,
      path: e.response.body.result.path,
      pipelineItemColumns: arr,
      uploadedFilesInfo: e.files,
      separator: this.mappingFileSeparator
    } as DataFileItem;

    this.transformationService
      .doEtlOnMappingFile(mappingFile.id, mappingFile.separator)
      .subscribe(res => {
        if (res) {
          this.mappingFileItem.emit(mappingFile);
        } else {
          this.messageService.sendMessage({
            type: MessageType.Error,
            text: `Error in ETL on mapping file!`
          });
        }
      });
  }
  public errorUploadEventHandler(e: ErrorEvent) {
    this.messageService.sendMessage({
      text: "Uploaded failed",
      type: MessageType.Error
    });
  }
  public resetUploadField() {
    this.uploadedFiles = [];
  }
}
