import { ComponentSavedStatus } from "./../../../../../models/workflow-item-component-type.enum";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { WorkflowItemComponentType } from "src/models/workflow-item-component-type.enum";
import * as WorkFlowActType from "src/models/workflow-item-component-type.enum";
import { WorkflowItem } from "src/models/workflow-item";
@Component({
  selector: "app-propeties-container",
  templateUrl: "./propeties-container.component.html",
  styleUrls: ["./propeties-container.component.css"]
})
export class PropetiesContainerComponent implements OnInit {
  @Input() componentType: WorkflowItemComponentType;
  @Input() graphCells: joint.dia.Cell[];
  @Input() componentSavedStatus: any;
  @Output() saveWorkflow = new EventEmitter<WorkflowItem>();
  public WorkflowItemComponentType = WorkFlowActType.WorkflowItemComponentType;
  constructor() {}
  ngOnInit() {}
  saveWorkflowItem(workflowItemForSave: WorkflowItem) {
    this.saveWorkflow.emit(workflowItemForSave);
  }
}
