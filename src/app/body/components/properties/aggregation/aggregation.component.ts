import { AggregationService } from "./../../../services/aggregation.service";
import {
  EditAggregationElement,
  AggregationItem,
  AggregationElement
} from "./../../../models/aggregation.model";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { EditAggregationItem } from "src/app/body/models/aggregation.model";

import { TransformationItem } from "src/app/body/models/transformation.model";
import { WorkflowItem } from "src/models/workflow-item";
import { WorkflowItemService } from "src/app/body/services/workflow-item.service";
import { Guid } from "src/app/shared/guid";
import { cloneDeep } from "lodash";
import { DataSourceItem } from "src/app/body/models/data-source.model";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { dia } from "vendor/rappid";
import { DesignService } from "src/app/core/services/design.service";
import { DataFileColumnItem } from "src/app/body/models/properties-container.model";
import { takeWhileAlive, AutoUnsubscribe } from "take-while-alive";
import { RapidEventsService } from "src/services/rapid-events.service";

@Component({
  selector: "app-aggregation",
  templateUrl: "./aggregation.component.html",
  styleUrls: ["./aggregation.component.css"]
})
@AutoUnsubscribe()
export class AggregationComponent implements OnInit {
  public prevConnectedTransformationItem: TransformationItem = new TransformationItem();
  public workflowItem: WorkflowItem;
  public aggregationFunctionNames: string[];
  public editAggregationElement: EditAggregationElement = new EditAggregationElement();
  public listAggregation: EditAggregationElement[] = [];
  public editAggregationItem: EditAggregationItem = new EditAggregationItem();
  public listGroupByColumns: string[] = [];
  public dsColumns: string[];
  public readOnly: boolean;
  public workflowItemData: WorkflowItemCustomData;
  @Output() saveAggregation = new EventEmitter<WorkflowItem>();
  public prevConnectedDataSourceItem: DataSourceItem = new DataSourceItem();
  public prevConnectedDataSourceItems: DataSourceItem[] = [];
  public prevConnectedTransformationItems: TransformationItem[] = [];
  public openedDetailsPopup: boolean = false;
  public pipelineColumnNames: Array<DataFileColumnItem> = [];
  @Input() public isEditForm = false;
  public openedEditAggregationPopup: boolean = false;
  isFirstTime = true;
  numericField: any[];
  public openedConfirmDeleteAction: boolean = false;
  public selectedItemForDelete: any;
  @Input()
  set graphCells(graphCells: joint.dia.Cell[]) {
    if (this.isFirstTime) {
      var currentLinks = graphCells.filter(
        x => x.isLink() && x.attributes.target.id == this.workflowItem.id
      );
      var linkedElements: dia.Cell[] = [];
      currentLinks.forEach(currentLink => {
        var connectedElement = graphCells.find(
          x => currentLink && x.id == currentLink.attributes.source.id
        );
        if (connectedElement) {
          linkedElements.push(connectedElement);
        }
      });

      linkedElements.forEach(linkedComponentElement => {
        if (
          linkedComponentElement &&
          linkedComponentElement.attributes &&
          linkedComponentElement.attributes.customData &&
          linkedComponentElement.attributes.customData.jsonMetadata
        ) {
          let dataSourceItem = JSON.parse(
            linkedComponentElement.attributes.customData.jsonMetadata
          ) as DataSourceItem;

          let transformationItem = JSON.parse(
            linkedComponentElement.attributes.customData.jsonMetadata
          ) as TransformationItem;

          if (linkedComponentElement.attributes.customData.componentType == 0) {
            if (dataSourceItem.id != this.prevConnectedDataSourceItem.id) {
              this.prevConnectedDataSourceItem = dataSourceItem;
              this.prevConnectedDataSourceItems.push(dataSourceItem);
              this.editAggregationElement.listcolumnName = this.prevConnectedDataSourceItem.dataSourceColumns;
              this.designService
                .getPipelineItemColumnsById(dataSourceItem.id)
                .pipe(takeWhileAlive(this))
                .subscribe(data => {
                  this.pipelineColumnNames = data.pipelineItemColumns as Array<
                    DataFileColumnItem
                  >;
                  this.editAggregationElement.listcolumnName = this.pipelineColumnNames;
                  this.numericField = this.pipelineColumnNames.filter(
                    x => x.dataType === 0
                  );
                  this.mapColumns();
                });
            }
          } else if (
            linkedComponentElement.attributes.customData.componentType == 1
          ) {
            if (
              transformationItem.id != this.prevConnectedTransformationItem.id
            ) {
              this.prevConnectedTransformationItem = transformationItem;
              this.editAggregationElement.listcolumnName = this.pipelineColumnNames;
              this.prevConnectedTransformationItems.push(transformationItem);
              this.designService
                .getPipelineItemColumnsById(transformationItem.id)
                .pipe(takeWhileAlive(this))
                .subscribe(data => {
                  this.pipelineColumnNames = data.pipelineItemColumns as Array<
                    DataFileColumnItem
                  >;
                  this.editAggregationElement.listcolumnName = this.pipelineColumnNames;
                  this.numericField = this.pipelineColumnNames.filter(
                    x => x.dataType === 0
                  );
                  this.mapColumns();
                });
            }
          }
        }
      });
      this.isFirstTime = false;
    }
  }

  public editAggregation: EditAggregationItem = {
    id: "",
    name: "",
    // key: [],
    aggregations: [],
    groupByFields: []
  };

  constructor(
    private aggregationService: AggregationService,
    private workflowItemService: WorkflowItemService,
    private designService: DesignService,
    private rapidEventsService: RapidEventsService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.readOnly = !this.workflowItemService.editable;
    this.workflowItemData = this.workflowItem.customData;
    this.rapidEventsService.hasUnSavedChanges = false;
  }
  ngOnInit() {
    this.aggregationService
      .GetAggregationFunctions()
      .subscribe(functionItem => {
        this.aggregationFunctionNames = functionItem;
      });
    this.mapColumns();
  }

  public onInputFieldChange(event: any) {
    this.rapidEventsService.hasUnSavedChanges = true;
  }
  mapColumns() {
    if (this.workflowItemData.jsonMetadata) {
      let jsonMetadata = JSON.parse(this.workflowItemData.jsonMetadata);
      let formatedListAggregation = jsonMetadata
        ? jsonMetadata.aggregations
        : [];
      let columnNames = this.editAggregationElement.listcolumnName;
      this.listAggregation = formatedListAggregation.map(function(item) {
        item.tempUiId = Guid.newGuid();
        item.listcolumnName = columnNames;
        item.columnName = columnNames
          ? columnNames.find(x => x.name == item.columnName)
          : item.columnName;
        return item;
      });
      this.editAggregation.name = jsonMetadata.name;
      this.editAggregation.groupByFields = jsonMetadata.groupByFields;
    }
  }

  public addNewEditAggregationElement(
    editAggregationElement: EditAggregationElement
  ) {
    this.editAggregationElement.tempUiId = Guid.newGuid();
    this.listAggregation.push(editAggregationElement);
    this.onInputFieldChange(true);
  }

  public saveConfiguredAggregation() {
    this.editAggregation.name = this.editAggregation.name
      ? this.editAggregation.name.replace(/\s/g, "_")
      : this.editAggregation.name;
    let workflowItemForSave: WorkflowItem = cloneDeep(this.workflowItem);
    let workflowMetadata = {
      id: this.workflowItem.id,
      name: this.editAggregation.name,
      //  key: this.editAggregation.key,
      aggregations: this.listAggregation.map(function(item) {
        return {
          id: item.id,
          aggregationFunction: item.aggregationFunction,
          columnName: item.columnName.name,
          columnId: item.columnName.id,
          alias: item.alias
        } as AggregationElement;
      }),
      groupByFields: this.editAggregation.groupByFields
    } as AggregationItem;
    workflowItemForSave.text = "Aggregation \n" + this.editAggregation.name;
    workflowItemForSave.customData.jsonMetadata = JSON.stringify(
      workflowMetadata
    );
    this.saveAggregation.emit(workflowItemForSave);
  }

  public editAggregationElementHandler({ dataItem }) {
    this.openedEditAggregationPopup = true;
    this.editAggregationElement = Object.assign({}, dataItem);
  }

  public removeAggregationElementHandler({ dataItem }) {
    this.selectedItemForDelete = dataItem;
    this.openedConfirmDeleteAction = dataItem != null;
  }
  public confirmDeleteItem(result: EditAggregationElement) {
    let index = this.listAggregation.findIndex(
      x => x.tempUiId == result.tempUiId
    );
    this.listAggregation.splice(index, 1);
    this.onInputFieldChange(true);
    this.openedConfirmDeleteAction = false;
  }

  public cancelDeleteItem() {
    this.openedConfirmDeleteAction = false;
  }
  public saveEditAggregationElement(aggregationItem: EditAggregationElement) {
    let updatedIndex = this.listAggregation.findIndex(
      x => x.tempUiId == aggregationItem.tempUiId
    );
    this.listAggregation[updatedIndex] = aggregationItem;
    this.openedEditAggregationPopup = false;
    this.onInputFieldChange(true);
  }
  public openDetailsPopup() {
    this.openedDetailsPopup = true;
  }

  public closeDetailsPopup() {
    this.openedDetailsPopup = false;
  }

  public cancelEditConfiguredAggregation() {
    this.openedEditAggregationPopup = false;
  }

  public closeEditAggregationPopup() {
    this.openedEditAggregationPopup = false;
  }
}
