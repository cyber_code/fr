import { RapidEventsService } from "./../../../../../services/rapid-events.service";
import {
  ColumnItem,
  EditComparisonElement,
  EditComparisonItem,
  ComparisonRule,
  ComparisonItem,
  PipelineComponentItem
} from "./../../../models/comparison-rules.model";
import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { ComparisionService } from "./../../../services/comparision.service";
import { KeyValueItem } from "src/app/body/models/properties-container.model";
import { AggregationItem } from "src/app/body/models/aggregation.model";
import { WorkflowItem } from "src/models/workflow-item";
import { WorkflowItemCustomData } from "src/models/workflow-item-custom-data";
import { WorkflowItemService } from "src/app/body/services/workflow-item.service";
import { Guid } from "src/app/shared/guid";
import { cloneDeep } from "lodash";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { PipelineItemColumnDataType } from "src/app/body/models/properties-container.enum";

@Component({
  selector: "app-comparison-rules",
  templateUrl: "./comparison-rules.component.html",
  styleUrls: ["./comparison-rules.component.css"]
})
export class ComparisonRulesComponent implements OnInit {
  openedComparisonDialogBox: boolean = false;
  areConnectedElementsValid: boolean = true;
  listLegacyColumns: ColumnItem[] = [];
  listTargetColumns: ColumnItem[] = [];
  leftExpressionColumns: ColumnItem[] = [];
  leftEn: any[] = [];
  leftLn: any[] = [];
  missingInLegacy: any[] = [];
  missingInTarget: any[] = [];
  rightExpressionColumns: ColumnItem[] = [];
  columnE: boolean;
  rightEn: any[] = [];
  rightTn: any[] = [];


  public listMathFunctions: KeyValueItem[] = [
    { key: "0", value: "+" },
    { key: "1", value: "-" },
    { key: "2", value: "*" },
    { key: "3", value: "/" }
  ];
  public listCompareFunctions: KeyValueItem[] = [
    { key: "4", value: "<" },
    { key: "5", value: ">" },
    { key: "6", value: "<=" },
    { key: "7", value: ">=" },
    { key: "8", value: "=" }
  ];

  // public listMathFunctionsArray: KeyValueItem[] = [

  // ];

  listMappings: ComparisonRule[] = [];
  isFirstTime = true;
  @Input()
  set graphCells(graphCells: joint.dia.Cell[]) {
    if (this.isFirstTime) {
      this.isFirstTime = false;
      var currentLinks = graphCells.filter(
        x => x.isLink() && x.attributes.target.id == this.workflowItem.id
      );

      // var aggregationElements: dia.Cell[] = [];
      var connectedElements: string[] = [];
      currentLinks.forEach(currentLink => {
        var connectedElement = graphCells.find(
          x => currentLink && x.id == currentLink.attributes.source.id
        );
        if (connectedElement) {
          // aggregationElements.push(connectedElement);
          if (
            connectedElement &&
            connectedElement.attributes &&
            connectedElement.attributes.customData &&
            connectedElement.attributes.customData.jsonMetadata
          ) {
            connectedElements.push(connectedElement.id.toString());
          } else {
            this.areConnectedElementsValid = false;
          }
        }
      });

      this.getComparisonColumns(connectedElements);

      if (!this.areConnectedElementsValid) {
        this.messageService.sendMessage({
          type: MessageType.Error,
          text:
            "One or more connected items on comparison are not saved correctly!"
        });
      }

      // console.log(connectedElements)

      // let componentColumns = this.getColumns();
      // connectedElements.forEach(ce => {
      //   let columnsOfCe = componentColumns.find(x => x.id = ce.attributes.source.id)
      //     columnsOfCe.pipelineItemColumns.forEach(column => {
      //       let c: ColumnItem = {
      //         columnId: column.id,
      //         __columnName: column.name,
      //         sourceId: ce.attributes.source.id
      //       }

      //       columnsOfCe.isTarget ? this.listTargetColumns.push(c) : this.listLegacyColumns.push(c);

      //       // if (columnsOfCe.isTarget) {
      //       //   this.listTargetColumns.push(c);
      //       // } else {
      //       //   this.listLegacyColumns.push(c);
      //       // }
      //     });
      // })

      // TODO: Check if all connected elements are aggregations (PS: They're not)
      // aggregationElements.forEach(ae => {
      //   let dsTypes = this.findDatasourceType(ae, graphCells);

      //   // If is array and has elements (has datasources)
      //   if (dsTypes && dsTypes.length > 0) {
      //     let firstItem = dsTypes[0];
      //     // If all elements are equal (ex: All links are from legacy datasource)
      //     if (dsTypes.every(ds => ds == firstItem)) {
      //       switch (firstItem) {
      //         case "0":
      //           // Logic for legacy columns
      //           let lCols: ColumnItem[] = [];
      //           let cdl = JSON.parse(ae.attributes.customData.jsonMetadata)
      //           cdl.aggregations.forEach(c => {
      //             let col: ColumnItem = {
      //               sourceId: cdl.id,
      //               columnId: c.id,
      //               __columnName: `${cdl.name}.${c.alias}`
      //             }
      //             lCols.push(col)
      //           });
      //           this.listLegacyColumns.push(...lCols)
      //           // console.log('LEGACY:', cdl, this.listLegacyColumns)
      //           break;

      //         case "1":
      //           // Logic for target column
      //           let tCols: ColumnItem[] = [];
      //           let cdt = JSON.parse(ae.attributes.customData.jsonMetadata)
      //           cdt.aggregations.forEach(c => {
      //             let col: ColumnItem = {
      //               sourceId: cdt.id,
      //               columnId: c.id,
      //               __columnName: `${cdt.name}.${c.alias}`
      //             }
      //             tCols.push(col)
      //           });
      //           this.listTargetColumns.push(...tCols)
      //           // console.log('TARGET:', cdt, this.listTargetColumns)
      //           break
      //         default:
      //           break;
      //       }
      //     }
      //   }
      // });

      // if (this.listLegacyColumns.length > 0 && this.listTargetColumns.length > 0) {
      //   this.openedComparisonDialogBox = true;
      // }

      // aggregationElements.forEach(aggregationElement => {
      //   if (
      //     aggregationElement &&
      //     aggregationElement.attributes &&
      //     aggregationElement.attributes.customData &&
      //     aggregationElement.attributes.customData.jsonMetadata
      //   ) {
      //     let aggregationItem = JSON.parse(
      //       aggregationElement.attributes.customData.jsonMetadata
      //     ) as AggregationItem;
      //     if (aggregationItem.id != this.prevConnectedAggregationItem.id) {
      //       this.prevConnectedAggregationItems.push(aggregationItem);
      //     }
      //   }
      // })
    }
  }
  @Output() saveComparison = new EventEmitter<WorkflowItem>();
  public prevConnectedAggregationItem: AggregationItem = new AggregationItem();
  public prevConnectedAggregationItems: AggregationItem[] = [];
  public pipelineId: string;
  public editComparisonElement: EditComparisonElement = new EditComparisonElement();
  public listComparison: Array<EditComparisonElement> = [];
  public comparison: EditComparisonItem;
  public editComparisonItem: EditComparisonItem = new EditComparisonItem();
  public openedDetailsPopup: boolean = false;
  public openedEditComparisonPopup: boolean = false;
  public workflowItem: WorkflowItem;
  public workflowItemData: WorkflowItemCustomData;
  public readOnly: boolean;
  constructor(
    private workflowItemService: WorkflowItemService,
    private comparisionService: ComparisionService,
    private messageService: MessageService,
    private rapidEventsService: RapidEventsService
  ) {
    this.workflowItem = this.workflowItemService.workflowItemSelected;
    this.workflowItemData = this.workflowItem.customData;
    this.comparison = this.workflowItemData.jsonMetadata
      ? JSON.parse(this.workflowItemData.jsonMetadata)
      : [];
    // console.log(this.comparison)
    // this.listComparison.map(function (item) {
    //   item.tempUiId = Guid.newGuid();
    //   return item;
    // });
    this.pipelineId = this.workflowItemData.pipelineId;
    this.editComparisonItem.id = this.workflowItemData.id;
    this.readOnly = !this.workflowItemService.editable;
    if (this.comparison && this.comparison.comparisonRules) {
      this.listMappings = this.comparison.comparisonRules;
    }
    this.rapidEventsService.hasUnSavedChanges = false;
  }

  ngOnInit() {
    // this.comparisionService
    //   .getComparisonRuleOperators()
    //   .subscribe(comparisonRuleOperators => {
    //     // this.listMathFunctions = comparisonRuleOperators
    //     //   .splice(0, comparisonRuleOperators.length - 1)
    //     //   .map((item, index) => {
    //     //   return this.listMathFunctionsArray.find(x => x.key == index + "")
    //     // });
    //   });
    this.findExpression();
  }
  public onInputFieldChange(event: any) {
    this.rapidEventsService.hasUnSavedChanges = true;
  }
  public isEnabledShowDifferenceCheckBox(dataItem: ComparisonRule): boolean {
    return (
      dataItem.itemsDataType == PipelineItemColumnDataType.Numeric &&
      !this.readOnly
    );
  }
  getComparisonColumns(ids: string[]): PipelineComponentItem[] {
    this.comparisionService
      .getComparisonRuleLinkedItems(ids)
      .subscribe((res: PipelineComponentItem[]) => {
        let targetColumns: PipelineComponentItem[] = res.filter(
          c => c.isTarget === true
        );
        let legacyColumns: PipelineComponentItem[] = res.filter(
          c => c.isTarget === false
        );

        targetColumns.forEach(ce => {
          ce.pipelineItemColumns.forEach(c => {
            let column = new ColumnItem();
            column.sourceId = ce.id;
            column.columnId = c.id;
            column.__columnName = `${ce.name || ""}.${c.name || ""}`;
            column.columnName = c.name;
            column.dataType = c.dataType;
            this.listTargetColumns.push(column);
          });
        });

        legacyColumns.forEach(ce => {
          ce.pipelineItemColumns.forEach(c => {
            let column = new ColumnItem();
            column.sourceId = ce.id;
            column.columnId = c.id;
            column.__columnName = `${ce.name || ""}.${c.name || ""}`;
            column.columnName = c.name;
            column.dataType = c.dataType;
            this.listLegacyColumns.push(column);
          });
        });
        this.fillInLegacy();
        this.fillInTarget();
      });
    return [];
  }

  findDatasourceType(component, cells) {
    let dsTypes = [];
    if (component.attributes.customData.componentType == 0) {
      let metaData = JSON.parse(component.attributes.customData.jsonMetadata);
      dsTypes.push(metaData.dataSourceType);
      return dsTypes;
    } else {
      let links = cells.filter(
        x => x.isLink() && x.attributes.target.id == component.id
      );
      links.forEach(link => {
        let upperElementId = link.attributes.source.id;
        let upperElement = cells.find(item => item.id == upperElementId);
        let conectedDatasources = this.findDatasourceType(upperElement, cells);
        dsTypes.push(...conectedDatasources);
        return dsTypes;
      });
    }
    return dsTypes;
  }

  ruleAdded(e: ComparisonRule) {
    let rule: ComparisonRule = new ComparisonRule();
    rule.ComparisonRuleElement = JSON.parse(
      JSON.stringify(e.ComparisonRuleElement)
    );
    rule.ShowDifference = false;
    rule.__tempId = Guid.newGuid();
    rule.itemsDataType = e.itemsDataType;
    if (
      !this.listMappings.find(
        x =>
          x.ComparisonRuleElement.expression ==
          e.ComparisonRuleElement.expression
      )
    ) {
      this.listMappings.push(rule);
      this.onInputFieldChange(true);
    }
  }

  removeItem(id: string) {
    this.listMappings = this.listMappings.filter(x => x.__tempId != id);
    this.findExpression();
    this.fillInTarget();
    this.fillInLegacy();
    this.onInputFieldChange(true);
  }
  checkShowDifferenceValue(id: string, checkValue: boolean) {
    if (checkValue) {
      this.listMappings.find(
        x => x.__tempId == id
      ).toleranceValue = this.listMappings.find(x => x.__tempId == id)
        .toleranceValue
          ? this.listMappings.find(x => x.__tempId == id).toleranceValue
          : 0;
    } else {
      this.listMappings.find(x => x.__tempId == id).toleranceValue = null;
    }
  }
  openComparisonDialogBox() {
    this.openedComparisonDialogBox = true;
  }

  closeComparisonDialogBox() {
    this.openedComparisonDialogBox = false;
  }

  saveComparisonComponent() {
      let workflowItemForSave: WorkflowItem = cloneDeep(this.workflowItem);
      let workflowMetadata = {
        id: this.workflowItem.id,
        name: "Comparison",
        comparisonRules: JSON.parse(JSON.stringify(this.listMappings))
      } as ComparisonItem;
      workflowItemForSave.customData.jsonMetadata = JSON.stringify(
        workflowMetadata
      );
      this.checkInLegacy();
      this.checkInTarget();
     if(this.missingInLegacy.length<1 && this.missingInTarget.length<1){
      this.saveComparison.emit(workflowItemForSave);
    }
  }


  findExpression() {
    this.leftExpressionColumns = [];
    this.rightExpressionColumns = [];
    this.listMappings.forEach(element => {
      let leftElement = element.ComparisonRuleElement.left.value;
      let rightElement = element.ComparisonRuleElement.right.value;
      let leftParsed = JSON.parse(leftElement);
      let rightParsed = JSON.parse(rightElement);
      this.leftExpressionColumns.push(leftParsed);
      this.rightExpressionColumns.push(rightParsed);
    }
    )
  }

  fillInLegacy() {
    this.leftLn = [];
    this.listLegacyColumns.forEach(element => {
      this.leftLn.push(element.__columnName)
    })

    this.leftEn = [];
    this.leftExpressionColumns.forEach(element => {
      this.leftEn.push(element.__columnName);
    });
  }

  fillInTarget() {
    this.rightTn = [];
    this.listTargetColumns.forEach(element => {
      this.rightTn.push(element.__columnName)
    })

    this.rightEn = [];
    this.rightExpressionColumns.forEach(element => {
      this.rightEn.push(element.__columnName);
    });

  }

  checkInLegacy() {
    this.missingInLegacy = this.leftEn.filter(item => this.leftLn.indexOf(item) < 0);
    if (this.missingInLegacy.length > 0) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `The column` + ` ` + `"` + this.missingInLegacy + `"` + ` ` + `does not exist.` + `Please rewrite the rule?`
      });
    }
  }

  checkInTarget() {
    this.missingInTarget = this.rightEn.filter(item => this.rightTn.indexOf(item) < 0);
    if (this.missingInTarget.length > 0) {
      this.messageService.sendMessage({
        type: MessageType.Error,
        text: `The column` + ` ` + `"` + this.missingInTarget + `"` + ` ` + `does not exist.` + `Please rewrite the rule?`
      });
    }
  }

}
