import {
  OperatorItem,
  TreeNodeElement
} from "./../../../models/properties-container.model";
import {
  TypesOfItem,
  FilterExpressionElement
} from "./../../../models/transformation.model";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { DataFileColumnItem } from "src/app/body/models/properties-container.model";
import { TransformationService } from "src/app/body/services/transformation.service";
import { takeWhileAlive, AutoUnsubscribe } from "take-while-alive";
import { MathOperatorType } from "src/app/body/models/properties-container.enum";

@Component({
  selector: "app-transformation-filter-item",
  templateUrl: "./transformation-filter-item.component.html",
  styleUrls: ["./transformation-filter-item.component.css"]
})
@AutoUnsubscribe()
export class TransformationFilterItemComponent implements OnInit {
  @Input() dataSourceColumns: Array<DataFileColumnItem>;
  @Output() filterAdded: EventEmitter<TreeNodeElement> = new EventEmitter();
  public mathOperators: OperatorItem[];
  public typeOfItem = TypesOfItem;
  public filterSelectedItems: Array<FilterExpressionElement> = [];
  public editExpression: string = "";
  public filterFreeText: string = "";
  public arithmeticOperator: OperatorItem[];
  public comparisonOperator: OperatorItem[];
  public logicalOperator: OperatorItem[];
  public bracketOperator: OperatorItem[];
  constructor(private transformationService: TransformationService) {}
  ngOnInit() {
    this.transformationService
      .getTransformationOperators()
      .pipe(takeWhileAlive(this))
      .subscribe(operators => {
        this.mathOperators = operators;
        this.arithmeticOperator = this.mathOperators.filter(
          x => x.mathOperatorType == MathOperatorType.Arithmetic
        );
        this.comparisonOperator = this.mathOperators.filter(
          x => x.mathOperatorType == MathOperatorType.Comparison
        );
        this.logicalOperator = this.mathOperators.filter(
          x => x.mathOperatorType == MathOperatorType.Logical
        );
        this.bracketOperator = this.mathOperators.filter(
          x => x.mathOperatorType == MathOperatorType.Parenthesis
        );
      });
  }
  public insertElement(clicked: any, type: TypesOfItem) {
    let node: TreeNodeElement = new TreeNodeElement();
    let value = JSON.parse(JSON.stringify(clicked));
    node.isOperator = type == TypesOfItem.Operator;
    switch (type) {
      case TypesOfItem.Operator:
        {
          node.expression = value.value;
          node.value = JSON.stringify(+value.key);
        }
        break;
      case TypesOfItem.Column:
        {
          node.expression = value.name;
          let val = {
            isColumn: true,
            value: value.id
          };
          node.value = JSON.stringify(val);
        }
        break;
      case TypesOfItem.InputText:
        {
          node.expression = "[" + value + "]";
          let val = {
            isColumn: false,
            value: value
          };
          node.value = JSON.stringify(val);
          this.filterFreeText = "";
        }
        break;
      default: {
        break;
      }
    }
    this.filterSelectedItems.push(node);
    this.prepareExpression();
  }
  public addFilter() {
    let postfixArray = this.convertInfixArrayToPostfixArray(
      this.filterSelectedItems
    ) as Array<TreeNodeElement>;
    let expressionTree = this.constructTree(postfixArray);
    expressionTree.expression = this.editExpression;
    this.filterAdded.emit(expressionTree);
    this.clearExpression();
  }
  private prepareExpression() {
    let s: string;
    this.filterSelectedItems.forEach(i => {
      s = `${s || ""} ${i.expression || ""}`;
    });
    this.editExpression = s;
  }
  private clearExpression() {
    this.filterSelectedItems = [];
    this.editExpression = "";
    this.filterFreeText = "";
  }
  public removeElements(position: number) {
    switch (position) {
      case 0:
        this.filterSelectedItems.shift();
        break;
      case 1:
        this.filterSelectedItems.pop();
      default:
        break;
    }
    this.prepareExpression();
  }
  public get isEmptyOrValidFilterExpression(): boolean {
    if (this.filterSelectedItems.length == 0) {
      return true;
    } else {
      return this.isValidFilterExpression;
    }
  }
  public get isValidFilterExpression(): boolean {
    if (
      this.filterSelectedItems.length > 0 &&
      ((this.filterSelectedItems[0].isOperator &&
        !this.bracketOperator.find(
          x => x.key == this.filterSelectedItems[0].value
        )) ||
        (this.filterSelectedItems[this.filterSelectedItems.length - 1]
          .isOperator &&
          !this.bracketOperator.find(
            x =>
              x.key ==
              this.filterSelectedItems[this.filterSelectedItems.length - 1]
                .value
          )))
    ) {
      return false;
    }
    let postfixArray = this.convertInfixArrayToPostfixArray(
      this.filterSelectedItems
    );
    return (
      this.filterSelectedItems.length != 0 && this.isValidPostfix(postfixArray)
    );
  }
  private peek(items: Array<any>) {
    // return the top most element from the stack
    // but doesn't delete it.
    return items[items.length - 1];
  }
  private isBraces = function(key) {
    let braces = ["(", ")"];
    let char = this.mathOperators.find(x => x.key == key).value;
    if (braces.indexOf(char) != -1) {
      return true;
    }
    return false;
  };
  private convertInfixArrayToPostfixArray(
    infixItems: Array<FilterExpressionElement>
  ): Array<FilterExpressionElement> {
    var output = []; //array that holds the output in postfix notation
    var stack = []; //stack for holding the operators during conversion
    for (var i = 0; i < infixItems.length; i++) {
      if (!infixItems[i].isOperator) {
        output.push(infixItems[i]);
      } else if (
        infixItems[i].isOperator &&
        !this.isBraces(infixItems[i].value)
      ) {
        if (stack.length === 0) {
          stack.push(infixItems[i]);
        } else {
          while (
            this.mathOperators.find(x => x.key == this.peek(stack).value)
              .precedence >=
            this.mathOperators.find(x => x.key == infixItems[i].value)
              .precedence
          ) {
            output.push(stack.pop());
            if (stack.length === 0) {
              break;
            }
          }
          stack.push(infixItems[i]);
        }
      } else if (this.isBraces(infixItems[i].value)) {
        if (
          infixItems[i].value ==
          this.mathOperators.find(x => x.value == "(").key
        ) {
          stack.push(infixItems[i]);
        } else {
          while (
            this.peek(stack) &&
            this.peek(stack).value !=
              this.mathOperators.find(x => x.value == "(").key
          ) {
            output.push(stack.pop());
          }
          stack.pop();
        }
      }
    }
    if (stack.length !== 0) {
      while (stack.length !== 0) {
        output.push(stack.pop());
      }
    }
    return output;
  }
  private constructTree(postfix: Array<TreeNodeElement>): TreeNodeElement {
    let st: Array<TreeNodeElement> = [];
    let t: TreeNodeElement;
    let t1: TreeNodeElement;
    let t2: TreeNodeElement;
    for (var i = 0; i < postfix.length; i++) {
      if (!postfix[i].isOperator) {
        t = postfix[i];
        st.push(t);
      } else {
        t = postfix[i];
        t1 = st.pop();
        t2 = st.pop();
        t.right = t1;
        t.left = t2;
        st.push(t);
      }
    }
    t = this.peek(st);
    st.pop();
    return t;
  }
  public isValidPostfix(expressionPostfixArray: Array<any>) {
    let stack = [];
    for (var i = 0; i < expressionPostfixArray.length; i++) {
      if (!expressionPostfixArray[i].isOperator) {
        expressionPostfixArray[i].valueType = "value";
        stack.push(expressionPostfixArray[i]);
      } else {
        let currentOperator = this.mathOperators.find(
          x => x.key == expressionPostfixArray[i].value
        );
        let el1 = stack.pop();
        let el2 = stack.pop();
        if (el1 && el2) {
          if (
            el1.valueType == "value" &&
            el2.valueType == "value" &&
            currentOperator.mathOperatorType == MathOperatorType.Logical
          ) {
            return false;
          } else if (
            el1.valueType == "boolean" &&
            el2.valueType == "boolean" &&
            (currentOperator.mathOperatorType == MathOperatorType.Arithmetic ||
              currentOperator.mathOperatorType == MathOperatorType.Comparison)
          ) {
            return false;
          } else if (el1.valueType != el2.valueType) {
            return false;
          }
          stack.push({
            isOperator: false,
            valueType:
              currentOperator.mathOperatorType == MathOperatorType.Arithmetic
                ? "value"
                : "boolean"
          });
        }
      }
    }
    let lastElement = stack.pop();
    if (
      stack.length == 0 &&
      lastElement &&
      lastElement.valueType == "boolean"
    ) {
      return true;
    } else {
      return false;
    }
  }
}
