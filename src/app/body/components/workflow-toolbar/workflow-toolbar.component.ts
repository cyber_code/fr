import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { MenuItem } from "../../models/menu-item";
import { NodeType } from "../../models/node-type.enum";
import { MenuAction } from "../../models/menu-action.enum";
import { itemAt } from "@progress/kendo-angular-grid/dist/es2015/data/data.iterators";
import { TreeViewItem } from "src/app/shared/tree-view-item";
import { ToolbarActionModel } from "../../models/workflow.model";
import { DesignService } from "src/app/core/services/design.service";

@Component({
  selector: "app-workflow-toolbar",
  templateUrl: "./workflow-toolbar.component.html",
  styleUrls: ["./workflow-toolbar.component.css"]
})
export class WorkflowToolbarComponent implements OnInit {
  private allMenuItems: MenuItem[];
  public menuItems: MenuItem[];
  private selectedTreeViewItem: TreeViewItem;
  public inProcessModel: ToolbarActionModel;
  public isPopupOpen: Boolean;
  public isCriticalDialogOpen: Boolean;
  @Output() public disableTreeView = new EventEmitter<boolean>();
  @Output() public treeViewItemCreated = new EventEmitter<TreeViewItem>();
  @Output() public treeViewItemRenamed = new EventEmitter<TreeViewItem>();
  @Output() public treeViewItemDeleted = new EventEmitter<string>();
  @Output() public startEditClicked = new EventEmitter();
  @Output() public stopEditClicked = new EventEmitter();

  constructor(private designService: DesignService) {
    this.allMenuItems = [
      new MenuItem(NodeType.Default, MenuAction.NewFolder, true),
      new MenuItem(NodeType.Folder, MenuAction.NewFolder, true),
      new MenuItem(NodeType.Folder, MenuAction.NewPipeline, true),
      new MenuItem(NodeType.Folder, MenuAction.Rename, true),
      new MenuItem(NodeType.Folder, MenuAction.Delete, true),
      new MenuItem(NodeType.Workflow, MenuAction.StartEdit, true),
      new MenuItem(NodeType.Workflow, MenuAction.StopEdit, false),
      new MenuItem(NodeType.Workflow, MenuAction.Rename, true),
      new MenuItem(NodeType.Workflow, MenuAction.Delete, true)
    ];
    this.inProcessModel = new ToolbarActionModel();
    this.isPopupOpen = false;
  }
  ngOnInit() {
    this.setInitToolbar();
  }
  updateToolbar(treeViewItem: TreeViewItem) {
    this.selectedTreeViewItem = treeViewItem;
    this.menuItems = [
      ...this.allMenuItems.filter(m => m.type === treeViewItem.type)
    ];
  }
  resetMenuForEdit(menuAction: MenuAction) {
    if (menuAction == MenuAction.StartEdit) {
      this.menuItems.forEach(item => {
        if (item.menuAction === MenuAction.StopEdit) {
          item.isActive = true;
        } else {
          item.isActive = false;
        }
      });
    } else if (menuAction === MenuAction.StopEdit) {
      this.menuItems.forEach(item => {
        if (item.menuAction === MenuAction.StopEdit) {
          item.isActive = false;
        } else {
          item.isActive = true;
        }
      });
      this.disableTreeView.emit(false);
    }
  }
  setInitToolbar() {
    this.menuItems = [
      ...this.allMenuItems.filter(item => item.type === NodeType.Default)
    ];
  }
  menuItemClick(itemClicked) {
    const menuitem = itemClicked.data as MenuItem;
    this.inProcessModel.action = menuitem.menuAction;
    if (menuitem.type === NodeType.Default) {
      this.menuClickOnDefault(menuitem.menuAction);
    } else if (menuitem.type === NodeType.Folder) {
      this.menuClickOnFolder(menuitem.menuAction);
    } else if (menuitem.type === NodeType.Workflow) {
      this.menuClickOnWorkflow(menuitem.menuAction);
    }
  }
  public menuClickOnDefault(menuAction: MenuAction) {
    if (menuAction === MenuAction.NewFolder) {
      this.inProcessModel.popupTitle = "Create a new folder";
      this.isPopupOpen = true;
      this.disableTreeView.emit(true);
    }
  }
  public menuClickOnFolder(menuAction: MenuAction) {
    switch (menuAction) {
      case MenuAction.NewFolder: {
        this.menuClickOnDefault(menuAction);
        break;
      }
      case MenuAction.NewPipeline: {
        this.inProcessModel.popupTitle = "Create a new pipeline";
        this.isPopupOpen = true;
        this.disableTreeView.emit(true);
        break;
      }
      case MenuAction.Rename: {
        this.inProcessModel.popupTitle = "Rename folder";
        this.inProcessModel.title = this.selectedTreeViewItem.text;
        this.inProcessModel.description = this.selectedTreeViewItem.description;
        this.isPopupOpen = true;
        this.disableTreeView.emit(true);
        break;
      }
      case MenuAction.Delete: {
        this.inProcessModel.popupTitle =
          "Are you sure you want to delete : '" +
          this.selectedTreeViewItem.text +
          "' folder ?";
        this.inProcessModel.description = this.selectedTreeViewItem.description;
        this.isCriticalDialogOpen = true;
        this.disableTreeView.emit(true);
        break;
      }
      default: {
        break;
      }
    }
  }
  public menuClickOnWorkflow(menuAction: MenuAction) {
    switch (menuAction) {
      case MenuAction.StartEdit: {
        this.startEditClicked.emit();
        break;
      }
      case MenuAction.StopEdit: {
        this.stopEditClicked.emit();
        break;
      }

      case MenuAction.Rename: {
        this.inProcessModel.popupTitle = "Rename pipeline";
        this.inProcessModel.title = this.selectedTreeViewItem.text;
        this.inProcessModel.description = this.selectedTreeViewItem.description;
        this.isPopupOpen = true;
        this.disableTreeView.emit(true);
        break;
      }
      case MenuAction.Delete: {
        this.inProcessModel.popupTitle =
          "Are you sure you want to delete : '" +
          this.selectedTreeViewItem.text +
          "' pipeline ?";
        this.inProcessModel.description = this.selectedTreeViewItem.description;
        this.isCriticalDialogOpen = true;
        this.disableTreeView.emit(true);
        break;
      }
      default: {
        break;
      }
    }
  }

  submit() {
    if (
      !this.selectedTreeViewItem ||
      this.selectedTreeViewItem.type === NodeType.Default
    ) {
      this.processActionOnDefault();
    } else if (this.selectedTreeViewItem.type === NodeType.Folder) {
      this.processActionOnFolder();
    } else if (this.selectedTreeViewItem.type === NodeType.Workflow) {
      this.processActionOnWorkflow();
    }
  }
  private processActionOnDefault() {
    if (this.inProcessModel.action === MenuAction.NewFolder) {
      this.createNewFolder();
    }
  }
  private processActionOnFolder() {
    switch (this.inProcessModel.action) {
      case MenuAction.NewFolder: {
        this.createNewFolder();
        break;
      }
      case MenuAction.NewPipeline: {
        this.designService
          .addPipeline(
            this.inProcessModel.title,
            this.inProcessModel.description,
            "",
            this.selectedTreeViewItem.id
          )
          .subscribe(res => {
            this.treeViewItemCreated.emit(
              new TreeViewItem(
                res.id,
                this.inProcessModel.title,
                this.inProcessModel.description,
                NodeType.Workflow,
                []
              )
            );
            this.close();
          });
        break;
      }
      case MenuAction.Rename: {
        this.designService
          .updateFolder(
            this.selectedTreeViewItem.id,
            this.inProcessModel.title,
            this.inProcessModel.description
          )
          .subscribe(res => {
            this.treeViewItemRenamed.emit(
              new TreeViewItem(
                this.selectedTreeViewItem.id,
                this.inProcessModel.title,
                this.inProcessModel.description,
                NodeType.Folder,
                this.selectedTreeViewItem.children
              )
            );
            this.selectedTreeViewItem.text = this.inProcessModel.title;
            this.selectedTreeViewItem.description = this.inProcessModel.description;
            this.close();
          });
        break;
      }
      case MenuAction.Delete: {
        this.designService
          .deleteFolder(this.selectedTreeViewItem.id)
          .subscribe(res => {
            this.treeViewItemDeleted.emit(this.selectedTreeViewItem.id);
            this.setInitToolbar();
            this.close();
          });

        break;
      }
      default: {
        break;
      }
    }
  }
  private processActionOnWorkflow() {
    switch (this.inProcessModel.action) {
      case MenuAction.Rename: {
        this.designService
          .renamePipeline(
            this.selectedTreeViewItem.id,
            this.inProcessModel.title,
            this.inProcessModel.description
          )
          .subscribe(res => {
            this.treeViewItemRenamed.emit(
              new TreeViewItem(
                this.selectedTreeViewItem.id,
                this.inProcessModel.title,
                this.inProcessModel.description,
                NodeType.Workflow,
                this.selectedTreeViewItem.children
              )
            );
            this.selectedTreeViewItem.text = this.inProcessModel.title;
            this.selectedTreeViewItem.description = this.inProcessModel.description;
            this.close();
          });
        break;
      }
      case MenuAction.Delete: {
        this.designService
          .deletePipeline(this.selectedTreeViewItem.id)
          .subscribe(res => {
            this.treeViewItemDeleted.emit(this.selectedTreeViewItem.id);
            this.setInitToolbar();
            this.close();
          });
        break;
      }
      default: {
        break;
      }
    }
  }
  private createNewFolder() {
    this.designService
      .addFolder(this.inProcessModel.title, this.inProcessModel.description)
      .subscribe(res => {
        this.treeViewItemCreated.emit(
          new TreeViewItem(
            res.id,
            this.inProcessModel.title,
            this.inProcessModel.description,
            NodeType.Folder,
            []
          )
        );
        this.close();
      });
  }
  close() {
    this.disableTreeView.emit(false);
    this.isPopupOpen = false;
    this.isCriticalDialogOpen = false;
    this.inProcessModel = new ToolbarActionModel();
  }
}
