import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-pipelines",
  templateUrl: "./pipelines.component.html",
  styleUrls: ["./pipelines.component.css"]
})
export class PipelinesComponent implements OnInit {
  constructor() {}
  public data: any[] = [
    {
      text: "Furniture",
      items: [
        { text: "Tables & Chairs" },
        { text: "Sofas" },
        { text: "Occasional Furniture" }
      ]
    },
    {
      text: "Decor",
      items: [
        { text: "Bed Linen" },
        { text: "Curtains & Blinds" },
        { text: "Carpets" }
      ]
    }
  ];
  ngOnInit() {}
}
