import { ApiAction } from "src/app/core/services/api-action";
import { SimpleFileItem } from "../models/data-source.model";

export class savePipelineItem extends ApiAction {
  constructor(id: string, pipelineId: string, designerJson: string) {
    super("SavePipelineItem");
    this.id = id;
    this.pipelineId = pipelineId;
    this.designerJson = designerJson;
  }
  id: string;
  pipelineId: string;
  designerJson: string;
}
export class deletePipelineItem extends ApiAction {
  constructor(id: string, pipelineId: string, designerJson: string) {
    super("DeletePipelineItem");
    this.id = id;
    this.pipelineId = pipelineId;
    this.designerJson = designerJson;
  }
  id: string;
  pipelineId: string;
  designerJson: string;
}
