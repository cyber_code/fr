import { ApiAction } from "src/app/core/services/api-action";
import { SimpleFileItem } from "../models/data-source.model";

//TODO: this is not used
export class DoEtlOnDataFile extends ApiAction {
  constructor(dataFileId: string, separator: string) {
    super("DoEtlOnDataFile");
    this.dataFileId = dataFileId;
    this.separator = separator;
  }
  dataFileId: string;
  separator: string;
}

export class DoEtlOnDataListOfFiles extends ApiAction {
  constructor(dataFileId: string, dataFiles: Array<SimpleFileItem>) {
    super("DoEtlOnDataListOfFiles");
    this.dataFileId = dataFileId;
    this.dataFiles = dataFiles;
  }
  dataFileId: string;
  dataFiles: Array<SimpleFileItem>;
}
