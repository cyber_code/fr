import { ApiAction } from "src/app/core/services/api-action";

export class GetTreeView extends ApiAction {
    constructor() {
        super('GetTreeView');
    }
}

export class GetPipelineById extends ApiAction {
    id: string;
    constructor(id: string) {
        super('GetPipelineById');
        this.id = id;
    }
}