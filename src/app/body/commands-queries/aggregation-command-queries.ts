import { ApiAction } from "src/app/core/services/api-action";

export class GetAggregationFunctions extends ApiAction {
  constructor() {
    super("GetAggregationFunctions");
  }
}
