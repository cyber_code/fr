import { ApiAction } from "src/app/core/services/api-action";

export class SaveReportPipelineItem extends ApiAction {
  constructor(id: string, pipelineId: string, designerJson: string) {
    super("SaveReportPipelineItem");
    this.id = id;
    this.pipelineId = pipelineId;
    this.designerJson = designerJson;
  }
  id: string;
  pipelineId: string;
  designerJson: string;
}
