import { ApiAction } from "src/app/core/services/api-action";

export class GetComparisonRuleOperators extends ApiAction {
  constructor() {
    super("GetComparisonRuleOperators");
  }
}
export class GetComparisonRuleLinkedItems extends ApiAction {
  constructor(ids: string[]) {
    super("GetComparisonRuleLinkedItems");
    this.ids = ids;
  }
  ids: string[];
}
