import { ApiAction } from "src/app/core/services/api-action";

export class ExecutePipeline extends ApiAction {
  constructor(id: string) {
    super("ExecutePipeline");
    this.pipelineId = id;
  }
  pipelineId: string;
}

export class GetPipelineItemByPipelineId extends ApiAction {
  constructor(id: string) {
    super("GetPipelineItemByPipelineId");
    this.PipelineId = id;
  }
  PipelineId: string;
}

export class CreateReportById extends ApiAction {
  constructor(id: string) {
    super("CreateReportById");
    this.Id = id;
  }
  Id: string;
}