import { ApiAction } from "src/app/core/services/api-action";

export class GetLatestPipelineExecutions extends ApiAction {
  constructor(
    skip: number,
    take: number,
    pipelineName: string,
    startTime: string,
    endTime: string
  ) {
    super("GetLatestPipelineExecutions");
    this.skip = skip;
    this.take = take;
    this.pipelineName = pipelineName;
    this.startTime = startTime;
    this.endTime = endTime;
  }
  skip: number;
  take: number;
  pipelineName: string;
  startTime: string;
  endTime: string;
}
