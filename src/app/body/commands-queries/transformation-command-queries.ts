import { ApiAction } from "src/app/core/services/api-action";

export class GetTransformationFunctions extends ApiAction {
  constructor() {
    super("GetTransformationFunctions");
  }
}
export class DoEtlOnMappingFile extends ApiAction {
  constructor(mappingFileId: string, separator: string) {
    super("DoEtlOnMappingFile");
    this.mappingFileId = mappingFileId;
    this.separator = separator;
  }
  mappingFileId: string;
  separator: string;
}
export class GetTransformationOperators extends ApiAction {
  constructor() {
    super("GetTransformationOperators");
  }
}