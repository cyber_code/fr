import { NotificationService } from "@progress/kendo-angular-notification";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { NotificationModule } from "@progress/kendo-angular-notification";
import {
  LayoutModule,
  SplitterModule,
  PanelBarModule
} from "@progress/kendo-angular-layout";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { AppComponent } from "./app.component";
import { MainScreenComponent } from "./body/components/main-screen/main-screen.component";
import { AppRoutingModule } from "./app-routing.module";
import { CoreModule } from "./core/core.module";
import { BodyModule } from "./body/body.module";
import { PipelinesComponent } from "./body/components/pipelines/pipelines.component";
import { TreeViewModule } from "@progress/kendo-angular-treeview";
import { ConfigurationService } from "./core/services/configuration.service";
import { StorageServiceModule } from "angular-webstorage-service";
import { TreeViewComponent } from "./body/components/tree-view/tree-view.component";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReportsModule } from "./reports/reports.module";

const appInitializerFn = (appConfig: ConfigurationService) => {
  return () => {
    return appConfig.load();
  };
};

@NgModule({
  declarations: [
    AppComponent,
    MainScreenComponent,
    PipelinesComponent,
    TreeViewComponent
  ],
  imports: [
    BrowserModule,
    StorageServiceModule,
    RouterModule,
    CoreModule,
    BodyModule,
    ReportsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    SplitterModule,
    PanelBarModule,
    TreeViewModule,
    NotificationModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [ConfigurationService]
    },
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
