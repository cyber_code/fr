import {RouterModule, Router, ActivatedRoute} from '@angular/router';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../../services/authentication.service";
import {first} from "rxjs/operators";
import {BuilderService} from "../../services/builder.service";
import {MessageComponent} from "../message/message.component";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    @ViewChild(MessageComponent) message: MessageComponent;

    constructor(public router: Router,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private builderService: BuilderService,
                private authenticationService: AuthenticationService,
                private http: HttpClient) {
    }

    ngOnInit() {
        if (this.authenticationService.isAuthenticated()) {
            this.router.navigateByUrl('/main-screen');
        }
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get f() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            if (this.f.username.value.trim('') === '') {
                this.message.displayMessage(
                    'Username must not be empty!',
                    'Error',
                    'danger'
                );
            } else if (this.f.password.value.trim('') === '') {
                this.message.displayMessage(
                    'Password must not be empty!',
                    'Error',
                    'danger'
                );
            }
            return;
        }
        this.loading = true;
        this.authenticationService
            .login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.builderService.removeBotSession();
                    this.router.navigateByUrl('/main-screen');
                },
                error => {
                    this.message.displayMessage(
                        'Error username or password!',
                        'Authentication Error',
                        'danger'
                    );
                    this.loading = false;
                }
            );
    }

// public login() {
//     this.router.navigate(['/main-screen']);
// }

}
