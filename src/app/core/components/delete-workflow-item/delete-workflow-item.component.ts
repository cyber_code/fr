import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-delete-workflow-item",
  templateUrl: "./delete-workflow-item.component.html",
  styleUrls: ["./delete-workflow-item.component.css"]
})
export class DeleteWorkflowItemComponent implements OnInit {
  @Input() public selectedItemsForDelete: Array<joint.dia.Cell>;
  @Output() public closeDeleteItemWindow = new EventEmitter<any>();
  @Output() public deleteWorkFlowItems = new EventEmitter<
    Array<joint.dia.Cell>
  >();
  constructor() {}

  ngOnInit() {}
  deleteItem() {
    this.deleteWorkFlowItems.emit(this.selectedItemsForDelete);
  }
  close() {
    this.closeDeleteItemWindow.emit();
  }
}
