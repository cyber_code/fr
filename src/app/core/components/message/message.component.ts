import {Component, OnInit, ViewChild, Input} from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  message: string;
  messageType: string;
  messageTitle: string;
  showMessage: boolean;
  icon: string;

  constructor() {
  }

  ngOnInit() {
  }

  displayMessage(message: string, messageTitle: string, messageType: string): void {
    this.message = message;
    this.messageTitle = messageTitle;
    this.messageType = messageType;
    this.showMessage = true;
    this.icon = this.returnIcon(messageType);
    setTimeout(() => {
      this.showMessage = false;
    }, 2000);
  }

  returnIcon(messageType: string): string {
    switch (messageType) {
      case 'danger':
        return 'ban';
      case 'success':
        return 'check';
      case 'warning':
        return 'warning';
    }
  }
}
