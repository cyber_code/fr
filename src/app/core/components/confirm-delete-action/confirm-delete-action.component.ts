import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-confirm-delete-action",
  templateUrl: "./confirm-delete-action.component.html",
  styleUrls: ["./confirm-delete-action.component.css"]
})
export class ConfirmDeleteActionComponent implements OnInit {
  @Input() public selectedItemForDelete: any;
  @Output() public cancelDeleteItem = new EventEmitter();
  @Output() public confirmDeleteItem = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {}
  deleteItem() {
    this.confirmDeleteItem.emit(this.selectedItemForDelete);
  }
  close() {
    this.cancelDeleteItem.emit();
  }
}
