import { Component, OnInit } from "@angular/core";
import {BuilderService} from "../../services/builder.service";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  constructor(private builderService: BuilderService,
              private authenticationService: AuthenticationService,
              private router: Router) {}

  ngOnInit() {}
  public logout() {
      this.builderService.removeBotSession();
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }
}
