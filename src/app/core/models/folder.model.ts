export class Folder {
  id: string;
  title: string;
  description: string;
}
