export class Configuration {
    apiUrl: string;
    identityUrl: string;
    signalRUrl: string;
}
