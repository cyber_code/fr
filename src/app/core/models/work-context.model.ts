import { User } from './user.model';
import { DefaultSettings } from "./default-settings.model";

export class WorkContext {
   user: User;
   identityUrl: string;
   defaultSettings: DefaultSettings;
}

