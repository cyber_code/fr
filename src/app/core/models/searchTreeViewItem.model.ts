export class SearchTreeViewItem {
    folderId: string;
    pipelineId: string;
    title: string;
    description: string;
}