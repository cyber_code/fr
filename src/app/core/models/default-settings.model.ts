import { System } from './system.model';
import { Catalog } from './catalog.model';

export class DefaultSettings {
  catalog: Catalog;
  system: System;
  constructor(catalog: Catalog, system: System) {
    this.catalog = catalog;
    this.system = system;
  }
}
export class Settings {
  id: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
}
