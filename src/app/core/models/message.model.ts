export class FRMessage {
  text: string;
  type: MessageType;
  timeout?: Number;
  appendTo?: any;
}
export enum MessageType {
  Error = 0,
  Success = 1,
  Warning = 2,
  Info = 3,
  Default = 4
}
