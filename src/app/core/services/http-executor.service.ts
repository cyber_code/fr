import { HaloService } from "src/services/halo-service";
import { MessageService } from "./message.service";
import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Router } from "@angular/router";
import { SessionService } from "./session.service";
import { Observable, throwError } from "rxjs";
import { CommandMethod } from "src/app/shared/command-method";
import { map, catchError } from "rxjs/operators";
import { MessageType } from "../models/message.model";
import { ApiAction } from "./api-action";
import {
  UNAUTHORIZED,
  FORBIDDEN,
  BAD_REQUEST,
  NOT_FOUND
} from "http-status-codes";
import { AuthenticationService } from "./authentication.service";
@Injectable({
  providedIn: "root"
})
export class HttpExecutorService {
  static readonly REFRESH_PAGE_MESSAGE: string =
    "An error occurred: Please refresh this page!";
  static readonly DEFAULT_ERROR_TITLE: string = "Something went wrong";
  constructor(
    private http: HttpClient,
    private router: Router,
    private sessionService: SessionService,
    private messageService: MessageService,
    private authenticationService: AuthenticationService
  ) {}

  executeQuery<T>(url: string, queryObject?: any): Observable<T> {
    return this.get<T>(this.getQueryUrl(url, queryObject), queryObject);
  }

  executeCommand<T>(
    url: string,
    command: any,
    httpMethod: CommandMethod
  ): Observable<T> {
    const commandUrl = this.getQueryUrl(url, command);
    switch (httpMethod) {
      case CommandMethod.GET:
        return this.get<T>(commandUrl, command);
      case CommandMethod.PUT:
        return this.put<T>(commandUrl, command);
      case CommandMethod.POST:
        return this.post<T>(commandUrl, command);
      case CommandMethod.DELETE:
        return this.delete<T>(commandUrl, command);
    }
  }

  getQueryUrl(url: string, commandOrQuery: ApiAction) {
    url = url + commandOrQuery.apiActionName;
    // We don't 'apiActionName' need any more
    delete commandOrQuery["apiActionName"];
    return url;
  }

  private get<T>(url: string, queryObject: any): Observable<T> {
    const queryUrl = url + "?" + this.toQueryString(queryObject);
    const options = {};
    this.setHeaders(options);
    return this.http.get<T>(queryUrl, options).pipe(
      // retry(3), // retry a failed request up to 3 times
      map((res: any) => {
        return this.handleResponse(res);
      }),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  private delete<T>(url, deleteCommand: object): Observable<T> {
    const options = {};
    this.setHeaders(options);

    const deleteUrl = url + "?" + this.toQueryString(deleteCommand);

    return this.http.delete(deleteUrl, options).pipe(
      map((res: any) => {
        return this.handleResponse(res);
      }),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  private put<T>(url: string, updateCommand: any): Observable<T> {
    const options = {};
    this.setHeaders(options);

    return this.http.put<T>(url, updateCommand, options).pipe(
      map((res: any) => {
        return this.handleResponse(res);
      }),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  private post<T>(url: string, createCommand: any): Observable<T> {
    const options = {};
    this.setHeaders(options);

    return this.http.post<T>(url, createCommand, options).pipe(
      map((res: any) => {
        return this.handleResponse(res);
      }),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  private toQueryString(queryObject: any) {
    const str = [];
    for (const p in queryObject) {
      if (queryObject.hasOwnProperty(p)) {
        str.push(
          encodeURIComponent(p) + "=" + encodeURIComponent(queryObject[p])
        );
      }
    }
    return str.join("&");
  }

  public handleError(error: HttpErrorResponse) {
    const httpErrorCode = error.status;
    switch (httpErrorCode) {
      case UNAUTHORIZED:
        this.authenticationService.logout();
        this.router.navigateByUrl("/login");
        break;
      case FORBIDDEN:
        this.router.navigateByUrl("/un-authorized");
        break;
      case BAD_REQUEST:
        this.messageService.sendMessage({
          type: MessageType.Error,
          text: error.error
        });
        break;
      case NOT_FOUND:
        this.router.navigateByUrl("/not-found");
        break;
      default:
        this.messageService.sendMessage({
          type: MessageType.Error,
          text: HttpExecutorService.REFRESH_PAGE_MESSAGE
        });
    }

    return throwError(error || "server error");
  }

  private setHeaders(options: any) {
    if (this.sessionService) {
      options["headers"] = new HttpHeaders().append(
        "Authorization",
        "Bearer " + this.sessionService.getToken()
      );
    }
  }

  handleResponse(res) {
    if (res.successful && res.hasResult && res.result) {
      return res.result;
    } else if (res.successful && !res.hasResult) {
      return;
    } else {
      this.messageService.sendMessage({
        type: res.response.messageType,
        text: res.response.message
      });
      return;
    }
  }
}
