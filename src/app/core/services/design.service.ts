import { PipelineItem } from "./../../body/models/aggregation.model";
import {
  AddPipeline,
  GetPipelinesByFolderId,
  GetFolders,
  UpdateFolder,
  DeleteFolder,
  DeletePipeline,
  RenamePipeline,
  GetPipelineItemById,
  GetPipelineItemColumnsById,
  SearchPipeline
} from "./../commands-queries/design-command-queries";
import { Injectable } from "@angular/core";
import { ConfigurationService } from "./configuration.service";
import { HttpExecutorService } from "./http-executor.service";
import { Folder } from "../models/folder.model";
import { AddFolder } from "../commands-queries/design-command-queries";
import { CommandMethod } from "src/app/shared/command-method";
import { GetPipelineById } from "src/app/body/commands-queries/tree-view-command-queries";
import { Observable } from "rxjs";
import { Workflow } from "src/app/body/models/workflow.model";
import { TreeViewItem } from "src/app/shared/tree-view-item";
import { map } from "rxjs/operators";
import { NodeType } from "src/app/body/models/node-type.enum";
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/switchMap";
import { SearchTreeViewItem } from "../models/searchTreeViewItem.model";

@Injectable()
export class DesignService {
  private designUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.designUrl = `${configurationService.serverSettings.apiUrl}Design/`;
  }
  getPipelineById(id: string): Observable<Workflow> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetPipelineById(id)
    );
  }
  getPipelinesByFolderId(folderId: string): Observable<Workflow[]> {
    return this.httpExecutor.executeQuery<any>(
      this.designUrl,
      new GetPipelinesByFolderId(folderId)
    );
  }
  fetchPipelines(
    folderId: string,
    keyword: string,
    isTreeMatch
  ): Observable<TreeViewItem[]> {
    return this.getPipelinesByFolderId(folderId).pipe(
      map(workflows => {
        return workflows
          .filter(
            x =>
              isTreeMatch ||
              x.title.toLowerCase().includes(keyword.toLowerCase())
          )
          .map(
            item =>
              new TreeViewItem(
                item.id,
                item.title,
                item.description,
                NodeType.Workflow,
                []
              )
          );
      })
    );
  }
  addPipeline(
    title: string,
    description: string,
    designerJson: string,
    folderId: string
  ) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new AddPipeline(title, description, designerJson, folderId),
      CommandMethod.POST
    );
  }
  renamePipeline(pipelineId: string, title: string, description: string) {
    return this.httpExecutor.executeCommand<Folder>(
      this.designUrl,
      new RenamePipeline(pipelineId, title, description),
      CommandMethod.PUT
    );
  }

  deletePipeline(id: string) {
    return this.httpExecutor.executeCommand<Folder>(
      this.designUrl,
      new DeletePipeline(id),
      CommandMethod.DELETE
    );
  }
  addFolder(title: string, description: string) {
    return this.httpExecutor.executeCommand<Folder>(
      this.designUrl,
      new AddFolder(title, description),
      CommandMethod.POST
    );
  }
  updateFolder(id: string, title: string, description: string) {
    return this.httpExecutor.executeCommand<Folder>(
      this.designUrl,
      new UpdateFolder(id, title, description),
      CommandMethod.PUT
    );
  }
  deleteFolder(id: string) {
    return this.httpExecutor.executeCommand<Folder>(
      this.designUrl,
      new DeleteFolder(id),
      CommandMethod.DELETE
    );
  }
  getFolders() {
    return this.httpExecutor.executeQuery<any>(
      this.designUrl,
      new GetFolders()
    );
  }

  getPipelineItemById(id: string) {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetPipelineItemById(id)
    );
  }

  getPipelineItemColumnsById(pipelineItemId: string): Observable<PipelineItem> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetPipelineItemColumnsById(pipelineItemId)
    );
  }

  searchTreeViewItem(keyWord: string): Observable<SearchTreeViewItem[]> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new SearchPipeline(keyWord)
    );
  }
}
