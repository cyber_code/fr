import {Injectable, Inject} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import {User} from "../models/user.model";
import {WorkContext} from "../models/work-context.model";
import {Catalog} from "../models/catalog.model";
import {System} from "../models/system.model";
import {DefaultSettings} from "../models/default-settings.model";


const STORAGE_KEY = 'RAP_SESSION_KEY';

@Injectable({providedIn: 'root'})
export class SessionService {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    }

    setUser(user: User) {
        const context = this.getWorkContext();
        context.user = user;
        this.storage.set(STORAGE_KEY, context);
    }

    getUser(): User {
        const context = this.getWorkContext();
        return context.user;
    }

    getToken(): string {
        const user = this.getUser();
        if (user == null) {
            return null;
        }
        return this.getUser().token;
    }

    clearSession() {
        this.storage.remove(STORAGE_KEY);
    }

    getWorkContext(): WorkContext {
        let context = this.storage.get(STORAGE_KEY) as WorkContext;
        if (context == null) {
            context = new WorkContext();

            context.defaultSettings = new DefaultSettings(new Catalog(), new System());
            this.storage.set(STORAGE_KEY, context);
        }
        return context;
    }



}
