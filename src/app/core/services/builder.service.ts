import { Injectable } from "@angular/core";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { SessionService } from "src/app/core/services/session.service";
import { Observable } from "rxjs";
import { CommandMethod } from "src/app/shared/command-method";
import { ResetSession } from "../commands-queries/default-command-queries";

@Injectable({
  providedIn: "root"
})
export class BuilderService {
  private dataUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}AAProductBuilder/`;
  }

  resetSession(sessionId): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl,
      new ResetSession(sessionId),
      CommandMethod.POST
    );
  }

  removeBotSession() {
    if (localStorage.getItem("botSessionId")) {
      this.resetSession(localStorage.getItem("botSessionId")).subscribe(() => {
        localStorage.removeItem("botSessionId");
      });
    }
  }
}
