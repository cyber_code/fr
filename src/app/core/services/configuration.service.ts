import { Injectable } from "@angular/core";
import { Configuration } from "../models/configuration.model";
import { HttpClient } from "@angular/common/http";
import { tap } from "rxjs/operators";
@Injectable({
  providedIn: "root"
})
export class ConfigurationService {
  public serverSettings: Configuration;
  constructor(private http: HttpClient) {}
  load(): Promise<Configuration> {
    return this.http
      .get("./config.json")
      .pipe(
        tap((config: Configuration) => {
          this.serverSettings = config;
        })
      )
      .toPromise<Configuration>();
  }
}
