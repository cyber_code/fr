import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ConfigurationService} from './configuration.service';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

import {SessionService} from 'src/app/core/services/session.service';
import {User} from '../models/user.model';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    private _isAuthenticated: boolean;
    private signinUrl: string;
    private profileUrl: string;
    private permissionsUrl: string;
    public authenticationChallange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());
    public authenticationChallange$ = this.authenticationChallange.asObservable();

    constructor(
        private http: HttpClient,
        private session: SessionService,
        private configurationService: ConfigurationService
    ) {
        this.signinUrl = configurationService.serverSettings.identityUrl + 'connect/token';
        this.profileUrl = configurationService.serverSettings.identityUrl + 'connect/userinfo';
        this.permissionsUrl = configurationService.serverSettings.apiUrl + 'Identity/GetAllPermissions';
    }

    challange(): void {
        this.authenticationChallange.next(this.isAuthenticated());
    }

    isAuthenticated() {
        if (this._isAuthenticated) {
            return true;
        }
        const user = this.session.getUser();
        if (!isNullOrUndefined(user) && user.id !== '') {
            this._isAuthenticated = true;
        }
        return this._isAuthenticated || false;
    }

    login(username: string, password: string): Observable<User> {
        const options = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        const user = new User();

        const req = {
            client_id: 'RPA.WebApi',
            scope: 'RPAWebApi UserProfile openid profile',
            username: username,
            password: password,
            grant_type: 'password',
            client_secret: 'Manager123'
        };
        const params = new URLSearchParams();
        for (const key of Object.keys(req)) {
            params.set(key, req[key]);
        }

        return this.http.post<any>(this.signinUrl, params.toString(), options).pipe(
            map(response => {
                if (response) {
                    user.token = response['access_token'];
                    const parsedToken = this.parseJwt(user.token);
                    user.name = parsedToken['name'];
                    user.email = parsedToken['email'];
                    user.role = parsedToken['role'];
                    user.tenantId = parsedToken['tenenat_id'];
                    this.session.setUser(user);
                    this.authenticationChallange.next(true);
                    this._isAuthenticated = true;
                }
                return user;
            })
        );
    }
    logout() {
        this.session.clearSession();
        this.authenticationChallange.next(false);
        this._isAuthenticated = false;
    }

    parseJwt(token) {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }
}
