import { ApiAction } from "../services/api-action";

export class ResetSession extends ApiAction {
  sessionId: string;
  constructor(sessionId) {
    super("ResetSession");
    this.sessionId = sessionId;
  }
}
