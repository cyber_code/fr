import { ApiAction } from "../services/api-action";

export class AddPipeline extends ApiAction {
  constructor(
    title: string,
    description: string,
    designerJson: string,
    folderId: string
  ) {
    super("AddPipeline");
    this.title = title;
    this.description = description;
    this.designerJson = designerJson;
    this.folderId = folderId;
  }
  title: string;
  description: string;
  designerJson: string;
  folderId: string;
}
export class RenamePipeline extends ApiAction {
  constructor(pipelineId: string, title: string, description: string) {
    super("RenamePipeline");
    this.pipelineId = pipelineId;
    this.title = title;
    this.description = description;
  }
  pipelineId: string;
  title: string;
  description: string;
}
export class DeletePipeline extends ApiAction {
  constructor(id: string) {
    super("DeletePipeline");
    this.id = id;
  }
  id: string;
}

export class GetPipelinesByFolderId extends ApiAction {
  constructor(folderId: string) {
    super("GetPipelinesByFolderId");
    this.folderId = folderId;
  }
  folderId: string;
}
export class AddFolder extends ApiAction {
  constructor(title: string, description: string) {
    super("AddFolder");
    this.title = title;
    this.description = description;
  }
  title: string;
  description: string;
}
export class UpdateFolder extends ApiAction {
  constructor(id: string, title: string, description: string) {
    super("UpdateFolder");
    this.id = id;
    this.title = title;
    this.description = description;
  }
  id: string;
  title: string;
  description: string;
}
export class DeleteFolder extends ApiAction {
  constructor(id: string) {
    super("DeleteFolder");
    this.id = id;
  }
  id: string;
}

export class GetFolders extends ApiAction {
  constructor() {
    super("GetFolders");
  }
}

export class GetPipelineItemById extends ApiAction {
  id: string;
  constructor(id: string) {
    super('GetPipelineItemById');
    this.id = id;
  }
}


export class GetPipelineItemColumnsById extends ApiAction {
  pipelineItemId: string;
  constructor(pipelineItemId: string) {
    super('GetPipelineItemColumnsById');
    this.pipelineItemId = pipelineItemId;
  }
}

export class SearchPipeline extends ApiAction {
  keyWord: string;
  constructor(keyWord: string) {
    super('SearchPipeline');
    this.keyWord = keyWord;
  }
}
