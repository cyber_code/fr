import { NavbarComponent } from "./components/navbar/navbar.component";
import { DesignService } from "./services/design.service";
import { LoginComponent } from "./components/login/login.component";
import { NgModule } from "@angular/core";
import { CoreRoutingModule } from "./core-routing.module";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogsModule } from "@progress/kendo-angular-dialog";
import { MessageComponent } from "./components/message/message.component";

@NgModule({
  declarations: [LoginComponent, MessageComponent, NavbarComponent],
  imports: [
    CoreRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DialogsModule
  ],
  providers: [DesignService],
  exports: [MessageComponent, NavbarComponent]
})
export class CoreModule {}
