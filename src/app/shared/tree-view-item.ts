import { NodeType } from "../body/models/node-type.enum";

export class TreeViewItem {
  id: string;
  text: string;
  description: string;
  type: NodeType;
  children: TreeViewItem[];

  constructor(
    id: string,
    text: string,
    description: string,
    type: NodeType,
    children: TreeViewItem[]
  ) {
    this.id = id;
    this.text = text;
    this.description = description;
    this.type = type;
    this.children = children;
  }
}
