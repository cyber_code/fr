import { PipelineItemExecutionStatus } from "../reports/models/pipeline-item-execution-status.enum";
import { WorkflowItemComponentType } from "src/models/workflow-item-component-type.enum";
import { ItemPipelineStatus } from "../reports/models/executed-pipeline-model";

export class HighLighGraphHelper {
  public static defaultColor = "#c8d6e5";
  public static fullGraphDesignedColor = "#87ceeb";
  public static successExecutedStatusColor = "#28a745";
  public static failedExecutedStatusColor = "#8a0303";

  public static highlightFromComponentStatus(
    graph: any,
    workflowItems: ItemPipelineStatus[] = []
  ) {
    const links = graph.getLinks();
    const cells = graph.getCells();
    let highlightedCells = [];
    for (let i = 0; i < workflowItems.length; i++) {
      const cellToHighlight = cells.find(
        x =>
          x.attributes.customData &&
          (x.attributes.customData.id === workflowItems[i].pipelineItemId ||
            x.attributes.id === workflowItems[i].pipelineItemId)
      );
      if (cellToHighlight) {
        highlightedCells.push(...[cellToHighlight]);
        if (
          workflowItems[i].pipelineItemExecutionStatus ==
          PipelineItemExecutionStatus.Success
        ) {
          this.changeShapeOutline(
            cellToHighlight,
            this.successExecutedStatusColor
          );
        } else if (
          workflowItems[i].pipelineItemExecutionStatus ==
          PipelineItemExecutionStatus.Failed
        ) {
          this.changeShapeOutline(
            cellToHighlight,
            this.failedExecutedStatusColor
          );
        }
      }
    }
    for (let i = 0; i < links.length; i++) {
      if (
        highlightedCells.find(
          cell => cell.id === links[i].attributes.source.id
        ) &&
        highlightedCells.find(cell => cell.id === links[i].attributes.target.id)
      ) {
        let targetItem = workflowItems.find(
          x => x.pipelineItemId == links[i].attributes.target.id
        );
        if (
          targetItem.pipelineItemExecutionStatus ==
          PipelineItemExecutionStatus.Success
        ) {
          this.changeLineColor(links[i], this.successExecutedStatusColor);
        } else if (
          targetItem.pipelineItemExecutionStatus ==
          PipelineItemExecutionStatus.Failed
        ) {
          this.changeLineColor(links[i], this.failedExecutedStatusColor);
        }
      }
    }
  }
  public static highLightFullDesignedGraph(graph: any) {
    let startItem = graph
      .getCells()
      .find(
        x =>
          x.attributes &&
          x.attributes.customData &&
          x.attributes.customData.componentType ===
            WorkflowItemComponentType.Start
      );
    let reportItem = graph
      .getCells()
      .find(
        x =>
          x.attributes &&
          x.attributes.customData &&
          x.attributes.customData.componentType ===
            WorkflowItemComponentType.Report
      );
    if (startItem && reportItem) {
      HighLighGraphHelper.highlightGraph(graph, this.fullGraphDesignedColor);
    } else {
      HighLighGraphHelper.highlightGraph(graph, this.defaultColor);
    }
  }
  public static highlightGraph(graph: any, color: string) {
    graph.getCells().forEach(cell => {
      this.changeShapeOutline(cell, color);
    });
    graph.getLinks().forEach(link => {
      this.changeLineColor(link, color);
    });
  }
  private static changeLineColor(link: any, color: string) {
    link.attr("line/stroke", color);
  }
  private static changeShapeOutline(cell: any, color: string) {
    if (cell.attributes.type === "fsa.State") {
      cell.attr("circle/stroke", color);
    } else if (
      cell.attributes.type === "standard.Polygon" ||
      cell.attributes.type === "standard.Rectangle"
    ) {
      cell.attr("body/stroke", color);
    }
  }
}
