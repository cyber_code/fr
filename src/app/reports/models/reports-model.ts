import { ReportTemplateType } from "src/app/body/models/report-type.enum";
import { ReportType } from "../../body/models/report-type.enum";

export class ReportFilterParameter {
  skip: number;
  take: number;
  pipelineName: string;
  startTime: string;
  endTime: string;
}
export class ExecutedReportItem {
  id: string;
  reportType: ReportType;
  reportTemplateType: ReportTemplateType;
}
