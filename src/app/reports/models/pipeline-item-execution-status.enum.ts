export enum PipelineItemExecutionStatus {
  Success = 0,
  Failed = 1,
  InProgress = 2
}
