import { PipelineItemExecutionStatus } from "./pipeline-item-execution-status.enum";
import { ExecutedReportItem } from "./reports-model";
import { WorkflowItemComponentType } from "src/models/workflow-item-component-type.enum";

export class ExecutedPipelineItem {
  endTime: string;
  id: string;
  pipelineItemExecutionDetails: Array<ExecutedPipelineComponentDetails>;
  pipelineName: string;
  reports: Array<ExecutedReportItem>;
  startTime: string;
  status: boolean;
  pipelineId: string;
  numberOfRows: number;
}
export class ExecutedPipelineComponentDetails {
  pipelineItemId: string;
  componentType: WorkflowItemComponentType;
  componentName: string;
  id: string;
  pipelineItemExecutionDetailEndTime: string;
  pipelineItemExecutionDetailStartTime: string;
  pipelineItemExecutionStatus: PipelineItemExecutionStatus;
}
export class ItemPipelineStatus {
  pipelineItemId: string;
  pipelineItemExecutionStatus: PipelineItemExecutionStatus;
}
