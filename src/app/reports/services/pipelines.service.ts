import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { GetLatestPipelineExecutions } from "../../body/commands-queries/pipelines-command-queries";
import { CommandMethod } from "src/app/shared/command-method";
import { ExecutedPipelineItem } from "src/app/reports/models/executed-pipeline-model";

@Injectable()
export class PipelinesService {
  private dataUrl: string;
  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Execution/`;
  }
  getLatestPipelineExecutions(
    skip: number,
    take: number,
    pipelineName: string,
    startTime: string,
    endTime: string
  ): Observable<ExecutedPipelineItem[]> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new GetLatestPipelineExecutions(
        skip,
        take,
        pipelineName,
        startTime,
        endTime
      ),
      CommandMethod.POST
    );
  }
}
