import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../shared/auth.guard";
import { ExecutedPipelineComponent } from "./components/executed-pipeline/executed-pipeline.component";

const routes: Routes = [
  {
    path: "app-executed-pipeline",
    component: ExecutedPipelineComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {}
