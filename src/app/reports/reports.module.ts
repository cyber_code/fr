import { ReportsRoutingModule } from "./reports-routing.module";
import { ChartsModule } from "ng2-charts";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExecutedPipelineComponent } from "./components/executed-pipeline/executed-pipeline.component";
import { GridModule } from "@progress/kendo-angular-grid";
import { DialogsModule } from "@progress/kendo-angular-dialog";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { NavbarComponent } from "../core/components/navbar/navbar.component";
import { CoreModule } from "../core/core.module";
import { ExecutedStatusChartComponent } from "./components/executed-status-chart/executed-status-chart.component";
import { ExecutedStatusWithFilterChartComponent } from "./components/executed-status-with-filter-chart/executed-status-with-filter-chart.component";
import { ExecutedPipelineDetailsComponent } from "./components/executed-pipeline-details/executed-pipeline-details.component";

@NgModule({
  declarations: [
    ExecutedPipelineComponent,
    ExecutedStatusChartComponent,
    ExecutedStatusWithFilterChartComponent,
    ExecutedPipelineDetailsComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    DialogsModule,
    DateInputsModule,
    CoreModule,
    FormsModule,
    ChartsModule,
    ReportsRoutingModule
  ],
  exports: []
})
export class ReportsModule {}
