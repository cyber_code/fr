import { Component, OnInit } from "@angular/core";
import {
  GridDataResult,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import { PipelinesService } from "../../services/pipelines.service";
import { ReportFilterParameter } from "../../models/reports-model";
import { ExecutedPipelineItem } from "../../models/executed-pipeline-model";

@Component({
  selector: "app-executed-pipeline",
  templateUrl: "./executed-pipeline.component.html",
  styleUrls: ["./executed-pipeline.component.css"]
})
export class ExecutedPipelineComponent implements OnInit {
  public executedPipelines: GridDataResult;
  public filter: any;
  public configuredFilter: ReportFilterParameter;
  public dateFormat = "dd/MM/yyyy";
  public dateTimeFormat = "dd/MM/yyyy HH:mm:ss.sss";
  public openedReportsWindow: boolean;
  public openedExecutedPipelineItem: ExecutedPipelineItem;
  constructor(private pipelinesService: PipelinesService) {
    this.setInitialFilterValue();
    this.doSearch();
    this.openedReportsWindow = false;
  }
  ngOnInit() {}
  public executedPipelinesDataStateChange(state: DataStateChangeEvent): void {
    this.configuredFilter.skip = state.skip;
    this.configuredFilter.take = state.take;
    this.doSearch();
  }
  setSearchFilter() {
    this.configuredFilter.pipelineName = this.filter.pipelineName;
    this.configuredFilter.startTime = this.filter.startTime;
    this.configuredFilter.endTime = this.filter.endTime;
    this.doSearch();
  }
  resetSearchField() {
    this.setInitialFilterValue();
    this.doSearch();
  }
  private doSearch() {
    this.pipelinesService
      .getLatestPipelineExecutions(
        this.configuredFilter.skip,
        this.configuredFilter.take,
        this.configuredFilter.pipelineName,
        this.configuredFilter.startTime,
        this.configuredFilter.endTime
      )
      .subscribe(res => {
        this.executedPipelines = {
          data: res,
          total: res.length > 0 ? res[0].numberOfRows : 0
        };
      });
  }
  private setInitialFilterValue() {
    this.filter = {
      pipelineName: "",
      startTime: null,
      endTime: null
    };
    this.configuredFilter = {
      skip: 0,
      take: 20,
      pipelineName: "",
      startTime: null,
      endTime: null
    };
  }
  public openReportsWindow(executedPipelineItem: ExecutedPipelineItem) {
    this.openedReportsWindow = true;
    this.openedExecutedPipelineItem = executedPipelineItem;
  }
  public closeReportsWindow() {
    this.openedReportsWindow = false;
  }
}
