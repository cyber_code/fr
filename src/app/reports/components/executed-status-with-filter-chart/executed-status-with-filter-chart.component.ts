import { Component, OnInit, Input } from "@angular/core";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import * as _ from "underscore";
import { Label } from "ng2-charts";
import { DatePipe } from "@angular/common";
import { ExecutedPipelineItem } from "../../models/executed-pipeline-model";

@Component({
  selector: "app-executed-status-with-filter-chart",
  templateUrl: "./executed-status-with-filter-chart.component.html",
  styleUrls: ["./executed-status-with-filter-chart.component.css"]
})
export class ExecutedStatusWithFilterChartComponent implements OnInit {
  @Input()
  set executedPipelinesData(
    executedPipelinesData: Array<ExecutedPipelineItem>
  ) {
    let datePipe = this.datepipe;
    let monthGroupExecuted = _.groupBy(executedPipelinesData, function(item) {
      let month = datePipe.transform(new Date(item.startTime), "MM/yyyy");
      return month;
    });
    let successData: Array<number> = [];
    let failedData: Array<number> = [];
    for (const key of Object.keys(monthGroupExecuted)) {
      if (this.barChartLabels.indexOf(key.toString()) === -1) {
        this.barChartLabels.push(key.toString());
      }
      const executedPipelinesPerMonth = monthGroupExecuted[key];
      successData.push(
        executedPipelinesPerMonth.filter(x => x.status == true).length
      );
      failedData.push(
        executedPipelinesPerMonth.filter(x => x.status == false).length
      );
    }
    this.barChartData[0].data = successData;
    this.barChartData[0].label = "Success";
    this.barChartData[1].data = failedData;
    this.barChartData[1].label = "Failed";
  }
  public barChartData: ChartDataSets[] = [
    { data: [], label: "" },
    { data: [], label: "" }
  ];
  public barChartLabels: Label[] = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: "end",
        align: "end"
      }
    }
  };
  public barChartLegend = true;
  public barChartType: ChartType = "bar";
  public barChartColors: Array<any> = [
    {
      backgroundColor: "#68ba8e" //Success
    },
    {
      backgroundColor: "#b50404" // Failed
    }
  ];
  constructor(private datepipe: DatePipe) {}

  ngOnInit() {}
}
