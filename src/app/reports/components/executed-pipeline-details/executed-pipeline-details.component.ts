import { WorkflowItemComponentType } from "src/models/workflow-item-component-type.enum";
import { DesignService } from "../../../core/services/design.service";
import { Component, OnInit, Input, ElementRef } from "@angular/core";
import { ReportsService } from "../../../body/services/reports.service";
import {
  ReportType,
  ReportTemplateType
} from "../../../body/models/report-type.enum";
import KitchenSinkService from "src/services/kitchensink-service";
import { StencilService } from "src/services/stencil-service";
import { ToolbarService } from "src/services/toolbar-service";
import { InspectorService } from "src/services/inspector-service";
import { HaloService } from "src/services/halo-service";
import { KeyboardService } from "src/services/keyboard-service";
import { RapidEventsService } from "src/services/rapid-events.service";

import { HighLighGraphHelper } from "src/app/shared/highlight-graph-helper";
import {
  ExecutedPipelineItem,
  ItemPipelineStatus
} from "../../models/executed-pipeline-model";

@Component({
  selector: "app-executed-pipeline-details",
  templateUrl: "./executed-pipeline-details.component.html",
  styleUrls: ["./executed-pipeline-details.component.css"]
})
export class ExecutedPipelineDetailsComponent implements OnInit {
  @Input() public executedPipelineItem: ExecutedPipelineItem;
  public reportType = ReportType;
  public rappid: KitchenSinkService;
  constructor(
    private reportService: ReportsService,
    private rapidEventsService: RapidEventsService,
    private element: ElementRef,
    private designService: DesignService
  ) {}

  ngOnInit() {
    this.executedPipelineItem.pipelineItemExecutionDetails.sort(
      (a, b) =>
        new Date(a.pipelineItemExecutionDetailStartTime).getTime() -
        new Date(b.pipelineItemExecutionDetailStartTime).getTime()
    );
  }

  ngAfterViewInit(): void {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false
    );
    this.rappid.startRappid();
    this.designService
      .getPipelineById(this.executedPipelineItem.pipelineId)
      .subscribe(result => {
        let jsonParsed =
          result && result.designerJson ? JSON.parse(result.designerJson) : [];
        if (
          jsonParsed.length === 0 ||
          (jsonParsed.cells && jsonParsed.cells.length === 0)
        ) {
          this.rappid.loadGraphJson({ cells: [] });
        } else {
          let executedComponents: Array<ItemPipelineStatus> = [];
          this.executedPipelineItem.pipelineItemExecutionDetails.forEach(
            element => {
              executedComponents.push({
                pipelineItemId: element.pipelineItemId,
                pipelineItemExecutionStatus: element.pipelineItemExecutionStatus
              });
              let graphCell = jsonParsed.cells.find(
                x => x.id == element.pipelineItemId
              );
              if (
                graphCell &&
                graphCell.customData &&
                graphCell.customData.jsonMetadata
              ) {
                let componentMetadata = JSON.parse(
                  graphCell.customData.jsonMetadata
                );
                element.componentName = componentMetadata.name;
              }
              element.componentName = element.componentName
                ? element.componentName
                : WorkflowItemComponentType[element.componentType];
            }
          );
          this.rappid.loadGraphJson(jsonParsed);
          this.rappid.saleContentToFitPaper(500, 500);
          this.rappid.layoutGraph(true);
          HighLighGraphHelper.highlightFromComponentStatus(
            this.rappid.graph,
            executedComponents
          );
        }
      });
  }
  public downloadReport(reportId: string, pipelineId: string) {
    window.open(this.reportService.getDownloadReportUrl(reportId, pipelineId));
  }
  public getTimeSpan(start: string, end: string) {
    let startDate = new Date(start);
    let endDate = new Date(end);
    if (start && end) {
      var date1 = startDate.getTime();
      var date2 = endDate.getTime();
      var msec = Math.round(((date2 - date1) / 1000) % 60);
      var mins = Math.floor(msec / 60000);
      var hrs = Math.floor(mins / 60);
      var days = Math.floor(hrs / 24);
      var yrs = Math.floor(days / 365);

      return (
        (hrs <= 9 ? "0" + hrs : hrs) +
        ":" +
        (mins <= 9 ? "0" + mins : mins) +
        ":" +
        (msec <= 9 ? "0" + msec : msec)
      );
    }
  }
}
