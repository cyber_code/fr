import { Component, OnInit, Input } from "@angular/core";
import { ChartType, ChartOptions } from "chart.js";
import { SingleDataSet, Label } from "ng2-charts";
import { ExecutedPipelineItem } from "../../models/executed-pipeline-model";
@Component({
  selector: "app-executed-status-chart",
  templateUrl: "./executed-status-chart.component.html",
  styleUrls: ["./executed-status-chart.component.css"]
})
export class ExecutedStatusChartComponent implements OnInit {
  @Input()
  set executedPipelinesData(
    executedPipelinesData: Array<ExecutedPipelineItem>
  ) {
    let nrSuccess = executedPipelinesData.filter(x => x.status == true).length;
    let nrFailed = executedPipelinesData.filter(x => x.status == false).length;
    this.pieChartData = [nrSuccess, nrFailed];
  }
  public pieChartData: SingleDataSet = [];
  public pieChartLabels: Label[] = ["Success", "Failed"];
  public pieChartType: ChartType = "doughnut";
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: "top"
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    }
  };
  public pieChartColors: Array<any> = [
    {
      backgroundColor: ["#68ba8e", "#b50404"] //Success, Failed
    }
  ];
  public pieChartLegend = true;

  constructor() {}

  ngOnInit() {}
}
