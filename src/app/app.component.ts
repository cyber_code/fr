import { Component, OnInit } from "@angular/core";
import { MessageService } from "./core/services/message.service";
import { MessageType, FRMessage } from "./core/models/message.model";
import { NotificationService } from "@progress/kendo-angular-notification";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";
import * as signalR from "@aspnet/signalr";
import { SignalRService } from "src/services/signalR.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
@AutoUnsubscribe()
export class AppComponent implements OnInit {
  constructor(
    private messageService: MessageService,
    private notificationService: NotificationService,
    private signalRService: SignalRService
  ) {
    // subscribe for messages sent by different components
    this.messageService
      .getMessage()
      .pipe(takeWhileAlive(this))
      .subscribe(message => {
        switch (message.type) {
          case MessageType.Default:
            this.showDefault(message.text);
            break;
          case MessageType.Success:
            this.showSuccess(message.text);
            break;
          case MessageType.Info:
            this.showInfo(message.text);
            break;
          case MessageType.Warning:
            this.showWarning(message.text);
            break;
          case MessageType.Error:
            this.showError(message);
            break;
        }
      });
  }

  ngOnInit() {
    //this.signalRService.startConnection();
    //this.signalRService.test();
  }

  public showDefault(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 600,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 400 },
      type: { style: "none", icon: false }
    });
  }

  public showSuccess(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 1000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "success", icon: true }
    });
  }

  public showWarning(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 5000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "warning", icon: true }
    });
  }

  public showInfo(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 2000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "info", icon: true }
    });
  }

  public showError(message: FRMessage): void {
    if (message.appendTo) {
      this.notificationService.show({
        content: message.text,
        hideAfter: 5000,
        position: { horizontal: "center", vertical: "top" },
        animation: { type: "fade", duration: 600 },
        type: { style: "error", icon: true },
        appendTo: message.appendTo
      });
    } else {
      this.notificationService.show({
        content: message.text,
        hideAfter: 2000,
        position: { horizontal: "center", vertical: "top" },
        animation: { type: "fade", duration: 600 },
        type: { style: "error", icon: true }
      });
    }
  }
}
